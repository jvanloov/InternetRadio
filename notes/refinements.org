

* 1/4/2017
  Modified the IP address parsing code to be a bit more robust. We can
  now also parse out the port number.
  Added test code for the number parsing code.

* 2/4/2017
  Added comments.

* 23/12/2017
  Updated the string input function to allow editing (cursor movement
  left and right, deleting chars in the string). Currently this is
  only used for the password, but it might be used in the future
  e.g. for inputting URLs or aliases for streams or...
