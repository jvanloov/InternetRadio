

* 12/3/2017
  Made a cable to connect a Nokia 5110 display (Adafruit version) to
  the LaunchPad. We use 4 GPIO pins to "bit-bang" the SPI(-like)
  protocol. (There is a data/command line in addition to the data/MOSI
  line, and there is no MISO line.)
  Checked the output quality with a scope, and it looks like CLK and
  CS are looking OK, while DIN and D/C look weird.

* 13/3/2017
  It turns out the GPIO pins 58 and 59 on the launchpad P3 connector
  are not connected by default. However the same pins are available on
  P1 (GPIO 58 as P1-2 and GPIO 59 as P1-6).
  Since P1-6 is not accessible on my improvised cable, I use P1-2 for
  DIN and GPIO 53 (P3-8) as D/C
  I also wired up P1-10 to use as reset, so I can reset it through
  software.
  There was one small logic issue in the data bitbang routine, with
  the mask for selecting the bit to send.
  After these corrections, the display works nicely :-)

* 14/3/2017
  Replaced the "standard" font with my own smaller, variable-width font.

* 18/3/2017
  Started implementing the functions in user_input.c to use the screen
  instead of the serial terminal.
  First off: select_from_list. The implementation uses 4 lines to show
  elements from an item list. It first clears the list area on the
  screen, and writes the items. The selected item is inverted. If
  there are more than 4 items, the list will scroll when the user
  reaches the 4th displayed item and presses the "down" button. It
  will scroll up when the user reaches the first displayed item and
  presses the "up" button.
  Only the list area is cleared, so that other items on the screen are
  untouched.

* 20/3/2017
  Adapted input routines for 4 buttons, added status line on the
  screen for more usage info.
* 26/3/2017
  There was an issue in combining the display code with the
  streaming/playing code, where it would not be able to connect to the
  remote server.
  It worked when debugging with GDB, but not standalone.
  It turned out that the serial terminal was not initialized.
