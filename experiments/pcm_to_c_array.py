#!/usr/bin/python
import struct

max_line_length = 50

min_val = 0
max_val = 0

# Determine min and max values in file
with open('moby_45sec_22kHz.pcm', 'rb') as infile:
    short = infile.read(2)
    while short != b"":
        val = struct.unpack('<h', short)[0]
        if (val < min_val):
            min_val = val
        if (val > max_val):
            max_val = val
        short = infile.read(2)

# Scaling factor to rescale min_val .. max_val -> 0 .. 4096,
# with the middle shifted from 0 to 2048
scaling_factor = 2048.0 / max(-min_val,max_val)

# Reread the file, now writing the output file at the same time
cur_line_length = 0
sample_count = 0
with open('sample_arr.h', 'w') as outfile:
    outfile.write('uint16_t samples[] = {\n     ');
    with open('moby_45sec_22kHz.pcm', 'rb') as infile:
        short = infile.read(2)
        while short != b"":
            # we have a next value, so write a ',' or a newline
            if sample_count > 0:
                if cur_line_length < max_line_length-5:
                    outfile.write(', ')
                else:
                    outfile.write(',\n     ')
                    cur_line_length = 0

            val = struct.unpack('<h', short)[0]
            str = "%d" % (2048 + (val * scaling_factor))
            outfile.write(str)
            cur_line_length += len(str)

            sample_count += 1
            short = infile.read(2)
    outfile.write('\n};\n')

print "Wrote %d samples." % sample_count

    
