#!/usr/bin/python
import struct

max_line_length = 50

cur_line_length = 0
byte_count = 0

chars = [ "0", "1", "2", "3", "4", "5", "6", "7",
	  "8", "9", "A", "B", "C", "D", "E", "F" ]
max = 500000

with open('moby_15secs.h', 'w') as outfile:
    outfile.write('const uint8_t mp3_data[] = {\n     ');
    with open('Moby 15secs.mp3', 'rb') as infile:
        byte = infile.read(1)
        while byte != b"" and byte_count < max:
            # we have a next value, so write a ',' or a newline
            if byte_count > 0:
                if cur_line_length < max_line_length-5:
                    outfile.write(', ')
                else:
                    outfile.write(',\n     ')
                    cur_line_length = 0

            #str = "%d" % ord(byte)
            str = "0x%c%c" % (chars[ord(byte) >> 4], chars[ord(byte) & 0x0F]) 
            outfile.write(str)
            cur_line_length += len(str)

            byte_count += 1
            byte = infile.read(1)
    outfile.write('\n};\n')

print "Wrote %d values." % byte_count

    
