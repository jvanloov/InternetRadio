from math import sin, trunc

angle = 0
for i in xrange(0,360):
  
  angle = angle + 0.01745;
  sinVal = sin(angle);

  # -1 < sinval < 1
  # the following will produce a value between 0 and 4000
  sample = trunc((sinVal * 2000) + 2000);

  print "{0:4}\t{1:7}".format(i, sample)
