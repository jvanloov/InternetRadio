#!/usr/bin/env python

import socket
import re
import sys

def read_m3u_data(data):
    http_status = None
    url = None
    content_type = None
    header_done = False
    for line in data.split('\n'):
        tline = line.strip();
        if header_done:
            url = tline
        if (tline == "") or (tline == "\r"):
            header_done = True
        if tline.startswith("HTTP"):
            res = re.search("HTTP/([0-9\.]+) ([0-9]{3})", tline)
            http_status = res.group(2)
        if tline.startswith("Content-Type"):
            res = re.search("[\w-]+/[\w-]+", tline)
            content_type = res.group(0)
    return { 'http_status': http_status, 'url': url, 'content_type': content_type }
            
TCP_PORT = 80
BUFFER_SIZE = 2048

TCP_IP1 = '146.185.16.113'
m3u_msg = "GET /vrtradio1-low.m3u HTTP/1.1\r\nHost: www.listenlive.eu\r\nAccept: */*\r\n\r\n"



chars = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F']
bitrates = {}
bitrates['0_1_1'] = -1
bitrates['0_1_2'] = -1
bitrates['0_1_3'] = -1
bitrates['0_2_1'] = -1
bitrates['0_2_2'] = -1
bitrates['0_2_3'] = -1
bitrates['1_1_1'] = 32
bitrates['1_1_2'] = 32
bitrates['1_1_3'] = 32
bitrates['1_2_1'] = 32
bitrates['1_2_2'] = 8
bitrates['1_2_3'] = 8
bitrates['2_1_1'] = 64
bitrates['2_1_2'] = 48
bitrates['2_1_3'] = 40
bitrates['2_2_1'] = 48
bitrates['2_2_2'] = 16
bitrates['2_2_3'] = 16
bitrates['3_1_1'] = 96
bitrates['3_1_2'] = 56
bitrates['3_1_3'] = 48
bitrates['3_2_1'] = 56
bitrates['3_2_2'] = 24
bitrates['3_2_3'] = 24
bitrates['4_1_1'] = 128
bitrates['4_1_2'] = 64
bitrates['4_1_3'] = 56
bitrates['4_2_1'] = 64
bitrates['4_2_2'] = 32
bitrates['4_2_3'] = 32
bitrates['5_1_1'] = 160
bitrates['5_1_2'] = 80
bitrates['5_1_3'] = 64
bitrates['5_2_1'] = 80
bitrates['5_2_2'] = 40
bitrates['5_2_3'] = 40
bitrates['6_1_1'] = 192
bitrates['6_1_2'] = 96
bitrates['6_1_3'] = 80
bitrates['6_2_1'] = 96
bitrates['6_2_2'] = 48
bitrates['6_2_3'] = 48
bitrates['7_1_1'] = 224
bitrates['7_1_2'] = 112
bitrates['7_1_3'] = 96
bitrates['7_2_1'] = 112
bitrates['7_2_2'] = 56
bitrates['7_2_3'] = 56
bitrates['8_1_1'] = 256
bitrates['8_1_2'] = 128
bitrates['8_1_3'] = 112
bitrates['8_2_1'] = 128
bitrates['8_2_2'] = 64
bitrates['8_2_3'] = 64
bitrates['9_1_1'] = 288
bitrates['9_1_2'] = 160
bitrates['9_1_3'] = 128
bitrates['9_2_1'] = 144
bitrates['9_2_2'] = 80
bitrates['9_2_3'] = 80
bitrates['10_1_1'] = 320
bitrates['10_1_2'] = 192
bitrates['10_1_3'] = 160
bitrates['10_2_1'] = 160
bitrates['10_2_2'] = 96
bitrates['10_2_3'] = 96
bitrates['11_1_1'] = 352
bitrates['11_1_2'] = 224
bitrates['11_1_3'] = 192
bitrates['11_2_1'] = 176
bitrates['11_2_2'] = 112
bitrates['11_2_3'] = 112
bitrates['12_1_1'] = 384
bitrates['12_1_2'] = 256
bitrates['12_1_3'] = 224
bitrates['12_2_1'] = 192
bitrates['12_2_2'] = 128
bitrates['12_2_3'] = 128
bitrates['13_1_1'] = 416
bitrates['13_1_2'] = 320
bitrates['13_1_3'] = 256
bitrates['13_2_1'] = 224
bitrates['13_2_2'] = 144
bitrates['13_2_3'] = 144
bitrates['14_1_1'] = 448
bitrates['14_1_2'] = 384
bitrates['14_1_3'] = 320
bitrates['14_2_1'] = 256
bitrates['14_2_2'] = 160
bitrates['14_2_3'] = 160
bitrates['15_1_1'] = -1
bitrates['15_1_2'] = -1
bitrates['15_1_3'] = -1
bitrates['15_2_1'] = -1
bitrates['15_2_2'] = -1
bitrates['15_2_3'] = -1
 
"""
bits	V1,L1	V1,L2	V1,L3	V2,L1	V2, L2 & L3
0000    free    free    free    free    free
0001	32	32	32	32	8
0010	64	48	40	48	16
0011	96	56	48	56	24
0100	128	64	56	64	32
0101	160	80	64	80	40
0110	192	96	80	96	48
0111	224	112	96	112	56
1000	256	128	112	128	64
1001	288	160	128	144	80
1010	320	192	160	160	96
1011	352	224	192	176	112
1100	384	256	224	192	128
1101	416	320	256	224	144
1110	448	384	320	256	160
1111	bad	bad	bad	bad	bad
"""

samplerates = {}
samplerates['0_1'] = 44100
samplerates['0_2'] = 22050
samplerates['1_1'] = 48000
samplerates['1_2'] = 24000
samplerates['2_1'] = 32000
samplerates['2_2'] = 16000

"""
bits	MPEG1	MPEG2	MPEG2.5
00	44100 Hz	22050 Hz	11025 Hz
01	48000 Hz	24000 Hz	12000 Hz
10	32000 Hz	16000 Hz	8000 Hz
11	reserv.	reserv.	reserv.
"""

# key = mpeg_layer _ mpeg_version
samples_per_frame = {}
samples_per_frame['1_1'] = 384
samples_per_frame['1_2'] = 384
samples_per_frame['1_0'] = 384
samples_per_frame['2_1'] = 1152
samples_per_frame['2_2'] = 1152
samples_per_frame['2_0'] = 1152
samples_per_frame['3_1'] = 1152
samples_per_frame['3_2'] = 576
samples_per_frame['3_0'] = 576

"""
                MPEG 1	MPEG 2 (LSF)	MPEG 2.5 (LSF)
Layer I	        384	384	384
Layer II	1152	1152	1152
Layer III	1152	576	576
"""


def decode_data(outf, maxloops, open_socket):
    index = 0
    synced = 0
    frame_sync = 0
    byte_count = 0

    for i in xrange(0,maxloops):
        outf.write("// FETCH MORE DATA\n")
        data = open_socket.recv(BUFFER_SIZE)
        for c in data:
            o_c = ord(c)
            nibble_h = (o_c >> 4) & 0x0F
            nibble_l = o_c & 0x0F

            outf.write("0x");
            outf.write(chars[nibble_h])
            outf.write(chars[nibble_l])
            outf.write(", ");

            if frame_sync == 1:
                if (nibble_h != 0x0F) or (nibble_l == 0x0F):
                    frame_sync = 0
                else:
                    synced = 1

                    # read frame info
                    mpeg_version = 4 - ((o_c >> 3) & 0x03)
                    mpeg_layer = 4 - ((o_c >> 1) & 0x03)
                    protected = 1 - (o_c & 0x01)
                    outf.write("// MPEG v")
                    outf.write(str(mpeg_version))
                    outf.write(" layer ")
                    outf.write(str(mpeg_layer))
                    outf.write(" / Protected: ")
                    if protected == 1:
                        outf.write("N")
                    else:
                        outf.write("Y")
                        frame_sync = 2
                        outf.write("\n")

            elif frame_sync == 2:
                bitrate_index = nibble_h
                samplerate_index = (nibble_l >> 2) & 0x03
                padding = (nibble_l >> 1) & 0x01
        
                frame_length = ( (samples_per_frame[str(mpeg_layer) + "_" + str(mpeg_version)] / 8 * 1000 * bitrates[str(bitrate_index) + "_" + str(mpeg_version) + "_" + str(mpeg_layer)]) / samplerates[str(samplerate_index) + "_" + str(mpeg_version)]) + (0 if not padding else (4 if mpeg_layer == 1 else 1))
                byte_count = frame_length

                # frame length includes the header. subtract the number of
                # bytes already read from the byte count
                byte_count -= 4
            
                outf.write("// Bitrate: ")
                outf.write(str(bitrates[str(bitrate_index) + "_" + str(mpeg_version) + "_" + str(mpeg_layer)]))
                outf.write(" / Sample rate: ")
                outf.write(str(samplerates[str(samplerate_index) + "_" + str(mpeg_version)]))
            
                outf.write(" / Frame size: ")
                outf.write(str(samples_per_frame[str(mpeg_layer) + "_" + str(mpeg_version)]))
                outf.write(" / Padded:  ")
                outf.write("Y" if padding else "N")
        
                outf.write(" / Frame length: ")
                outf.write(str(frame_length))
                outf.write("\n")
        
                frame_sync = 3
            elif frame_sync == 3:
                channels = (nibble_h >> 2) & 0x03

                if channels == 0:
                    outf.write("// stereo ")
                elif channels == 1:
                    outf.write("// joint stereo ")
                elif channels == 2:
                    outf.write("// dual channel (mono) ")
                else:
                    outf.write("// mono")
        
                outf.write("\n")
        
                # We read the whole frame header. Go back to "normal" byte reading
                frame_sync = 0
            
            elif (synced == 0) and (frame_sync == 0) and (synced == 0) and  \
                 (nibble_h == 15) and (nibble_l == 15):
                outf.write("\n// First frame detected: \n")
                # FF read -> start of frame; next 3 bytes contain frame info
                frame_sync = 1

            elif (synced == 1) and (frame_sync == 0) and (byte_count == 0) and \
                 (nibble_h == 15) and (nibble_l == 15):
                outf.write("\n// New frame\n")
                frame_sync = 1
        
            elif frame_sync == 0:
                byte_count -= 1
        
                if byte_count == 0:
                    outf.write("\n// ----------\n")

            index += 3
            if index > 40:
                index = 0
                outf.write("\n")

        outf.write("\n")
        


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

s.connect((TCP_IP1, TCP_PORT))
s.send(m3u_msg)
data = s.recv(BUFFER_SIZE)
s.close()

m3u_res = read_m3u_data(data) 
print m3u_res
if m3u_res['http_status'] == "200" and m3u_res['content_type'] == 'audio/x-mpegurl':
    url = m3u_res['url']
    res = re.search("http://([\w\.]+)/([-/\w\.]+)", url)
    host = res.group(1)
    ip = socket.gethostbyname_ex(host)[2][0]
    file = res.group(2)
else:
    exit(-1)

print "URL  =  ", url
print "host = ", host
print "ip   = ", ip
print "file = ", file
print "--------------------------"

stream_msg = "GET /" + file + " HTTP/1.1\r\nHost: " + host + "\r\nAccept: */*\r\n\r\n"

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((ip, TCP_PORT))
s.send(stream_msg)

data = s.recv(BUFFER_SIZE)
print "received data 2 (len: " + str(len(data)) + "):"
print data

with open("stream_data.h", "w") as f:
    f.write("const uint8_t mp3_data[] = {\n")
    decode_data(f, 40, s)
    f.write("};\n")

s.close()

