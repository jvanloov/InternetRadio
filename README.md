# Introduction

The InternetRadio project is an ongoing project to build a
WiFi-connected radio that streams MP3 streams from radio stations on
the internet.

<img src="http://joris.van-looveren.net/images/internetradio-01.jpg"
     alt="InternetRadio device" width="480px" style="text-align: center" />

This project has several different aspects to it: the software for the
user interface and for streaming the data, the software for decoding
the data (using the Helix MP3 decoder); the hardware for turning the
decoded sound samples into actual sound, and an enclosure to house the
electronics. The end goal is to have a reasonably good-looking,
standalone WiFi Internet radio, with decent sound quality, that can
run for a decent amount of time on batteries.

For more details, see:
http://joris.van-looveren.net/projects/internetradio/