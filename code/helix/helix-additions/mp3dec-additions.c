/************************************************************
 * mp3dec-additions.h                                       *
 ************************************************************
 * Configuration constants.                                 *
 * Joris Van Looveren, 4/2017                               *
 ************************************************************
 * We provide a Reset function for the decoder. This        *
 * function resets the state of the decoder. We use this    *
 * to avoid having to re-allocate all buffers when we start *
 * decoding a new stream. Less malloc-ing and free-ing is   *
 * more predictable :-)                                     *
 ************************************************************/

#include <string.h>

#include "mp3common.h"
#include "coder.h"

#define ClearBuffer(buf, nBytes) memset(buf, 0, nBytes)


void Reset(MP3DecInfo *dec) {

  /* important to do this - DSP primitives assume a bunch of state variables are 0 on first use */
  ClearBuffer(dec->FrameHeaderPS,     sizeof(FrameHeader));
  ClearBuffer(dec->SideInfoPS,        sizeof(SideInfo));
  ClearBuffer(dec->ScaleFactorInfoPS, sizeof(ScaleFactorInfo));
  ClearBuffer(dec->HuffmanInfoPS,     sizeof(HuffmanInfo));
  ClearBuffer(dec->DequantInfoPS,     sizeof(DequantInfo));
  ClearBuffer(dec->IMDCTInfoPS,       sizeof(IMDCTInfo));
  ClearBuffer(dec->SubbandInfoPS,     sizeof(SubbandInfo));
  
  ClearBuffer(&(dec->mainBuf[0]), MAINBUF_SIZE);

  for (int i=0;i<MAX_NGRAN; i++) {
    ClearBuffer(&(dec->part23Length[i]), MAX_NCHAN);
  }

  dec->freeBitrateFlag = 0;
  dec->freeBitrateSlots = 0;

  dec->bitrate = 0;
  dec->nChans = 0;
  dec->samprate = 0;
  dec->nGrans = 0;
  dec->nGranSamps = 0;
  dec->nSlots = 0;
  dec->layer = 0;
  dec->version = 0;

  dec->mainDataBegin = 0;
  dec->mainDataBytes = 0;

}
