/************************************************************
 * mp3dec-additions.h                                       *
 ************************************************************
 * Configuration constants.                                 *
 * Joris Van Looveren, 4/2017                               *
 ************************************************************
 * We provide a Reset function for the decoder. This        *
 * function resets the state of the decoder. We use this    *
 * to avoid having to re-allocate all buffers when we start *
 * decoding a new stream. Less malloc-ing and free-ing is   *
 * more predictable :-)                                     *
 ************************************************************/

#include "mp3common.h"

void Reset(MP3DecInfo *dec);
