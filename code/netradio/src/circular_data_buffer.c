// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/************************************************************
 * circular_data_buffer.c                                   *
 ************************************************************
 * Ring buffer implementation                               *
 * Joris Van Looveren                                       *
 ************************************************************/


#include <stdio.h>
#include <string.h>

#include "configuration.h"
#include "circular_data_buffer.h"


void circular_data_buffer_init(circular_data_buffer *d_buf,
			       char *d_buf_storage,
			       int d_buf_storage_size) {
  memset(d_buf_storage, 0, d_buf_storage_size);
  d_buf->data_buffer = d_buf_storage;
  d_buf->data_buffer_size = d_buf_storage_size;
  d_buf->read_pos = 0;
  d_buf->write_pos = 0;
  d_buf->wrapped = 0;
  d_buf->empty = 1;
}


void circular_data_buffer_clear(circular_data_buffer *d_buf) {
  memset(d_buf->data_buffer, 0, d_buf->data_buffer_size);
  d_buf->read_pos = 0;
  d_buf->write_pos = 0;
  d_buf->wrapped = 0;
  d_buf->empty = 1;
}

			       
int circular_data_buffer_usage(circular_data_buffer *d_buf) {
  if (d_buf->wrapped) {
    return d_buf->write_pos + (d_buf->data_buffer_size - d_buf->read_pos);
  } else {
    return d_buf->write_pos - d_buf->read_pos;
  }
}


int circular_data_buffer_chars_left(circular_data_buffer *d_buf) {
  return d_buf->data_buffer_size - circular_data_buffer_usage(d_buf);
}


int circular_data_buffer_store(circular_data_buffer *d_buf, char ch) {
  // d_buf->data_buffer_size-1 because we need one spot for the
  // new character, without overfilling the buffer.
  if ((circular_data_buffer_usage(d_buf) < d_buf->data_buffer_size-1) ||
      d_buf->empty) {
    d_buf->data_buffer[d_buf->write_pos] = ch;
    d_buf->write_pos = d_buf->write_pos + 1;
    d_buf->empty = 0;
    if (d_buf->write_pos >= d_buf->data_buffer_size) {
      d_buf->write_pos = 0;
      d_buf->wrapped = 1;
    }
    return 0;
  } else {
    return -1;
  }
}


int circular_data_buffer_store_block(circular_data_buffer *d_buf, const char *block, int block_size) {
  if (block_size >= 0) {
    if (circular_data_buffer_usage(d_buf) + block_size < d_buf->data_buffer_size) {
      if (d_buf->write_pos + block_size < d_buf->data_buffer_size) {
	// all data fits -> copy in one go
	memcpy(&(d_buf->data_buffer[d_buf->write_pos]), block, block_size);
	d_buf->write_pos += block_size;
      } else {
	// more data than space at the end
	int left_at_end = d_buf->data_buffer_size - d_buf->write_pos;
	memcpy(&d_buf->data_buffer[d_buf->write_pos], block, left_at_end);
	memcpy(&d_buf->data_buffer[0],
	       &block[left_at_end],
	       block_size - left_at_end);
	d_buf->write_pos = block_size - left_at_end;
	d_buf->wrapped = 1;
      }
      d_buf->empty = 0;
      return 0;
    } else {
      return -1;
    }
  } else {
    return -1;
  }
}


int circular_data_buffer_read(circular_data_buffer *d_buf) {
  if (circular_data_buffer_usage(d_buf) > 0) {
    char ch = d_buf->data_buffer[d_buf->read_pos];
    d_buf->read_pos = d_buf->read_pos + 1;
    if (d_buf->read_pos >= d_buf->data_buffer_size) {
      d_buf->read_pos = 0;
      d_buf->wrapped = 0;
    }
    return ch;
  } else {
    return -1;
  }
}

int circular_data_buffer_read_block(circular_data_buffer *d_buf, char *block, int block_size) {
  if (circular_data_buffer_usage(d_buf) >= block_size) {
    if (d_buf->read_pos + block_size < d_buf->data_buffer_size) {  // all can be copied at once
      memcpy(block, &d_buf->data_buffer[d_buf->read_pos], block_size);
      d_buf->read_pos += block_size;
    } else {
      int left_at_end = d_buf->data_buffer_size - d_buf->read_pos;
      memcpy(block, &d_buf->data_buffer[d_buf->read_pos], left_at_end);
      memcpy(&block[left_at_end], d_buf->data_buffer, block_size - left_at_end);
      d_buf->read_pos = block_size - left_at_end;
      d_buf->wrapped = 0;
    }
    return 0;
  } else {
    return -1;
  }
}


int circular_data_buffer_contains_char(circular_data_buffer *d_buf, char ch) {
  if (!d_buf->empty) {
    int index = d_buf->read_pos;
    int read = 0;
    int total = circular_data_buffer_usage(d_buf);

    while (read < total) {
      //printf("idx = %d; ch = %c\n", index, d_buf->data_buffer[index]);
      if (d_buf->data_buffer[index] == ch) {
        return read;
      } else {
        index++;
	read++;
        if (index > d_buf->data_buffer_size) {
	  index = 0;
	}
      }
    }
    return -1;
  } else {
    return -1;
  };  
}
