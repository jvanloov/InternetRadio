// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/************************************************************
 * station_list_plugin.c                                    *
 ************************************************************
 * Plugin to allow the user to select a station from the    *
 * global list of stations.                                 *
 * Joris Van Looveren, 02/2018                              *
 ************************************************************/

#include <stdlib.h>

#include "menu.h"
#include "station_list_plugin.h"
#include "stations_data.h"
#include "player.h"

// Internal data for this plugin
static STATION_DATA* station_list_plugin_data;

// Local function definitions
static int station_list_plugin_initialize(
		      UI_PLUGIN_DATA station_list_plugin_data,
		      STATION_DATA *global_station_list,
		      MENU* parent_menu);
static int station_list_plugin_cleanup(
 		      UI_PLUGIN_DATA station_list_plugin_data);
static int station_list_plugin_terminate(
 		      UI_PLUGIN_DATA station_list_plugin_data);
static char* station_list_plugin_get_menuitem(
		      UI_PLUGIN_DATA station_list_plugin_data);
static int station_list_plugin_get_menuitemactiontype(void);
static void* station_list_plugin_get_menuitemaction(UI_PLUGIN_DATA data, int entry_index);

static void* play_fun(UI_PLUGIN_DATA data, int entry_index);

static MENU* parent;
static MENU station_list = { 0, 0, NULL };

int station_list_plugin_setup(UI_PLUGIN_API* station_list_plugin_api,
			      UI_PLUGIN_DATA* station_list_plugin_data) {
  term_print("Setup station list plugin API functions.\r\n");
  station_list_plugin_api->initialize = station_list_plugin_initialize;
  station_list_plugin_api->cleanup = station_list_plugin_cleanup;
  station_list_plugin_api->terminate = station_list_plugin_terminate;
  station_list_plugin_api->get_menuitem = station_list_plugin_get_menuitem;
  station_list_plugin_api->get_menuitemactiontype = menu_item_submenu_fun;
  station_list_plugin_api->get_menuitemaction = station_list_plugin_get_menuitemaction;

  //station_list.current_selection = -1;
  return 0;
}


static int station_list_plugin_initialize(UI_PLUGIN_DATA station_list_plugin_data,
					  STATION_DATA *global_station_list,
					  MENU* parent_menu) {
  int num_stations = 0, idx;
  
  station_list_plugin_data = global_station_list;
  parent = parent_menu;
  term_print("Initialize station list from global station data.\r\n");

  num_stations = ((STATION_DATA*)station_list_plugin_data)->num_stations;

  menu_initialize(&station_list,
		  ((STATION_DATA*)station_list_plugin_data)->category_name,
		  num_stations);
  for (idx=0; idx < num_stations; idx++) {
    STATION* station = &(((STATION_DATA*)station_list_plugin_data)->stations[idx]);
    station_list.entry_names[idx] = station->short_name;
    station_list.entry_data[idx] = (void*)station;
    station_list.entry_actions[idx] = play_fun;
    station_list.entry_action_types[idx] = menu_item_action_fun;
  }
  return 0;
}

static int station_list_plugin_cleanup(UI_PLUGIN_DATA station_list_plugin_data) {
  term_print("Cleanup station list\r\n");
  menu_terminate(&station_list);
  return 0;
}

static int station_list_plugin_terminate(UI_PLUGIN_DATA station_list_plugin_data) {
  term_print("Terminate station list\r\n");
  return 0;
}

static char* station_list_plugin_get_menuitem(UI_PLUGIN_DATA station_list_plugin_data) {
  return station_list.title;
}

static void* station_list_plugin_get_menuitemaction(UI_PLUGIN_DATA data, int entry_index) {
  return &station_list;
}

static void* play_fun(UI_PLUGIN_DATA plugin_data, int entry_index) {
  STATION* data = station_list.entry_data[entry_index];
  term_print("Play '");
  term_print(data->short_name);
  term_print("'\r\n");
  //station_list.current_selection = entry_index;
  player_play_station(data);
  
  return NULL;
}
