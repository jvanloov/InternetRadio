// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "hal_mcu.h"


void print_frame_header(const uint8_t *data_buffer) {
  term_print("vals: ");
  term_print_int(data_buffer[0]);
  term_print(" ");
  term_print_int(data_buffer[1]);
  term_print(" ");
  term_print_int(data_buffer[2]);
  term_print(" ");
  term_print_int(data_buffer[3]);
  term_print("\r\n");
}

void print_frame_beginning(uint8_t *frame) {
  int i;
  int line_length = 0;
  for (i=0; i<208; i++) {
    uint8_t val = frame[i];

    term_print_hex(val, 1);
    term_print(" ");

    line_length += 5;
    if (line_length > 80) {
      term_print("\r\n");
      line_length = 0;
    }
    
  }
  term_print("\r\n");
}


