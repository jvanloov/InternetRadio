// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/************************************************************
 * display_nokia-5110_lowlevel.c                            *
 ************************************************************
 * PCD8544-based LCD driver layer for CC3200                *
 * Joris Van Looveren, 03/2017                              *
 *                                                          *
 * This display driver provides controller routines for     *
 * a PCD8544 LCD controller. Connections:                   *
 * - GPIO 60: clock signal (CLK)                            *
 * - GPIO 58: data signal (DIN)                             *
 * - GPIO 53: data/command selection (D/C)                  *
 * - GPIO 50: chip select (CS)                              *
 * - GPIO 63: reset (RESET)                                 *
 *                                                          *
 * Inspired from                                            *
 * https://github.com/sparkfun/GraphicLCD_Nokia_5110/blob/  *
 *   master/Firmware/Nokia-5100-LCD-Example/LCD_Functions.h *
 ************************************************************/

#include "hal_mcu.h"
#include "configuration.h"

/*** LOW-LEVEL INTERFACE ***/

// Local variables:
// screen_contents is the buffer that we use for preparing the
// contents. This buffer follows the format of the LCD driver
// chip: we can send it as an array to update the screen.
//static char screen_contents[SCREENW*SCREENH/8];
static char display_buffer[SCREENW*SCREENH/8];

// Send the buffer contents to the LCD driver chip.
void display_send_buffer(){
  //display_set_hw_cursor(0, 0);
  for(int i=0; i<SCREENW*SCREENH/8; i=i+1){
    display_write(DISPLAY_DATA, display_buffer[i]);
  }
}

void display_erase_buffer(void){
  for(int i=0; i<SCREENW*SCREENH/8; i=i+1){
    display_buffer[i] = 0x00;
  }
}

// Clear the display directly, by sending a string of 0's to the
// driver chip. Also reset the hardware cursor.
void display_erase(void){
  display_erase_buffer();
  display_send_buffer();
}

void display_init_lowlevel() {
  // chip active; horizontal addressing mode (V = 0);
  // use extended instruction set (H = 1)
  display_write(DISPLAY_COMMAND, 0x21);
  // set LCD Vop (contrast), which may require some tweaking: try
  // 0xB1 (for 3.3V red SparkFun),
  // 0xB8 (for 3.3V blue SparkFun),
  // 0xBF if your display is too dark, or
  // 0x80 to 0xFF if experimenting
  display_write(DISPLAY_COMMAND, 0xB8);
  // set temp coefficient
  display_write(DISPLAY_COMMAND, 0x04);
  // LCD bias mode 1:48: try 0x13 or 0x14
  display_write(DISPLAY_COMMAND, 0x14);
  // we must send 0x20 before modifying the display control mode
  display_write(DISPLAY_COMMAND, 0x20);
  // set display control to normal mode: 0x0D for inverse
  display_write(DISPLAY_COMMAND, 0x0C);
}


void display_set_buffer_pixel(unsigned char x, unsigned char y,
				     unsigned char on) {
  int rowByte = y / 8;
  int rowBit = y % 8;

  if (on) {
    display_buffer[(x+1)+(rowByte*SCREENW-1)] |= (1<<rowBit);
  } else {
    display_buffer[(x+1)+(rowByte*SCREENW-1)] &= ~(1<<rowBit);    
  }
}


int display_get_buffer_pixel(unsigned char x, unsigned char y) {
  int rowByte = y / 8;
  int rowBit = y % 8;

  return (display_buffer[(x+1)+(rowByte*SCREENW-1)] & (1<<rowBit)) > 0;
}


