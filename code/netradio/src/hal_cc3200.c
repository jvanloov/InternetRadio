// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/************************************************************
 * hal_cc3200.c                                             *
 ************************************************************
 * Hardware abstraction layer for CC3200                    *
 * Joris Van Looveren, 10/2016                              *
 *                                                          *
 * This hal provides:                                       *
 * - an SPI channel on PIN_05 to PIN_08                     *
 * - a timer (timer A0)                                     *
 * - an output "terminal" on UART A0                        *
 * - a PCD8544 LCD controller on GPIOs 60, 58, 53, 50, 63   *
 *                                                          *
 ************************************************************/

#include <stdio.h>

#include "hw_ints.h"
#include "hw_types.h"
#include "hw_memmap.h"
#include "pin.h"
#include "rom.h"
#include "rom_map.h"
#include "gpio.h"
#include "prcm.h"

#include "common.h"
#include "interrupt.h"
#include "timer.h"
#include "utils.h"
#include "spi.h"
#include "uart.h"
#include "fs.h"

#include "hal_mcu.h"

static const char *hex[] = { "0", "1", "2", "3", "4", "5", "6", "7",
    	              "8", "9", "A", "B", "C", "D", "E", "F" };


// import vector table base
#if defined(gcc) || defined(ccs)
extern void (* const g_pfnVectors[])(void);
#endif
#if defined(ewarm)
extern uVectorEntry __vector_table;
#endif


void hal_init(void) {
}

void hal_cleanup(void) {
}

void board_init(void)
{
  // Set vector table base
#if defined(ccs) || defined(gcc)
    MAP_IntVTableBaseSet((unsigned long)&g_pfnVectors[0]);
#endif
#if defined(ewarm)
    MAP_IntVTableBaseSet((unsigned long)&__vector_table);
#endif

    MAP_IntMasterEnable();
    MAP_IntEnable(FAULT_SYSTICK);

    PRCMCC3200MCUInit();
}


#ifndef NO_TERM
void uart_init(void)
{
    MAP_PRCMPeripheralClkEnable(PRCM_UARTA0, PRCM_RUN_MODE_CLK);

    // Configure PIN_55 for UART0 UART0_TX
    MAP_PinTypeUART(PIN_55, PIN_MODE_3);
    // Configure PIN_57 for UART0 UART0_RX
    MAP_PinTypeUART(PIN_57, PIN_MODE_3);
}


void term_init(int baud_rate) {
  MAP_UARTConfigSetExpClk(UARTA0_BASE,MAP_PRCMPeripheralClockGet(PRCM_UARTA0), 
                  baud_rate, (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                   UART_CONFIG_PAR_NONE));
}

void term_print(const char *str) {
    if(str != NULL)
    {
        while(*str!='\0')
        {
            MAP_UARTCharPut(UARTA0_BASE,*str++);
        }
    }
}


static void term_print_int_aux(int val) { 
  if (val > 9) {
    int a = val / 10;
    
    val -= 10 * a;
    term_print_int_aux(a);
  }
  MAP_UARTCharPut(UARTA0_BASE, '0' + val);
}


void term_print_int(int val) {
  char buf[7];
  int index = 0;
  
  if (val < 0) {
    MAP_UARTCharPut(UARTA0_BASE,'-');
    val = -val;
  }

  term_print_int_aux(val);
}


void term_print_val(char *msg, int val) {
  term_print(msg);
  term_print_int(val);
  term_print("\r\n");
}


void term_print_hex(uint8_t val, char header) {
  if (header) {
    term_print("0x");
  }
  term_print(hex[(val >> 4)]);
  term_print(hex[(val & 0x0F)]);    
}
#endif

// Code to control a Nokia 5110 LCD from the CC3200. The
// display_reset function is basically the one from
// https://github.com/sparkfun/GraphicLCD_Nokia_5110/blob/master/Firmware/Nokia-5100-LCD-Example/LCD_Functions.h
// the display_write function sends byte-sized messages and
// commands to the PCD8544.
// We use the GPIO pins defined below for clock, data, d/c,
// chipselect and reset

#define DISPLAY_CLK     PIN_60
#define DISPLAY_PIN_CLK GPIO_PIN_5
#define DISPLAY_DIN     PIN_58
#define DISPLAY_PIN_DIN GPIO_PIN_3
#define DISPLAY_DC      PIN_53
#define DISPLAY_PIN_DC  GPIO_PIN_6
#define DISPLAY_CS      PIN_50
#define DISPLAY_PIN_CS  GPIO_PIN_0
#define DISPLAY_RES     PIN_63
#define DISPLAY_PIN_RES GPIO_PIN_0

#define DISPLAY_BACKLIGHT     PIN_64
#define DISPLAY_MAX_BL          255
#define DUTYCYCLE_GRANULARITY   157

void display_init() {
  MAP_PRCMPeripheralClkEnable(PRCM_GPIOA0, PRCM_RUN_MODE_CLK);
  MAP_PRCMPeripheralClkEnable(PRCM_GPIOA1, PRCM_RUN_MODE_CLK);
  MAP_PRCMPeripheralClkEnable(PRCM_GPIOA3, PRCM_RUN_MODE_CLK);

  // The display is driven using SPI (more or less), including
  // a data/command pin. Since we're already using the CC3200
  // SPI for audio out, we'll bit-bang the display "SPI" using
  // a couple of GPIO pins.

  // CLK signal - clock
  MAP_PinTypeGPIO(DISPLAY_CLK, PIN_MODE_0, false);
  MAP_GPIODirModeSet(GPIOA0_BASE, DISPLAY_PIN_CLK, GPIO_DIR_MODE_OUT);
  // DIN signal - MOSI
  MAP_PinTypeGPIO(DISPLAY_DIN, PIN_MODE_0, false);
  MAP_GPIODirModeSet(GPIOA0_BASE, DISPLAY_PIN_DIN, GPIO_DIR_MODE_OUT);
  // D/C signal - data/command
  MAP_PinTypeGPIO(DISPLAY_DC, PIN_MODE_0, false);
  MAP_GPIODirModeSet(GPIOA3_BASE, DISPLAY_PIN_DC, GPIO_DIR_MODE_OUT);
  // CS signal - chip select
  MAP_PinTypeGPIO(DISPLAY_CS, PIN_MODE_0, false);
  MAP_GPIODirModeSet(GPIOA0_BASE, DISPLAY_PIN_CS, GPIO_DIR_MODE_OUT);
  // Reset
  MAP_PinTypeGPIO(DISPLAY_RES, PIN_MODE_0, false);
  MAP_GPIODirModeSet(GPIOA1_BASE, DISPLAY_PIN_RES, GPIO_DIR_MODE_OUT);  

  MAP_GPIOPinWrite(GPIOA0_BASE, DISPLAY_PIN_CS, DISPLAY_PIN_CS);

  // Timer for backlight PWM
  MAP_PRCMPeripheralClkEnable(PRCM_TIMERA2, PRCM_RUN_MODE_CLK);
  // Backlight
  MAP_PinTypeTimer(DISPLAY_BACKLIGHT, PIN_MODE_3);

  MAP_PRCMPeripheralClkEnable(PRCM_TIMERA2, PRCM_RUN_MODE_CLK);
  MAP_TimerConfigure(TIMERA2_BASE,(TIMER_CFG_SPLIT_PAIR | TIMER_CFG_B_PWM));
  MAP_TimerPrescaleSet(TIMERA2_BASE,TIMER_B, 0);
  MAP_TimerControlLevel(TIMERA2_BASE,TIMER_B, 1);
  MAP_TimerLoadSet(TIMERA2_BASE,TIMER_B,(DISPLAY_MAX_BL*DUTYCYCLE_GRANULARITY));
  MAP_TimerMatchSet(TIMERA2_BASE,TIMER_B,(DISPLAY_MAX_BL/2*DUTYCYCLE_GRANULARITY));
  MAP_TimerEnable(TIMERA2_BASE,TIMER_B);
}


void display_reset() {
  MAP_GPIOPinWrite(GPIOA1_BASE, DISPLAY_PIN_RES, 0);
  mcu_delay(10);
  MAP_GPIOPinWrite(GPIOA1_BASE, DISPLAY_PIN_RES, DISPLAY_PIN_RES);
}

void display_set_backlight_brightness(int level) {
  int match_set_val = DUTYCYCLE_GRANULARITY;
  switch (level) {
  case 1:
    match_set_val *= 16;
    break;
  case 2:
    match_set_val *= 32;
    break;
  case 3:
    match_set_val *= 64;
    break;
  case 4:
    match_set_val *= 127;
    break;
  case 5:
    match_set_val *= 255;
    break;
  default:
    match_set_val *= 127;
    break;
  }
  MAP_TimerMatchSet(TIMERA2_BASE,TIMER_B, match_set_val);
}


void display_write(char type, char msg) {
  // Send a byte to the display:
  // 1. D/C is set to the right type (command = low, data = high)
  // 2. CS goes low
  // 3. send the eight bytes
  // 4. CS goes high

  // 1.
  if (type == DISPLAY_COMMAND) {
    MAP_GPIOPinWrite(GPIOA3_BASE, DISPLAY_PIN_DC, 0);
  } else {
    MAP_GPIOPinWrite(GPIOA3_BASE, DISPLAY_PIN_DC, DISPLAY_PIN_DC);
  }
  // 2.
  MAP_GPIOPinWrite(GPIOA0_BASE, DISPLAY_PIN_CS, 0);
  mcu_delay(10);
  // 3. byte is sent MSB first
  for (int i=0; i<8; i++) {
    // clock low
    MAP_GPIOPinWrite(GPIOA0_BASE, DISPLAY_PIN_CLK, 0);
    mcu_delay(10);
    // set data signal
    char val = (msg >> (7-i)) & 0x01;
    if (val > 0) {
      MAP_GPIOPinWrite(GPIOA0_BASE, DISPLAY_PIN_DIN, DISPLAY_PIN_DIN);
    } else {
      MAP_GPIOPinWrite(GPIOA0_BASE, DISPLAY_PIN_DIN, 0);
    }
    // clock high
    MAP_GPIOPinWrite(GPIOA0_BASE, DISPLAY_PIN_CLK, DISPLAY_PIN_CLK);
    mcu_delay(10);
  }
  // 4.
  MAP_GPIOPinWrite(GPIOA0_BASE, DISPLAY_PIN_CS, DISPLAY_PIN_CS);
  mcu_delay(10);
}


void button_init() {
    MAP_PRCMPeripheralClkEnable(PRCM_GPIOA0, PRCM_RUN_MODE_CLK);
    MAP_PRCMPeripheralClkEnable(PRCM_GPIOA1, PRCM_RUN_MODE_CLK);
    MAP_PRCMPeripheralClkEnable(PRCM_GPIOA2, PRCM_RUN_MODE_CLK);
    MAP_PRCMPeripheralClkEnable(PRCM_GPIOA3, PRCM_RUN_MODE_CLK);

    // S2 / SW2
    MAP_PinTypeGPIO(PIN_15, PIN_MODE_0, false);
    MAP_GPIODirModeSet(GPIOA2_BASE, GPIO_PIN_6, GPIO_DIR_MODE_IN);
    MAP_PinConfigSet(PIN_15, PIN_STRENGTH_4MA, PIN_TYPE_STD_PD);
    // S1
    MAP_PinTypeGPIO(PIN_18, PIN_MODE_0, false);
    MAP_GPIODirModeSet(GPIOA3_BASE, GPIO_PIN_4, GPIO_DIR_MODE_IN);
    MAP_PinConfigSet(PIN_18, PIN_STRENGTH_4MA, PIN_TYPE_STD_PD);
    // S3
    MAP_PinTypeGPIO(PIN_62, PIN_MODE_0, false);
    MAP_GPIODirModeSet(GPIOA0_BASE, GPIO_PIN_7, GPIO_DIR_MODE_IN);
    MAP_PinConfigSet(PIN_62, PIN_STRENGTH_4MA, PIN_TYPE_STD_PD);
    // S4
    MAP_PinTypeGPIO(PIN_61, PIN_MODE_0, false);
    MAP_GPIODirModeSet(GPIOA0_BASE, GPIO_PIN_6, GPIO_DIR_MODE_IN);
    MAP_PinConfigSet(PIN_61, PIN_STRENGTH_4MA, PIN_TYPE_STD_PD);
    // SW3
    MAP_PinTypeGPIO(PIN_04, PIN_MODE_0, false);
    MAP_GPIODirModeSet(GPIOA1_BASE, GPIO_PIN_5, GPIO_DIR_MODE_IN);
    MAP_PinConfigSet(PIN_04, PIN_STRENGTH_4MA, PIN_TYPE_STD_PD);
}


int button_check(BUTTONS button) {
  if (button == BUTTON_2) {
    return (MAP_GPIOPinRead(GPIOA2_BASE, GPIO_PIN_6) > 0);
  } else if (button == BUTTON_1) {
    return (MAP_GPIOPinRead(GPIOA3_BASE, GPIO_PIN_4) > 0);
  } else if (button == BUTTON_3) {
    return (MAP_GPIOPinRead(GPIOA0_BASE, GPIO_PIN_7) > 0);
  } else if (button == BUTTON_4) {
    return (MAP_GPIOPinRead(GPIOA0_BASE, GPIO_PIN_6) > 0);
  } else if (button == BUTTON_5) {
    return (MAP_GPIOPinRead(GPIOA1_BASE, GPIO_PIN_5) > 0);
  }

  return -1;
}


void spi_setup() {
  MAP_PRCMPeripheralClkEnable(PRCM_GSPI, PRCM_RUN_MODE_CLK);
  
  MAP_PinTypeSPI(PIN_05, PIN_MODE_7);  // CLK
  MAP_PinTypeSPI(PIN_06, PIN_MODE_7);  // MISO
  MAP_PinTypeSPI(PIN_07, PIN_MODE_7);  // MOSI
  MAP_PinTypeSPI(PIN_08, PIN_MODE_7);  // CS

  MAP_SPIReset(GSPI_BASE);
  MAP_SPIConfigSetExpClk(
			 GSPI_BASE,
			 80000000,  // System clock
			 10000000,  // 10Mhz transfer rate
			 SPI_MODE_MASTER,
			 SPI_SUB_MODE_2,
			 (SPI_SW_CTRL_CS | SPI_4PIN_MODE | SPI_TURBO_ON |
			  SPI_CS_ACTIVELOW | SPI_WL_8));
}


void spi_enable() {
  MAP_SPIEnable(GSPI_BASE);
}

void spi_send(void *bytes, int byte_count) {
  MAP_SPITransfer(GSPI_BASE,(unsigned char*)bytes,0,byte_count,SPI_CS_ENABLE|SPI_CS_DISABLE);
}


void timer_setup(int timer_val, void (* timerA0_handler)(void)) {
// Set up the timer at the desired sample rate
  MAP_PRCMPeripheralClkEnable(PRCM_TIMERA0, PRCM_RUN_MODE_CLK);
  MAP_PRCMPeripheralReset(PRCM_TIMERA0);
  MAP_TimerConfigure(TIMERA0_BASE,TIMER_CFG_PERIODIC);
  //MAP_TimerPrescaleSet(TIMERA0_BASE,TIMER_A, timer_val);
  
  MAP_IntPrioritySet(INT_TIMERA0A, INT_PRIORITY_LVL_1);
  MAP_TimerIntRegister(TIMERA0_BASE, TIMER_A, timerA0_handler);
  MAP_TimerIntEnable(TIMERA0_BASE, TIMER_TIMA_TIMEOUT);

  MAP_TimerLoadSet(TIMERA0_BASE, TIMER_A, timer_val);
}


void timer_enable() {  
  MAP_TimerEnable(TIMERA0_BASE, TIMER_A);
}

void timer_disable() {
  MAP_TimerDisable(TIMERA0_BASE, TIMER_A);
}

void timer_interrupt_clear() {
  int ulInts = MAP_TimerIntStatus(TIMERA0_BASE, true);
  MAP_TimerIntClear(TIMERA0_BASE, ulInts);
}


void timer_interrupt_disable() {
  MAP_IntDisable(INT_TIMERA0A);
}


void timer_interrupt_enable() {
  MAP_IntEnable(INT_TIMERA0A);
}


void mcu_delay(int delay_us)  {
  int loops_per_us = (80000000 / 3) / 1000000;
  
  MAP_UtilsDelay(delay_us * loops_per_us);
}


void led_init(void) {
  MAP_PRCMPeripheralClkEnable(PRCM_GPIOA1, PRCM_RUN_MODE_CLK);
  // led 1: orange
  MAP_PinTypeGPIO(PIN_01, PIN_MODE_0, false);
  MAP_GPIODirModeSet(GPIOA1_BASE, 0x04, GPIO_DIR_MODE_OUT);
  // led 2: green
  MAP_PinTypeGPIO(PIN_02, PIN_MODE_0, false);
  MAP_GPIODirModeSet(GPIOA1_BASE, 0x08, GPIO_DIR_MODE_OUT);
}

void led_toggle(void) {
  int ledOn = MAP_GPIOPinRead(GPIOA1_BASE, 0x04);
  if (ledOn) {
    MAP_GPIOPinWrite(GPIOA1_BASE, 0x04, 0);
  } else {
    MAP_GPIOPinWrite(GPIOA1_BASE, 0x04, 0x04);
  }
}

void led2_toggle(void) {
  int ledOn = MAP_GPIOPinRead(GPIOA1_BASE, 0x08);
  if (ledOn) {
    MAP_GPIOPinWrite(GPIOA1_BASE, 0x08, 0);
  } else {
    MAP_GPIOPinWrite(GPIOA1_BASE, 0x08, 0x08);
  }
}


void amplifier_init(void) {
  // the enable pin for the amplifier is connected to:
  // - PIN_21 (CC3200)      (GPIO_25, GPIOA3/GPIO_PIN_1)
  // - PIN_59 (CC3200MOD)   (GPIO_04, GPIOA0/GPIO_PIN_4)
  // Set as output pin
  MAP_PRCMPeripheralClkEnable(PRCM_GPIOA3, PRCM_RUN_MODE_CLK);
  MAP_PinTypeGPIO(PIN_21, PIN_MODE_0, false);
  MAP_GPIODirModeSet(GPIOA3_BASE, 0x2, GPIO_DIR_MODE_OUT);

  MAP_PRCMPeripheralClkEnable(PRCM_GPIOA0, PRCM_RUN_MODE_CLK);
  MAP_PinTypeGPIO(PIN_59, PIN_MODE_0, false);
  MAP_GPIODirModeSet(GPIOA0_BASE, 0x10, GPIO_DIR_MODE_OUT);

  // off at start
  MAP_GPIOPinWrite(GPIOA3_BASE, 0x02, 0);
  MAP_GPIOPinWrite(GPIOA0_BASE, 0x10, 0);
}

void amplifier_enable(void) {
  MAP_GPIOPinWrite(GPIOA3_BASE, 0x02, 0x02);  
  MAP_GPIOPinWrite(GPIOA0_BASE, 0x10, 0x10);
}

void amplifier_disable(void) {
  MAP_GPIOPinWrite(GPIOA3_BASE, 0x02, 0);  
  MAP_GPIOPinWrite(GPIOA0_BASE, 0x10, 0);
}

// Filesystem

FILE_STATE file_open_for_write(const char* name, FILE_HANDLE* handle) {
  int result = sl_FsOpen((unsigned char *) name,
			 FS_MODE_OPEN_CREATE(16384,   // estimate 16K for each file
 					     _FS_FILE_OPEN_FLAG_NO_SIGNATURE_TEST |
					     _FS_FILE_PUBLIC_WRITE),
			 NULL,
			 &(handle->handle));
  handle->current_offset = 0;
  if (result == SL_FS_OK) {
    return FILE_OK;
  } else {
    // Error during open. Block future file operations.
    handle->current_offset == -1;
    return FILE_ERROR;
  }
}

FILE_STATE file_open_for_read(const char* name, FILE_HANDLE* handle) {
  int result = sl_FsOpen((unsigned char *) name,
			 FS_MODE_OPEN_READ,
			 NULL,
			 &(handle->handle));
  handle->current_offset = 0;
  if (result == SL_FS_OK) {
    return FILE_OK;
  } else {
    // Error during open. Block future file operations.
    handle->current_offset == -1;
    return FILE_ERROR;
  }
}

FILE_STATE file_write_block(FILE_HANDLE* handle, char* block, int block_size) {
  if (handle->current_offset != -1) {
    int result = sl_FsWrite(handle->handle, handle->current_offset, block, block_size);
    if (result > 0) {
      handle->current_offset += result;
      return FILE_OK;
    } else {
      // Error during write. Block future file operations.
      handle->current_offset == -1;
    }
  }
  return FILE_ERROR;
}

FILE_STATE file_read_block(FILE_HANDLE* handle, char* block, int block_size, int* chars_read) {
  if (handle->current_offset != -1) {
    int result = sl_FsRead(handle->handle, handle->current_offset, (unsigned char*) block, block_size);
    if (result > 0) {
      handle->current_offset += result;
      *chars_read = result;
      return FILE_OK;
    } else {
      // Error during read. Block future file operations.
      handle->current_offset == -1;
    }
  }
  return FILE_ERROR;
}

FILE_STATE file_close(FILE_HANDLE* handle) {
  int result = sl_FsClose(handle->handle, NULL, NULL , 0);
  if (result == SL_FS_OK) {
    return FILE_OK;
  } else {
    return FILE_ERROR;
  }
}

FILE_STATE file_delete(const char* name) {
  int result = sl_FsDel(name, 0);
  if (result == SL_FS_OK) {
    return FILE_OK;
  } else {
    return FILE_ERROR;
  }
}
