// Copyright 2019 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/************************************************************
 * display_ea-dogl128-6.c                                   *
 ************************************************************
 * ST7565R-based LCD driver layer.                          *
 * Joris Van Looveren, 02/2019                              *
 *                                                          *
 * This display driver provides controller routines for     *
 * a STR565R LCD controller.                                *
 * The connections are the same as for 
 ************************************************************/

#include <string.h>
#include "hal_mcu.h"
#include "configuration.h"
#include "display_lowlevel.h"


// Local variables:
// screen_contents is the buffer that we use for preparing the
// contents. This buffer follows the format of the LCD driver
// chip: we can send it as an array to update the screen.
static char screen_contents[SCREENW][SCREENH/8];


// Low-level LCD driver initialization
void display_init_lowlevel(void) {
  // Initialize the display in low-power/single-supply mode
  // from the datasheet at https://www.lcd-module.com/eng/pdf/grafik/dogl128-6e.pdf
  // Display start line set: Display start line 0
  display_write(DISPLAY_COMMAND, 0x40);
  // ADC set: ADC reverse
  display_write(DISPLAY_COMMAND, 0xA1);
  // Common output mode select: Normal COM0~COM63
  display_write(DISPLAY_COMMAND, 0xC0);
  // Display normal/reverse: Display normal
  display_write(DISPLAY_COMMAND, 0xA6);
  // LCD bias set: Set bias 1/9 (Duty 1/65)
  display_write(DISPLAY_COMMAND, 0xA2);
  // Power control set: Booster, Regulator and Follower on
  display_write(DISPLAY_COMMAND, 0x2F);
  // Booster ratio set: Set internal Booster to 4x
  display_write(DISPLAY_COMMAND, 0xF8);
  display_write(DISPLAY_COMMAND, 0x00);
  // V0 voltage regulator set
  display_write(DISPLAY_COMMAND, 0x27);
  // Electronic volume mode set (contrast set)
  display_write(DISPLAY_COMMAND, 0x81);
  display_write(DISPLAY_COMMAND, 0x10);
  // Static indicator set: no indicator
  display_write(DISPLAY_COMMAND, 0xAC);
  display_write(DISPLAY_COMMAND, 0x00);
  // Display ON/OFF: Display on
  display_write(DISPLAY_COMMAND, 0xAF);
}

// Send the buffer contents to the LCD driver chip.
void display_send_buffer(void){
  display_write(DISPLAY_COMMAND, 0x40);  // start line: 0

  for (int page=0; page<SCREENH/8; page++) {
    display_write(DISPLAY_COMMAND, 0xb0 + page);  // row no. = page * 8
    display_write(DISPLAY_COMMAND, 0x10);         // start at col. 0
    display_write(DISPLAY_COMMAND, 0x00);         //   "    "  "   "
  
    for (int column=0; column<SCREENW; column++) {
      // send data; col. no is incremented automatically
      display_write(DISPLAY_DATA, screen_contents[column][page]);
    }
  }
}


// Clear the display buffer.
// This only clears the buffer; the screen itself is not updated.
void display_erase_buffer(void){
  for (int page=0; page<SCREENH/8; page++) {
    for (int column=0; column<SCREENW; column++) {
      screen_contents[column][page] = 0;
    }
  }
}

// Clear the buffer and the display.
void display_erase(void){
  display_erase_buffer();
  display_send_buffer();
}


// Internal function: set the pixel at (x, y) on or off in the
// screen buffer.
void display_set_buffer_pixel(unsigned char x, unsigned char y, unsigned char on) {
  int rowByte = y / 8;
  int rowBit = y % 8;

  if (on) {
    screen_contents[x][rowByte] |= (1<<rowBit);
    //screen_contents[(x+1)][(rowByte*SCREENW-1)] |= (1<<rowBit);
  } else {
    screen_contents[x][rowByte] &= ~(1<<rowBit);    
    screen_contents[x][rowByte] &= ~(1<<rowBit);    
  }
}


// Internal function: get value of the pixel at (x, y) in the
// screen buffer. value = 0 (off) or 1 (on)
int display_get_buffer_pixel(unsigned char x, unsigned char y) {
  int rowByte = y / 8;
  int rowBit = y % 8;

  return (screen_contents[x][rowByte] & (1<<rowBit)) > 0;
}


