// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/************************************************************
 * user_input.c                                             *
 ************************************************************
 * User input routines                                      *
 * Joris Van Looveren, 02/2017                              *
 *                                                          *
 * This module provides a few routines to get input from    *
 * the user using the buttons on the prototype PCB.         *
 * These include getting a no/yes style confirmation (left- *
 * most or rightmost button), and a multi-character word.   *
 ************************************************************/

#include <string.h>

#include "hal_mcu.h"
#include "display.h"
#include "configuration.h"
#include "messages.h"

#include "user_input.h"

static char valid_string_chars[] =
  { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
    'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
    'w', 'x', 'y', 'z',
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K',
    'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
    'W', 'X', 'Y', 'Z',
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
    '!', ',', '-', '.', '/', '<', '=', '>', '?', '@', ':', ';' };
static int num_valid_string_chars = sizeof(valid_string_chars) / sizeof(char);

#ifdef __TEST__
#define MAX_SHIFT_DELAY 30
#else
#define MAX_SHIFT_DELAY 12
#endif


void user_input_get_string(char *inputstring, int max_inputstring_length) {
  int inputstring_complete = 0;

  memset(&inputstring[0], 0, max_inputstring_length);
  inputstring[0] = valid_string_chars[0];
  int char_index = 0;
  int cursor_position = 0;
  int shift_state = 0;

  // For starters, we assume that we can display the whole string
  int string_len = strlen(inputstring);
  // display_start_index holds the index of the left-most
  // character of the string that's currently showing on the
  // display.
  int display_start_index = 0;
  // display_end_index holds the index of the right-most
  // character of the string that's currently showing on the
  // display.
  int display_end_index = string_len;

  display_update_status_bar_keys(MSG_INPUT_STATUS_UP,
				 MSG_INPUT_STATUS_DOWN,
				 MSG_INPUT_STATUS_NEXT,
				 MSG_INPUT_STATUS_MORE);
  
  while (!inputstring_complete) {
    // Print the string on the screen.
    display_clear_buffer_region(0, 20, SCREENW, 28);
    
    string_len = strlen(inputstring);

    // Part 1: determine which part of the string to show on the display
    int current_width_in_pixels = 
      display_calculate_substring_length(&inputstring[0],
					 display_start_index,
					 display_end_index);
    // this will contain the length in pixels of the substring demarcated
    // by display_start_index and display_end_index
    int string_display_width_in_pixels = 0;
    
    if (cursor_position < display_start_index) {
      // The user moved the cursor to the left, and its current position
      // is LESS than the left-most character showing on the screen.
      // This means we have to scroll the string to the right, so the
      // "previous" character will be shown as the left-most one.
      display_start_index -= 1;

      // From here, add characters to the displayed part, as long as
      // the displayed part still fits on the display.
      display_end_index = display_start_index;
      string_display_width_in_pixels = 0;
      while ((string_display_width_in_pixels < SCREENW-15) &&
	     (display_end_index < string_len)){
	display_end_index += 1;
	string_display_width_in_pixels =
	  display_calculate_substring_length(&inputstring[0],
					     display_start_index,
					     display_end_index);
      }
    } else if ((cursor_position >= display_end_index) &&
               (current_width_in_pixels >= SCREENW-15)) {
      // The user moved the cursor to the right, and its current position
      // is MORE than the right-most character showing on the screen.
      // This means we have to scroll the string to the left, so the
      // "next" character will be shown as the right-most one.
      display_end_index += 1;
      
      // From here, "backwards"-add characters to the displayed part,
      // as long as the displayed part still fits on the display.
      display_start_index = display_end_index;
      string_display_width_in_pixels = 0;
      while (string_display_width_in_pixels < SCREENW-15) {
	display_start_index -= 1;
	string_display_width_in_pixels =
	  display_calculate_substring_length(&inputstring[0],
					     display_start_index,
					     display_end_index);
      }
    } else {
      // Cursor somewhere in the middle. We don't modify the start and
      // end indices; only recalculate the length. We can probably
      // optimize this to skip the calculation unless needed.
      display_end_index = display_start_index;
      string_display_width_in_pixels = 0;
      while ((string_display_width_in_pixels < SCREENW-15) &&
	     (display_end_index < string_len)){
	display_end_index += 1;
	string_display_width_in_pixels =
	  display_calculate_substring_length(&inputstring[0],
					     display_start_index,
					     display_end_index);
      }
    }

    // Part 2: display the string.
    // Show a "<" if the left-most character shown is not at index '0'
    if (display_start_index > 0) {
      display_setcursor(1,21);
      display_print(MSG_INPUT_STATUS_LEFT);
    }
    // Show the characters from display_start_index to display_end_index
    // We print it character by character. At the cursor position, we
    // invert the character to show the cursor.
    display_setcursor(7,21);
    for (int i=display_start_index; i<display_end_index; i++) {
      char ch[2] = { '\0', '\0' };
      ch[0] = inputstring[i];
      int len = display_calculate_string_length(ch);
      display_print(&ch[0]);
      if (cursor_position == i) {
	int x = display_getcursor_x();
	int y = display_getcursor_y();
	display_invert_buffer_region(x-len-1, y-1, x, y+7);
      }
    }
    // Show a ">" if the string is longer than the part we can show
    // on the display.
    if ((string_display_width_in_pixels > SCREENW-16) &&
	(display_end_index < string_len)) {
      display_setcursor(SCREENW-5,21);
      display_print(MSG_INPUT_STATUS_RIGHT);
    }
    // Part 3: send the updated string to the display.
    display_draw_buffer();
    
    // Wait for button press.
    // If the current shift state is 1, we count the iterations where
    // no button is pressed. If we reach the threshold, we switch back
    // to shift state 0.
    int button1 = 0, button2 = 0, button3 = 0, button4 = 0;
    int shift_delay = 0;
    do {
      mcu_delay(75000);
      shift_delay += 1;
      button1 = button_check(BUTTON_1);
      button2 = button_check(BUTTON_2);
      button3 = button_check(BUTTON_3);
      button4 = button_check(BUTTON_4);
    } while (!(button1 || button2 || button3 || button4 ||
	       ((shift_state == 1) && (shift_delay > MAX_SHIFT_DELAY))));

    // Handle the button presses.
    if (shift_delay > MAX_SHIFT_DELAY) {
      shift_state = 0;
      //display_update_status_bar(" ^   `   next   ...");
      display_update_status_bar_keys(MSG_INPUT_STATUS_UP,
				     MSG_INPUT_STATUS_DOWN,
				     MSG_INPUT_STATUS_NEXT,
				     MSG_INPUT_STATUS_MORE);
    } else {
      switch(shift_state) {
      case 0:
	if (button1) {
	  // Scroll backwards in the character list
	  if (char_index == 0) {
	    char_index = num_valid_string_chars - 1;
	  } else {
	    char_index = (char_index - 1) % num_valid_string_chars;
	  }
	  inputstring[cursor_position] = valid_string_chars[char_index];
	} else if (button2) {
	  // Scroll forward in the character list
	  char_index = (char_index + 1) % num_valid_string_chars;
	  inputstring[cursor_position] = valid_string_chars[char_index];
	} else if (button3) {
	  // Confirm the currently shown character;
	  // if we will not exceed the max. length: move to
	  // next position and fill in the "starting" character
	  if (cursor_position < max_inputstring_length-2) {
	    cursor_position += 1;
	    inputstring[cursor_position] = valid_string_chars[char_index];
	  }
	} else if (button4) {
	  // Shift to alternate button modes
	  shift_state = 1;
	  //display_update_status_bar("<  >  del  done");
	  display_update_status_bar_keys(MSG_INPUT_STATUS_LEFT,  MSG_INPUT_STATUS_RIGHT,  MSG_INPUT_STATUS_DEL, MSG_INPUT_STATUS_DONE);
	}
	break;
      case 1:
	if (button1) {
	  // Move cursor to previous position
	  if (cursor_position > 0) {
	    cursor_position -= 1;
	  }
	} else if (button2) {
	  // Move cursor to next position  
	  if (cursor_position < strlen(inputstring)-1) {
	    cursor_position += 1;
	  }
	} else if (button3) {
	  // Delete current character; shift rest of string backwards
	  int orig_len = strlen(inputstring);
	  if ((cursor_position < orig_len-1) &&
	      (cursor_position >= 0)) {
	    for (int i=cursor_position; i<orig_len-1; i++) {
	      inputstring[i] = inputstring[i+1];
	    }
	    inputstring[orig_len-1] = '\0';
	  }
	} else if (button4) {
	  // Input string complete; confirm and continue
	  inputstring_complete = 1;
	}
	break;
      }
    }
  }
#ifndef NO_TERM
  term_print("String read: '");
  term_print(inputstring);
  term_print("'\r\n");
#endif
}


int user_input_confirm() {
  int confirmed = 0;
  // wait 1 sec or so to make sure the button is no longer pressed
  mcu_delay(1000000); 
      
  int button1 = 0, button4 = 0;
  do {
    mcu_delay(75000);
    button1 = button_check(BUTTON_1);
    button4 = button_check(BUTTON_4);
  } while (!(button1 || button4));
  if (button4) {
    confirmed = 1;
  }
  return confirmed;
}


static inline int min(int a, int b) {
  return ((a <= b) ? a : b);
}



// Select from list uses the region (0,7)-(84,41) on the screen
int user_input_select_from_list(char **list, int entries, int entry_length, int selected_entry) {
  int selected = -1;
  int current_selection = (selected_entry >= 0)?selected_entry:0;
  int current_displayed_top_item =
    (current_selection >= LIST_ITEMS)?(current_selection-(LIST_ITEMS-1)) :0;

  display_update_status_bar_keys(MSG_INPUT_STATUS_UP, MSG_INPUT_STATUS_DOWN, MSG_INPUT_STATUS_BACK, MSG_INPUT_STATUS_SELECT);
  while (selected == -1) {
    // Display the visible list items. We have at most 5 lines to display
    // items, so if we have more items, we need to scroll the list.
    // The current selection is "highlighted" (i.e., inverted)
    display_clear_buffer_region(LIST_LEFT,
				LIST_TOP,
				LIST_RIGHT,
				LIST_TOP+(LIST_ITEM_HEIGHT*LIST_ITEMS)+1);
    // Display "up" arrow if we can scroll up
    if (current_displayed_top_item > 0) {
      display_setcursor(LIST_RIGHT-5, LIST_TOP);
      display_print(MSG_INPUT_STATUS_UP);
    }
    // Display "down" arrow if we can scroll down
    if (current_displayed_top_item + LIST_ITEMS < entries) {
      display_setcursor(LIST_RIGHT-5, LIST_TOP+((LIST_ITEMS-1)*LIST_ITEM_HEIGHT)+1);
      display_print(MSG_INPUT_STATUS_DOWN);
    }
    // Draw item text
    for (int i=0; i<min(entries, LIST_ITEMS); i++) {
      display_setcursor(1, (LIST_TOP+1)+(i*LIST_ITEM_HEIGHT));
      display_print(list[current_displayed_top_item + i]);
      display_print("\n");
    }
    // Highlight the current selection
    display_invert_buffer_region(LIST_LEFT,
				 (LIST_TOP+1)+(current_selection-current_displayed_top_item)*LIST_ITEM_HEIGHT-1,
				 LIST_RIGHT,
				 (LIST_TOP+LIST_ITEM_HEIGHT)+(current_selection-current_displayed_top_item)*LIST_ITEM_HEIGHT+1);
    // Update the screen
    display_draw_buffer();

    // Listen for button presses
    // wait for button press.
    // S1 = confirm char, S2, S3 = cycle, S4 = confirm PW, S5 = quit
    int button1 = 0, button2 = 0, button3 = 0, button4 = 0, button5 = 0;
    do {
      mcu_delay(100000);
      button1 = button_check(BUTTON_1);
      button2 = button_check(BUTTON_2);
      button3 = button_check(BUTTON_3);
      button4 = button_check(BUTTON_4);
      button5 = button_check(BUTTON_5);
    } while (!(button1 || button2 || button3 || button4 || button5));

    if (button5) {
      return -2;
    } else if (button4) {
      // make sure selected > 0, otherwise the while loop doesn't stop!
      selected = current_selection;
    } else if (button3) {
      return -1;
    } else if (button2) {
      current_selection += 1;
      if (current_selection - current_displayed_top_item >= LIST_ITEMS) {
	current_displayed_top_item += 1;
      }
      if (current_selection >= entries) {
	current_selection = 0;
	current_displayed_top_item = 0;
      }
    } else if (button1) {
      current_selection -= 1;
      if (current_selection < 0) {
	current_selection = entries-1;
	if (entries >= LIST_ITEMS) {
	  current_displayed_top_item = entries - LIST_ITEMS;
	}
      }
      if (current_selection < current_displayed_top_item) {
	current_displayed_top_item -= 1;
      }
    }

  }

  return selected;
}


