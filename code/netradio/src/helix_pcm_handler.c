// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/************************************************************
 * helix_pcm_handler.c                                      *
 ************************************************************
 * PCM buffer callbacks                                     *
 * Joris Van Looveren, 12/2016                              *
 *                                                          *
 * (See the .h file header for an explanation.)             *
 ************************************************************/

#include <inttypes.h>
#include <string.h>   // memcpy
#include <stdlib.h>

#include "configuration.h"
#include "helix_pcm_handler.h"
#include "audio.h"
#include "player.h"


// prototypes
void clear_current_frame_cb(void);
void frame_start_cb(void);
void frame_end_cb(void);
int16_t *get_block_buffer_cb(int stereo);
void return_block_buffer_cb(int16_t *buf, int stereo);

// 2 small local buffers
#define NBANDS 32
int16_t samples_buf[NBANDS*2];
int16_t transformed_samples_buf[NBANDS*2];


void init_helix_pcm_handler(void) {
  buf_callbacks.frame_start_callback = frame_start_cb;
  buf_callbacks.frame_end_callback = frame_end_cb;
  buf_callbacks.clear_current_frame_callback = clear_current_frame_cb;
  buf_callbacks.get_buffer_callback = get_block_buffer_cb; 
  buf_callbacks.return_buffer_callback = return_block_buffer_cb;
}


void clear_current_frame_cb(void) {
  // this clears more than just the current frame...
  audio_buffer_clear();
}


void frame_start_cb(void) {
}


void frame_end_cb(void) {
}


inline static int space_left() {
  return OUTPUT_BUFFER_SIZE - audio_buffer_lead();
}

// Return a buffer to Helix where it can place audio samples.
// This callback blocks until the audio-out buffer effectively has
// room to store the samples that will be put in this buffer.
int16_t *get_block_buffer_cb(int stereo) {
  // wait until there is enough space available in the buffer for the next block.
  int space_available = 0;
  while (space_left() < NBANDS*(stereo+1));
  // zero the block before returning it
  memset(samples_buf, 0, (NBANDS*(stereo+1))*sizeof(int16_t));
  return samples_buf;
}

// Transfer samples from the small Helix buffer to the actual audio-out
// buffer. If we're not playing yet, Helix output is discarded. Otherwise,
// the 16-bit samples are transformed to 12 bits.
void return_block_buffer_cb(int16_t *buf, int stereo) {
  // only copy even samples if stereo; all samples if mono
  int offset = (stereo == BUF_STEREO)?2:1;
  for (int in_index=0, out_index=0; in_index<NBANDS*offset; in_index+=offset, out_index++) {
    // Samples from Helix are signed (i.e., 0-centered), 16 bits
    // Our DAC requires unsigned, 12 bits, i.e. 2048-centered, with values 0..4096
    if (is_playing()) {
      transformed_samples_buf[out_index] = (samples_buf[in_index] >> 4) + 2048;
    } else {
      transformed_samples_buf[out_index] = SILENCE;
    }
  }
  // for our mono prototype setup, we always pass NBANDS samples
  audio_buffer_store_block(transformed_samples_buf, NBANDS, BUFFER_BLOCK);
}
