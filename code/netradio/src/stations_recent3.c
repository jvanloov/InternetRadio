// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/***********************************************
 * stations_recent3.c                          *
 ***********************************************
 * Keep the 3 most recent station names.       *
 * Joris Van Looveren                          *
 ***********************************************
 * This is used for the most_recent_plugins in *
 * the menu system, to keep track of the 3 most*
 * recently played stations. They will be made *
 * available as "speed dial" selections in the *
 * main menu.                                  *
 ***********************************************/

#include <stdlib.h>
#include <string.h>

#include "hal_mcu.h"
#include "stations_recent3.h"

// One entry in the recent stations list.
// This consists of a name (to be looked up in the global
// station list), and a rank.
// The rank keeps track of how many "selections" ago
// this station was selected. Whenever a new station is
// selected, the ranks are increased. 1 is the most
// recent station, 3 is the least recently selected
// station.
typedef struct {
  char* name;
  int rank;
} RECENT_STATION;

// The station list
static RECENT_STATION recent3_stations[3];

// Load the 3 stations from the file designated by "file_name".
FILE_STATE stations_recent3_load(const char* file_name,
				 RECENT_STATION *station1,
				 RECENT_STATION *station2,
				 RECENT_STATION *station3
				 ) {
  FILE_HANDLE stations_selected_file;
  FILE_STATE status;
  char line_buf[150];
  int chars_read = 0;
  char* names[3] = { NULL, NULL, NULL };
  int ranks[3] = { 1, 2, 3 };
  
  memset(&(line_buf[0]), 0, 150);
  status = file_open_for_read(file_name, &stations_selected_file);
  if (status == FILE_OK) {
    status = file_read_block(&stations_selected_file, line_buf, 148, &chars_read);
    if (status == FILE_OK) {
      char* newline = NULL;
      int len = -1, start_pos = 0, idx = 0;

      line_buf[chars_read] = '\n';
      while (start_pos < chars_read) {
	newline = strchr(&(line_buf[start_pos]), '\n');
	if (newline != NULL) {
	  len = newline - &(line_buf[start_pos]);
	} else {
	  len = chars_read - start_pos;
	}
	if (len > 0) {
	  char* line = (char*) malloc(len+1);
	  memset(&(line[0]), 0, len+1);
	  memcpy(&(line[0]), &(line_buf[start_pos]), len);
		 
	  char* separator_pos = strchr(line, '|');
	  if (separator_pos == NULL) {
	    names[idx] = (char*) malloc(len+1);
	    memset(&(*names[idx]), 0, len+1);
	    memcpy(&(*names[idx]), &(line[0]), len);
	  } else {
	    // we assume 1 character rank + 1 character separator
	    names[idx] = (char*) malloc(len-1);
	    memset(&(*names[idx]), 0, len-1);
	    memcpy(&(*names[idx]), &(line[2]), len-2);
	    ranks[idx] = (line[0] - '0');
	    term_print("Favorite name: ");
	    term_print(names[idx]);
	    term_print_val("; rank: ", ranks[idx]);
	  }
	  free(line);
	}
#if (!defined NO_TERM) && (!defined __TEST__)
	term_print("Load selected station name: ");
	term_print(names[idx]);
	term_print("\r\n");
#endif
	idx++;
	start_pos = start_pos + len + 1;
      }

      station1->name = names[0];
      station1->rank = ranks[0];
      station2->name = names[1];
      station2->rank = ranks[1];
      station3->name = names[2];
      station3->rank = ranks[2];
      
      status = file_close(&stations_selected_file);
    }
  }
  return status;
}

// Save the station names to the file designated by "file_name".
// We expect to write at least one station name/rank. station2 and
// station3 can be NULL.
FILE_STATE stations_recent3_save(const char* file_name,
				 RECENT_STATION* station1,
				 RECENT_STATION* station2,
				 RECENT_STATION* station3
				 ) {
  FILE_HANDLE stations_selected_file;
  FILE_STATE status;
  char line_buf[100];
  int chars_read = 0;

#if (!defined NO_TERM) && (!defined __TEST__)
  term_print("Saving selected station names: ");
  term_print(station1->name);
  term_print(" (#");
  term_print_int(station1->rank);
  term_print(")");
  if (station2->name != NULL) {
    term_print(", ");
    term_print(station2->name);
    term_print(" (#");
    term_print_int(station2->rank);
    term_print(")");
  }
  if (station3->name != NULL) {
    term_print(", ");
    term_print(station3->name);
    term_print(" (#");
    term_print_int(station3->rank);
    term_print(")");
  }
  term_print("\r\n");
#endif

  status = file_open_for_write(file_name, &stations_selected_file);
  if (status == FILE_OK) {
    char rank_str[2];
    rank_str[1] = '|';

    rank_str[0] = '0' + station1->rank;
    status = file_write_block(&stations_selected_file, rank_str, 2);
    status = file_write_block(&stations_selected_file, station1->name, strlen(station1->name));
    if (station2->name != NULL) {
      status = file_write_block(&stations_selected_file, "\n", 1);
      rank_str[0] = '0' + station2->rank;
      status = file_write_block(&stations_selected_file, rank_str, 2);
      status = file_write_block(&stations_selected_file, station2->name, strlen(station2->name));
    }
    if (station3->name != NULL) {
      status = file_write_block(&stations_selected_file, "\n", 1);
      rank_str[0] = '0' + station3->rank;
      status = file_write_block(&stations_selected_file, rank_str, 2);
      status = file_write_block(&stations_selected_file, station3->name, strlen(station3->name));
    }
    if (status == FILE_OK) {
      status = file_close(&stations_selected_file);
    }
  }
  return status;  
}


void stations_recent3_init(const char* file_name) {
  FILE_STATE state;
  
  recent3_stations[0].name = NULL;
  recent3_stations[0].rank = 0;
  recent3_stations[1].name = NULL;
  recent3_stations[1].rank = 0;
  recent3_stations[2].name = NULL;
  recent3_stations[2].rank = 0;

  if (file_name != NULL) {
    state = stations_recent3_load(file_name,
				  &(recent3_stations[0]),
				  &(recent3_stations[1]),
				  &(recent3_stations[2]));
  } 
}


void stations_recent3_add(const char* file_name, char* station_name_orig) {
  int index, index2;
  int found = 0;

  // Check if the station is already in the recent3 list.
  // If so, we don't need to do anything and can return immediately.
  for (index=0; index<3; index++) {
    if (recent3_stations[index].name != NULL) {
      if (strcmp(recent3_stations[index].name, station_name_orig) == 0) {
	found = 1;
      }
    }
  }
  if (found) {
    return;
  }  

  // if we don't have 3 entries yet: add to the list.
  // if we have 3 entries: replace the least-recently used one
  // 1: adapt ranks: 1->2, 2->3, new station = 1
  int added = 0;
  for (index=0; index<3; index++) {
    if (recent3_stations[index].name != NULL) {
      if (recent3_stations[index].rank <= 2) {
	recent3_stations[index].rank += 1;
	term_print_val(recent3_stations[index].name, recent3_stations[index].rank);
      } else {
	int len = strlen(station_name_orig) + 1;
	// free the name of the station that falls off the list
	free(recent3_stations[index].name);
	// allocate space for the new name and copy it
	recent3_stations[index].name = (char*) malloc(len);
	memset(recent3_stations[index].name, 0, len);
	strcpy(recent3_stations[index].name, station_name_orig);
	
	recent3_stations[index].rank = 1;
      }
    } else if (!added) {  // add in first empty spot
      int len = strlen(station_name_orig) + 1;
      // allocate space for the new name and copy it
      recent3_stations[index].name = (char*) malloc(len);
      memset(recent3_stations[index].name, 0, len);
      strcpy(recent3_stations[index].name, station_name_orig);
      
      recent3_stations[index].rank = 1;
      added = 1;  // don't add it again in the next iteration
    }
  }
  
  // 2: sort the list
  // only 3 elements in the list: do a naive bubble sort.
  for (index2=0; index2<2; index2++) {
    for (index=0; index<2-index2; index++) {
      if ((recent3_stations[index].name != NULL) &&
	  (recent3_stations[index+1].name != NULL)) {
	int comparison_result = strcmp(recent3_stations[index].name,
				       recent3_stations[index+1].name); 
	if (comparison_result > 0) {
	  char* temp_name = recent3_stations[index].name;
	  int temp_rank = recent3_stations[index].rank;
	  recent3_stations[index].name = recent3_stations[index+1].name;
	  recent3_stations[index+1].name = temp_name;
	  recent3_stations[index].rank = recent3_stations[index+1].rank;
	  recent3_stations[index+1].rank = temp_rank;
	}
      }
    }
  }
  
  // 3: save the updated list
  if (file_name != NULL) {
    stations_recent3_save(file_name,
			  &(recent3_stations[0]),
			  &(recent3_stations[1]),
			  &(recent3_stations[2]));
  }
}


char *stations_recent3_get(int pos) {
  switch (pos) {
  case 1: return recent3_stations[0].name;
  case 2: return recent3_stations[1].name;
  case 3: return recent3_stations[2].name;
  }
  return NULL;
}

void stations_recent3_print(void) {
#if (!defined NO_TERM) && (!defined __TEST__)  
  term_print("Recent 3 stations:\r\n");
  term_print(recent3_stations[0].name);
  term_print(" (");
  term_print_int(recent3_stations[0].rank);
  term_print("), ");
  term_print(recent3_stations[1].name);
  term_print(" (");
  term_print_int(recent3_stations[1].rank);
  term_print("), ");
  term_print(recent3_stations[2].name);
  term_print(" (");
  term_print_int(recent3_stations[2].rank);
  term_print(")\r\n");
  term_print("----------------\r\n");
#endif
}

void stations_recent3_cleanup(void) {
#if (!defined NO_TERM) && (!defined __TEST__)
  term_print("Cleanup recent stations.\r\n");
#endif
  free(recent3_stations[0].name);
  free(recent3_stations[1].name);
  free(recent3_stations[2].name);
}
