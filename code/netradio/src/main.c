// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/************************************************************
 * main.c                                                   *
 ************************************************************
 * Main loop for the netradio implementation.               *
 ************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "socket.h"  // from simplelink SDK

#include "hal_mcu.h"
#include "display.h"
#include "user_input.h"

#include "simplelink.h"
#include "network.h"

#include "audio.h"
#include "streaming_callbacks.h"
#include "stations_data.h"
#include "misc_utils.h"
#include "player.h"

#include "menu.h"
#include "ui-plugin.h"
#include "station_list_plugin.h"
#include "system_menu_plugin.h"
#include "most_recent_plugin.h"
#include "stations_recent3.h"

#include "messages.h"
#include "logo.h"

static STATION_DATA global_station_data;


void stations_show(STATION_DATA* global_station_data) {
#ifndef NO_TERM  
  STATION *favorites;
  int num_favorites;

  term_print("Category '");
  term_print(global_station_data->category_name);
  term_print("':\r\n");
  stations_get_stations(global_station_data, &favorites, &num_favorites);
  for (int i=0; i<num_favorites; i++) {
    term_print("* ");
    term_print(favorites[i].short_name);
    term_print(" (");
    term_print(favorites[i].ip);
    term_print(") [");
    term_print(favorites[i].url);
    term_print("]\r\n");
  }
#endif
}


int main(void) {
  long result;
  
  board_init();
  hal_init();
  
#ifndef NO_TERM
  uart_init();
  term_init(115200);
#endif
  led_init();
  button_init();
  amplifier_init();
  
#ifndef NO_TERM
  term_print("Welcome to Internet Radio!\r\n");
  term_print("Starting...\r\n");
#endif
  display_init();
  display_initialize();
  display_set_backlight_brightness(3);

  display_clear_buffer();
  display_draw_bitmap(44, 20, logo, 40, 22);
  display_draw_buffer();
  
  network_init();
  network_connect_or_select();

  mcu_delay(500000);

  FILE_STATE status = FILE_OK;

  stations_init(&global_station_data, GLOBAL_STATIONS_ENTRIES);

  status = stations_load(&global_station_data, GLOBAL_STATIONS_FILE);

  if (status == FILE_OK) {
#ifndef NO_TERM
    term_print("Show favorites\r\n");
#endif  
    stations_show(&global_station_data);
  } else {
    display_clear();
    display_print(MSG_LOADING_SL_COULD_NOT_LOAD);
    display_print(GLOBAL_STATIONS_FILE "\n\n");
    display_print(MSG_LOADING_SL_CANNOT_CONTINUE);
    display_draw_buffer();
#ifndef NO_TERM
    term_print("Unable to load favorites; halting\r\n");
#endif
    hal_cleanup();
    return 1;
  }

  mcu_delay(500000);

  stations_recent3_init(SELECTED_FILE);
  
  mcu_delay(100000);

  UI_PLUGIN_API most_recent_plugin;
  UI_PLUGIN_DATA most_recent_data1;
  UI_PLUGIN_DATA most_recent_data2;
  UI_PLUGIN_DATA most_recent_data3;
  UI_PLUGIN_API station_list_plugin;
  UI_PLUGIN_DATA station_list_data;
  UI_PLUGIN_API system_menu_plugin;
  UI_PLUGIN_DATA system_menu_data;

  // 3 most_recent_plugin instances.
  // We keep a "global" list of the 3 most recently played stations.
  // Each most_recent_plugin shows the station at a different index in
  // this list. This way, we get the 3 most recent stations in the main
  // menu as "quick links".
  most_recent_plugin_setup(&most_recent_plugin, &most_recent_data1, 1);
  most_recent_plugin_setup(&most_recent_plugin, &most_recent_data2, 2);
  most_recent_plugin_setup(&most_recent_plugin, &most_recent_data3, 3);
  // station_list_plugin has a sub-menu with the global list of stations. 
  station_list_plugin_setup(&station_list_plugin, &station_list_data);
  // system_menu_plugin has a sub-menu with system options.
  system_menu_plugin_setup(&system_menu_plugin, &system_menu_data);

  // Initialize the main menu.
  // We allocate the memory for the main menu. The items themselves are
  // not filled here; we re-populate the main menu every time the user
  // returns to the main menu. This allows e.g. to update the entries
  // for the most recent stations.
  MENU main_menu;
  main_menu.title = MSG_MAIN_MENU;
  main_menu.parent_menu = NULL;
  main_menu.num_entries = 5;
  main_menu.entry_names =
    (char**) malloc(main_menu.num_entries * sizeof(char*));
  main_menu.entry_action_types =
    (menu_action_type_fun*) malloc(main_menu.num_entries *
				   sizeof(menu_action_type_fun));
  main_menu.entry_actions =
    (menu_action_fun*) malloc(main_menu.num_entries *
			      sizeof(menu_action_fun));
  main_menu.entry_data =
    (void**) malloc(main_menu.num_entries * sizeof(void*));
  main_menu.current_selection = -1;

  // Menus are hierarchical, with the main menu being the "root" of the
  // hierarchy. The main menu is the only menu that will have NULL in its
  // parent field.
  MENU* current_menu = NULL;
  current_menu = &main_menu;

  // The main loop.
  // In principle, we'll never exit this loop; however, when running in
  // simulation mode, we do have a way to exit the program and properly
  // clean up the memory.
  int _continue = 1;
  while (_continue) {
    most_recent_plugin.initialize(most_recent_data1, &global_station_data, &main_menu);
    most_recent_plugin.initialize(most_recent_data2, &global_station_data, &main_menu);
    most_recent_plugin.initialize(most_recent_data3, &global_station_data, &main_menu);
    station_list_plugin.initialize(&station_list_data, &global_station_data, &main_menu);
    system_menu_plugin.initialize(&system_menu_data, &global_station_data, &main_menu);

    // Refresh the main menu items.
    // A menu item, and its action function, might have changed since the
    // last time. For example, if a menu item shows the last played station,
    // this should be updated.
    // The action type will indicate whether the menu item leads to a submenu,
    // or if an action has to be performed. The action is the function that
    // will be executed IF the action type is a MENU_ACTION.
    main_menu.entry_names[0] = most_recent_plugin.get_menuitem(most_recent_data1);
    main_menu.entry_action_types[0] = most_recent_plugin.get_menuitemactiontype;
    main_menu.entry_actions[0] = most_recent_plugin.get_menuitemaction;
    main_menu.entry_data[0] = most_recent_data1;
    main_menu.entry_names[1] = most_recent_plugin.get_menuitem(most_recent_data2);
    main_menu.entry_action_types[1] = most_recent_plugin.get_menuitemactiontype;
    main_menu.entry_actions[1] = most_recent_plugin.get_menuitemaction;
    main_menu.entry_data[1] = most_recent_data2;
    main_menu.entry_names[2] = most_recent_plugin.get_menuitem(most_recent_data3);
    main_menu.entry_action_types[2] = most_recent_plugin.get_menuitemactiontype;
    main_menu.entry_actions[2] = most_recent_plugin.get_menuitemaction;
    main_menu.entry_data[2] = most_recent_data3;
    main_menu.entry_names[3] = station_list_plugin.get_menuitem(&station_list_data);
    main_menu.entry_action_types[3] = station_list_plugin.get_menuitemactiontype;
    main_menu.entry_actions[3] = station_list_plugin.get_menuitemaction;
    main_menu.entry_data[3] = station_list_data;
    main_menu.entry_names[4] = system_menu_plugin.get_menuitem(&system_menu_data);
    main_menu.entry_action_types[4] = system_menu_plugin.get_menuitemactiontype;
    main_menu.entry_actions[4] = system_menu_plugin.get_menuitemaction;
    main_menu.entry_data[4] = system_menu_data;
  
    display_clear_buffer();
    display_print(current_menu->title);
    display_draw_buffer();

    int selection = user_input_select_from_list(
			      current_menu->entry_names,
			      current_menu->num_entries,
			      20,
			      current_menu->current_selection);
    term_print_val("selection: ", selection);
    if (selection == -1) {
      if (current_menu->parent_menu != NULL) {
	current_menu = current_menu->parent_menu;
	selection = current_menu->current_selection;
	term_print("Cancelled; back to parent menu.\r\n");
      } else {
	term_print("Already in top-level menu; cannot go to parent menu.\r\n");
      }
    } else if (selection == -2) {
      _continue = 0;
    } else {
      UI_PLUGIN_DATA data = NULL;
      
      current_menu->current_selection = selection;
      int action_type = current_menu->entry_action_types[selection]();

      switch (action_type) {
      case MENU_SUBMENU: ;
	data = (UI_PLUGIN_DATA) current_menu->entry_data[selection];
	MENU* new_current_menu =
	  (MENU*) current_menu->entry_actions[selection](data, selection);
	new_current_menu->parent_menu = current_menu;
	current_menu = new_current_menu;
	term_print("Submenu: ");
	term_print(current_menu->title);
	term_print("\r\n");
	break;
      case MENU_ACTION: ;
	data = (UI_PLUGIN_DATA) current_menu->entry_data[selection];
	current_menu->entry_actions[selection](data, selection);
	break;
      }
      
      display_draw_buffer();

      mcu_delay(100000);
    }
     
    most_recent_plugin.cleanup(most_recent_data1);
    most_recent_plugin.cleanup(most_recent_data2);
    most_recent_plugin.cleanup(most_recent_data3);
    // clean up station list after most recent plugins, because
    // most recent plugins have pointers to station list data.
    station_list_plugin.cleanup(station_list_data);
    system_menu_plugin.cleanup(system_menu_data);
  }

  most_recent_plugin.terminate(most_recent_data1);
  most_recent_plugin.terminate(most_recent_data2);
  most_recent_plugin.terminate(most_recent_data3);
  station_list_plugin.terminate(station_list_data);
  system_menu_plugin.terminate(system_menu_data);
  
  free(main_menu.entry_names);
  free(main_menu.entry_action_types);
  free(main_menu.entry_actions);
  free(main_menu.entry_data);

  stations_recent3_cleanup();
  stations_destroy(&global_station_data);
  hal_cleanup();

  return 1;
}
