// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/************************************************************
 * audio.c                                                  *
 ************************************************************
 * Audio driver for CC3200 and MCP4921 DAC                  *
 * Joris Van Looveren, 10/2016                              *
 *                                                          *
 ************************************************************
 * CC3200 SPI BoosterPack connections:                      *
 *   - -     - -                                            *
 *   - -     - -                                            *
 *   3 -     - -                                            *
 *   - -     - -                                            *
 *   - -     - -                                            *
 *   - -     - 15                                           *
 *   7 -     - 14                                           *
 *   - -     - -                                            *
 *   - -     - -                                            *
 *   - -     - -                                            *
 *                                                          *
 * BP Pin   Function   DriverLib designation                *
 *   3    = SPI CS   = "PIN_08"                             *
 *   7    = SPI CLK  = "PIN_05"                             *
 *  14    = SPI MISO = "PIN_06"                             *
 *  15    = SPI MOSI = "PIN_07"                             *
 ************************************************************/

#include <string.h>
#include <stdlib.h>
#include <stdint.h>

#include "configuration.h"
#include "audio.h"
#include "hal_mcu.h"
#include "circular_data_buffer.h"


//** Sample buffer *****************************************
// This buffer holds output samples. The buffer is filled by
// the decoder, by calling audio_buffer_store_block, and is
// emptied by the audio output ISR, reading samples one-by-one
// using audio_buffer_read
char* sample_buffer = NULL;
// State of the player (STATE_STOPPED, STATE_PLAYING)
char state;
// The circular buffer implements the buffering logic. It
// uses the sample_buffer for storage.
circular_data_buffer audio_buffer;
//**********************************************************

void audio_buffer_clear() {
  if (!(state == STATE_STOPPED)) {
    audio_stop();
  }
  circular_data_buffer_clear(&audio_buffer);
}


// The audio buffer lead is the number of playable samples left in the
// buffer. Since a sample is 2 bytes, the number is exactly half the
// buffer usage (= the number of unread bytes).
int audio_buffer_lead() {
  return circular_data_buffer_usage(&audio_buffer) / sizeof(uint16_t);
}


int audio_buffer_store(uint16_t sample, int block) {
  // Write 2 consecutive bytes from the 'sample' 16-bit int into the buffer.
  // We pretend 'sample' is a 2-byte char buffer to do this without additional operations.
  return audio_buffer_store_block(&sample, 1, block);
}


int audio_buffer_store_block(uint16_t *samples, int numSamples, int block) {
  //  if we're reaching full buffer
  if (audio_buffer_lead() > (OUTPUT_BUFFER_SIZE - numSamples - 1)) {
    if (block) {
      while (audio_buffer_lead() > (OUTPUT_BUFFER_SIZE - numSamples - 1));
    } else {
      return BUFFER_FULL;
    }
  }

  timer_interrupt_disable();
  // The 'samples' buffer contains a series of 2 consecutive bytes (in the shape of
  // 16-bit ints). We pretend 'samples' is a numSamples*2 long char buffer so we can
  // store all the data without additional operations.
  circular_data_buffer_store_block(&audio_buffer, (char*) &(samples[0]), numSamples*sizeof(uint16_t));
  timer_interrupt_enable();

  return BUFFER_OK;
}


// Read a sample from the buffer. This method must not stall;
// if there are no samples, return silence.
// This code is not wrapped in int disable/int enable, since
// it will be called from inside the interrupt handler
static uint16_t audio_buffer_read() {
  uint16_t sample;
  // Read 2 consecutive bytes in the 'sample' 16-bit int. We pretend 'sample'
  // is a 2-byte char buffer to do this without additional operations.
  if (circular_data_buffer_read_block(&audio_buffer, (char*) &sample, 2) != -1) {
    return sample;
  } else {
    return SILENCE;
  }
}



static void timer_handler(void);


void audio_setup(uint32_t sample_rate) {
  // We assume 80 MHz system clock
  uint32_t timer_val = 80000000 / sample_rate;
  // Set up the timer at the desired sample rate
  timer_setup(timer_val, timer_handler);
}

void audio_init(void) {
  // if a buffer is not yet allocated, do so.
  // a buffer may exist if audio_setup is called a second time to change the sample rate.
  if (sample_buffer == NULL) {
    sample_buffer = (char*) malloc(OUTPUT_BUFFER_SIZE*sizeof(uint16_t));
  }
  circular_data_buffer_init(&audio_buffer, &(sample_buffer[0]), OUTPUT_BUFFER_SIZE*sizeof(uint16_t));
  
  spi_setup();
  spi_enable();
}


void audio_start(void) {
  state = STATE_PLAYING;
  timer_enable();
  timer_interrupt_enable();
}

void audio_stop(void) {
  timer_interrupt_disable();
  timer_disable();
  state = STATE_STOPPED;
}
 
int audio_state(void) {
  return state;
}
 
 void audio_close(void) {
  if (sample_buffer != NULL) {
    free(sample_buffer);
    sample_buffer = NULL;
  }
}
 

// This handler seems to take +/- 8us to run
static void timer_handler(void) {
  unsigned long ulInts;
  char bytes[2];
  uint16_t sample;

  timer_interrupt_clear();

  sample = audio_buffer_read();
  // Our DAC is an MCP4921; every write to the chip is 2 bytes.
  // Byte 0:
  // - 4 contol bits: 0011 = DAC_A, unbuffered, gain 1x, output buffer enabled
  // - 4 data bits (bits 11-8 = 4 bits)
  bytes[0] = (3 << 4) | (sample >> 8);
  // Byte 1:
  // - 8 data bits (bits 7-0 = 8 bits)
  bytes[1] = sample & 0xFF;

  spi_send(bytes,2);
}
