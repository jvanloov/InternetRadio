// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/************************************************************
 * menu.c                                                   *
 ************************************************************
 * Joris Van Looveren, 02/2018                              *
 ************************************************************/

#include <stdlib.h>
#include <string.h>
#include "menu.h"
#include "misc_utils.h"

int menu_item_action_fun(void) {
  return MENU_ACTION;
}

int menu_item_submenu_fun(void) {
  return MENU_SUBMENU;
}

void menu_initialize(MENU* menu, const char* name, int number_of_items) {
  int menuname_len = (strlen(name)>20)?21:strlen(name)+1;
  char* menuname_copy = (char*) malloc(menuname_len);
  string_build(menuname_copy, menuname_len);
  string_add(name);
  
  menu->title = menuname_copy;
  menu->num_entries = number_of_items;
  menu->entry_names = (char**) malloc(menu->num_entries * sizeof(char*));
  menu->entry_data = (void*) malloc(menu->num_entries * sizeof(void*));
  menu->entry_actions = (menu_action_fun*) malloc(menu->num_entries * sizeof(menu_action_fun));
  menu->entry_action_types = (menu_action_type_fun*) malloc(menu->num_entries * sizeof(menu_action_type_fun));
  menu->current_selection = -1;

  memset(menu->entry_names, 0, menu->num_entries * sizeof(char*));
  memset(menu->entry_data, 0, menu->num_entries * sizeof(void*));
  memset(menu->entry_actions, 0, menu->num_entries * sizeof(menu_action_fun));
  memset(menu->entry_action_types, 0, menu->num_entries * sizeof(menu_action_type_fun));
}

void menu_terminate(MENU* menu) {
  free(menu->title);
  free(menu->entry_names);
  free(menu->entry_actions);
  free(menu->entry_action_types);
  free(menu->entry_data);
}

