// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/************************************************************
 * stations_data.c                                          *
 ************************************************************
 * Read and write station data to/from file                 *
 * Joris Van Looveren                                       *
 ************************************************************
 * Global station data:                                     *
 * stations_save and stations_load write and read station   *
 * data from the global station data file. This includes    *
 * the station name, the server IP and port, and the stream *
 * URL. This is the list of stations as displayed in the    *
 * "Station List" menu entry.                               *
 *                                                          *
 * Selected station name:                                   *
 * stations_selected_save and stations_selected_load write  *
 * and read a station name from a file.                     *
 ************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "configuration.h"
#include "stations_data.h"

#include "hal_mcu.h"
#include "misc_utils.h"

#define FSM_CAT_OR_ALIAS   0
#define FSM_IP             1
#define FSM_URL            2

#ifdef __TEST__
extern void itoa(int value, char* str, int base);
#endif

static int fsm_state = FSM_CAT_OR_ALIAS;

// This buffer is used to extract lines from the circular buffer
static char line_buffer[REMOTE_DATA_LINE_BUFFER_SIZE];
static int line_buffer_pos = 0;

// Initialize the station list data structure
// return: 1 on success, 0 on failure
int stations_init(STATION_DATA *station_data, int max_stations) {
  STATION* station_list = (STATION*) malloc(max_stations * sizeof(STATION));

  if (station_list == NULL) {
    return 0;
  }
  
  memset(&(station_data->category_name[0]), 0, STATION_ENTRY_NAME_LENGTH);
  memset(&(station_list[0]), 0, max_stations * sizeof(struct station_entry));
  station_data->stations = station_list;
  station_data->max_stations = max_stations;
  station_data->num_stations = 0;
  station_data->buffer = NULL;

  return 1;
}


void stations_destroy(STATION_DATA* station_data) {
  if ((station_data->buffer != NULL) &&
      (station_data->buffer->data_buffer != NULL)) {
#ifndef NO_TERM
    term_print("!! station_data data_buffer not null on destroy!\r\n");
#endif
    free(station_data->buffer->data_buffer);
  }
  free(station_data->stations);
}

// Read a chunk of text and store it into the internal circular buffer. 
int stations_read_chunk(STATION_DATA *station_data, const char *block, int block_size) {
  if (station_data->buffer != NULL) {
    int chars_left = circular_data_buffer_chars_left(station_data->buffer);
    //printf("chars_left: %d (required: %d)\n", chars_left, block_size);
    if (chars_left > block_size) {
      if (block_size > 0) {
	return (circular_data_buffer_store_block(station_data->buffer, block, block_size) != -1);
      } else {
	return 1;
      }
    } else {
      return 0;
    }
  } else {
    return 0;
  }
}

// Return 1 if there is a full line present in the buffer; return 0 otherwise
int stations_has_next_line(STATION_DATA *station_data) {
  if (station_data->buffer != NULL) {
    return (circular_data_buffer_contains_char(station_data->buffer, '\n') != -1);
  } else {
    return 0;
  }
}

// Try to read the next line from the data buffer.
// On success, return 1. The next line will be stored in line_buffer
// On failure, return 0. 
int stations_get_next_line(STATION_DATA *station_data, char *line_buffer, int *line_buffer_len) {
  if (station_data->buffer != NULL) {
    int full_line_read = 0;
    int temp_line_buffer_pos = 0;

    if (circular_data_buffer_contains_char(station_data->buffer, '\n') != -1) {
      // there's at least one line in the buffer
      int chars = circular_data_buffer_usage(station_data->buffer);
      // continue as long as our line buffer isn't full, and
      // we still have data left in the data buffer
      while ((temp_line_buffer_pos < REMOTE_DATA_LINE_BUFFER_SIZE-1) &&
	     (chars - temp_line_buffer_pos > 0) &&
	     !full_line_read) {
	char c = circular_data_buffer_read(station_data->buffer);
	if (c == '\n') {  // end of line
	  line_buffer[temp_line_buffer_pos++] = '\0';
	  full_line_read = 1;
	} else {
	  line_buffer[temp_line_buffer_pos++] = c;
	}
      }
      *line_buffer_len = temp_line_buffer_pos - 1;
      //printf("data_get_next_line buffer pos: %d\n", station_data->buffer.read_pos);
      return full_line_read;
    } else {
      return 0;
    }
  } else {
    return 0;
  }
}

// This function processes the favorites data line-by-line. 
void stations_process_line(STATION_DATA *station_data,
		       const char *line,
		       int line_len) {
  int start = 0;
  int end = 0;
  int type = 0;
  STATION *current_entry = &(station_data->stations[station_data->num_stations]);
  
  if (line_len == 0) {
    return;
  }
    
  // Check if this is a comment line
  for (int i=0; i<line_len; i++) {
    if (line[0] == '#') {
      return;
    }
  }

  // Content line: process
  switch (fsm_state) {
  case FSM_CAT_OR_ALIAS:
    end = line_len;  // make sure we will always copy max. up to line_len
    for (int i=0; i<line_len; i++) {
      if (line[i] == '{') {
	start = i+1;
	type = 0;
      } else if (line[i] == '[') {
	start = i+1;
	type = 1;
      } else if ((line[i] == ']') || (line[i] == '}')) {
	end = i;
      }
    }
    if (type == 0) {   // Category title.
                       // Next state is again a category name or alias.
      memcpy(&(station_data->category_name[0]), &line[start], end-start);
      /*#ifndef NO_TERM
      term_print("category name: ");
      term_print(&(station_data->category_name[0]));
      term_print("\r\n");      
      #endif*/
      fsm_state = FSM_CAT_OR_ALIAS;
    } else {
      memcpy(current_entry->short_name, &line[start], end-start);
      /*#ifndef NO_TERM
      term_print("alias: ");
      term_print(current_entry->short_name);
      term_print("\r\n");      
      #endif*/
      fsm_state = FSM_IP;
    }
    break;
  case FSM_IP:
    start = 0;
    end = line_len;
    for (int i=0; i<line_len; i++) {
      if (line[i] != ' ') {
	start = i;
	break;
      }
    }
    for (int i=line_len-1; i>0; i--) {
      if (line[i] != ' ') {
	end = i+1;
	break;
      }
    }
    int port = 80;
    char* portNumber = strchr(line, ':'); 
    if (portNumber != NULL) {  // contains port number
      int index = 1;
      port = 0;
      int ch = portNumber[index];
      index++;
      while (ch != '\0') {
	port = (port * 10) + (ch - 48);
	ch = portNumber[index];
	index++;
      }
      end = portNumber - line;
    }    
    memcpy(current_entry->ip, &line[start], end-start);
    current_entry->port = port;
    /*#ifndef NO_TERM
    term_print("ip: ");
    term_print(line);
    term_print(" - ");
    term_print(current_entry->ip);
    term_print_val(" - ", current_entry->port);
    term_print("\r\n");      
    #endif*/
    fsm_state = FSM_URL;
    break;
  case FSM_URL:
    start = 0;
    end = line_len;
    for (int i=0; i<line_len; i++) {
      if (line[i] != ' ') {
	start = i;
	break;
      }
    }
    for (int i=line_len-1; i>0; i--) {
      if (line[i] != ' ') {
	end = i+1;
	break;
      }
    }
    memcpy(current_entry->url, &line[start], end-start);
    /*#ifndef NO_TERM
      term_print("url: ");
      term_print(current_entry->url);
      term_print("\r\n");      
      #endif*/
    current_entry->index = station_data->num_stations;
    
    station_data->num_stations++;

    fsm_state = FSM_CAT_OR_ALIAS;
    break;
  }
}


void stations_get_stations(STATION_DATA *station_data, STATION **station_list, int *station_list_len) {
  *station_list = station_data->stations;
  *station_list_len = station_data->num_stations;
}


void stations_save(STATION_DATA *station_data, const char* file_name) {
  FILE_HANDLE stations_file;
  FILE_STATE status;
  char line_buf[100];

  term_print("save stations\r\n");
  status = file_open_for_write(file_name, &stations_file);
  term_print_val("status: ", status);
  if (status == FILE_OK) {
    string_build(&(line_buf[0]), 100);
    string_add("{");
    string_add(station_data->category_name);
    string_add("}\n");
    status = file_write_block(&stations_file, line_buf, strlen(line_buf));

    for (int i=0; i<station_data->num_stations; i++) {
      // name
      string_build(&(line_buf[0]), 100);
      string_add("[");
      string_add(station_data->stations[i].short_name);
      string_add("]\n");
      status = file_write_block(&stations_file, line_buf, strlen(line_buf));
    
      // IP
      string_build(&(line_buf[0]), 100);
      string_add(station_data->stations[i].ip);

      if (station_data->stations[i].port != 80) {
	char port[6] = { 0, 0, 0, 0, 0, 0 };
	itoa(station_data->stations[i].port, port, 10);
	
	string_add(":");
	string_add(port);
      }

      string_add("\n");
      status = file_write_block(&stations_file, line_buf, strlen(line_buf));

      // REQUEST
      string_build(&(line_buf[0]), 100);
      string_add(station_data->stations[i].url);
      string_add("\n");
      
      status = file_write_block(&stations_file, line_buf, strlen(line_buf));
    }
  }

  file_close(&stations_file);
}


FILE_STATE stations_load(STATION_DATA *station_data, const char* file_name) {
  FILE_HANDLE stations_file;
  FILE_STATE status;
  int buffer_size = REMOTE_BUFFER_SIZE;

  char* buffer_storage;
  circular_data_buffer buffer;

  char line_buf[100];
  
  status = file_open_for_read(file_name, &stations_file);

  if (status == FILE_OK) {
    int stop = 0;

    buffer_storage = (char*) malloc(buffer_size * sizeof(char));
    memset(&(buffer_storage[0]), 0, buffer_size);
    circular_data_buffer_init(&buffer, &(buffer_storage[0]), buffer_size);
    station_data->buffer = &buffer;
  
    while (!stop) {
      int chars_read = 0;
      memset(line_buf, 0, 100);
      status = file_read_block(&stations_file, line_buf, 100, &chars_read);

      if (status != FILE_ERROR) {
	int res = stations_read_chunk(station_data, line_buf, chars_read);
	// If successful, try to process as much as possible from
	// the buffer.
	if (res) {
	  // Read all lines we can from the buffer. (Reading a chunk may have
	  // added data for more than one line.)
	  while ((res = stations_has_next_line(station_data))) {
	    int entry_complete = 0;
	    int line_read = stations_get_next_line(station_data, &(line_buffer[0]), &line_buffer_pos);
	    stations_process_line(station_data, line_buffer, line_buffer_pos);
	  }
	}
      } else {
	stop = 1;
      }
    }

    status = file_close(&stations_file);

    free(station_data->buffer->data_buffer);
    station_data->buffer = NULL;
    
    return status;
  } else {
    return status;
  }
  
}


FILE_STATE stations_selected_load(const char* file_name, char **short_name) {
  FILE_HANDLE stations_selected_file;
  FILE_STATE status;
  char line_buf[100];
  int chars_read = 0;

  memset(&(line_buf[0]), 0, 100);
  status = file_open_for_read(file_name, &stations_selected_file);
  if (status == FILE_OK) {
    status = file_read_block(&stations_selected_file, line_buf, 100, &chars_read);
    term_print_val("Load selected station name: ", chars_read);
    if (status == FILE_OK) {
      if (chars_read > 0) {
	*short_name = (char*) malloc(chars_read+1);
	memset(&(*short_name[0]), 0, chars_read+1);
	memcpy(&(*short_name[0]), &(line_buf[0]), chars_read);
      }
      status = file_close(&stations_selected_file);
    }
  }
  return status;
}


FILE_STATE stations_selected_save(const char* file_name, char *short_name) {
  FILE_HANDLE stations_selected_file;
  FILE_STATE status;
  char line_buf[100];
  int chars_read = 0;

  term_print("Saving selected station name: ");
  term_print(short_name);
  term_print("\r\n");
  status = file_open_for_write(file_name, &stations_selected_file);
  if (status == FILE_OK) {
    status = file_write_block(&stations_selected_file, short_name, strlen(short_name));
    if (status == FILE_OK) {
      status = file_close(&stations_selected_file);
    }
  }
  return status;  
}

