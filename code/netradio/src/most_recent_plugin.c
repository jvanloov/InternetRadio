// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <stdlib.h>
#include <string.h>

#include "configuration.h"

#include "menu.h"
#include "station_list_plugin.h"
#include "stations_data.h"
#include "stations_recent3.h"
#include "display.h"
#include "player.h"
#include "hal_mcu.h"
#include "messages.h"

typedef struct {
  STATION* most_recent_station;
  int index;
} most_recent_data;

static int most_recent_plugin_initialize(
		      UI_PLUGIN_DATA most_recent_plugin_data,
		      STATION_DATA *global_station_list,
		      MENU* parent_menu);
static int most_recent_plugin_cleanup(
		      UI_PLUGIN_DATA most_recent_plugin_data);
static int most_recent_plugin_terminate(
		      UI_PLUGIN_DATA most_recent_plugin_data);
static char* most_recent_plugin_get_menuitem(
		      UI_PLUGIN_DATA most_recent_plugin_data);
static int most_recent_plugin_get_menuitemactiontype(void);
static void* most_recent_plugin_get_menuitemaction(UI_PLUGIN_DATA data, int entry_index);

static void* play_fun(int entry_index);

static MENU* parent;


int most_recent_plugin_setup(UI_PLUGIN_API* most_recent_plugin_api,
			     UI_PLUGIN_DATA* most_recent_plugin_data,
			     int index) {
  term_print("Setup most recent station plugin API functions.\r\n");
  most_recent_plugin_api->initialize = most_recent_plugin_initialize;
  most_recent_plugin_api->cleanup = most_recent_plugin_cleanup;
  most_recent_plugin_api->terminate = most_recent_plugin_terminate;
  most_recent_plugin_api->get_menuitem = most_recent_plugin_get_menuitem;
  most_recent_plugin_api->get_menuitemactiontype = menu_item_action_fun;
  most_recent_plugin_api->get_menuitemaction = most_recent_plugin_get_menuitemaction;

  most_recent_data* data = (most_recent_data*) malloc(sizeof(most_recent_data));
  data->most_recent_station = NULL;
  data->index = index;
  *most_recent_plugin_data = data;
  
  return 0;
}


static int most_recent_plugin_initialize(UI_PLUGIN_DATA most_recent_plugin_data,
					  STATION_DATA *global_station_list,
					  MENU* parent_menu) {
  FILE_HANDLE selected;
  FILE_STATE fstate = FILE_OK;
  char* selected_station_name = NULL;

  parent = parent_menu;

  // the name is only borrowed; we only use it to look up the station
  // data in the global station list.
  selected_station_name = stations_recent3_get(((most_recent_data*)most_recent_plugin_data)->index);

  if (selected_station_name != NULL) {
    // look up station name in global station list
    for (int idx=0; idx<global_station_list->num_stations; idx++) {
      if (strncmp(global_station_list->stations[idx].short_name,
		  selected_station_name,
		  strlen(selected_station_name)) == 0) {
	((most_recent_data*)most_recent_plugin_data)->most_recent_station = &(global_station_list->stations[idx]);
	break;
      }
    }
  }   
  
  if (((most_recent_data*)most_recent_plugin_data)->most_recent_station) {
    term_print("Most recent station #");
    term_print_int(((most_recent_data*)most_recent_plugin_data)->index);
    term_print(": ");
    term_print(((most_recent_data*)most_recent_plugin_data)->most_recent_station->short_name);
    term_print("\r\n");
  } else {
    term_print("(Most recent station not found in station list.)\r\n");
  }

  return 0;  
}

static int most_recent_plugin_cleanup(UI_PLUGIN_DATA most_recent_plugin_data) {
  term_print("Cleanup most recent plugin data\r\n");
  return 0;
}

static int most_recent_plugin_terminate(UI_PLUGIN_DATA most_recent_plugin_data) {
  free(most_recent_plugin_data);
  term_print("Terminate most recent plugin\r\n");
  return 0;
}

static char* most_recent_plugin_get_menuitem(UI_PLUGIN_DATA most_recent_plugin_data) {
  if (((most_recent_data*)most_recent_plugin_data)->most_recent_station != NULL) {
    return ((most_recent_data*)most_recent_plugin_data)->most_recent_station->short_name;
  } else {
    return MSG_MRP_NO_SELECTION;
  }
}

void* most_recent_plugin_get_menuitemaction(UI_PLUGIN_DATA most_recent_plugin_data,
					    int entry_index) {
  if (((most_recent_data*)most_recent_plugin_data)->most_recent_station != NULL) {
    term_print("Play '");
    term_print(((most_recent_data*)most_recent_plugin_data)->most_recent_station->short_name);
    term_print("'\r\n");
    player_play_station(((most_recent_data*)most_recent_plugin_data)->most_recent_station);
  } else {
    term_print("(no most recent station to play.)\r\n");

    display_clear_buffer();

    display_setcursor(10, 7);
    display_print(MSG_MRP_NO_MOST_RECENT_1);
    display_setcursor(8, 14);
    display_print(MSG_MRP_NO_MOST_RECENT_2);
    
    display_draw_buffer();
    mcu_delay(1000000);
  }
  return NULL;
}

