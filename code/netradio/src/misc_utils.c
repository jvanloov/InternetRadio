// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/************************************************************
 * misc_utils.c                                             *
 ************************************************************
 * Miscellaneous utility routines                           *
 * Joris Van Looveren, 04/2017                              *
 *                                                          *
 * This file provides a collection of useful routines, to   *
 * be used in the other files.                              *
 ************************************************************/

#include <stdlib.h>
#include <string.h>

#include "hal_mcu.h"


// Parse an unsigned number from a string. 
// The parameter maxlen specifies the maximum number of digits
// that will be parsed from the string, even if it contains
// more digits.
// If any other character besides 0 .. 9 is encountered during
// parsing, parsing is interrupted and -1 is returned.
// If more than 9 characters characters are requested (string
// length or maxlen), we return -1. (We don't want to risk
// going over INT_MAX for a 32-bit signed number.)
int parse_unsigned_num(char *str, int maxlen) {
  int parsed_val = 0;
  int len = strlen(str);

  // only parse up to maxlen digits, even if the string is longer
  if (maxlen < len) {
    len = maxlen;
  }
  if (len > 9) {
    return -1;
  }
  
  for (int i=0; i<len; i++) {
    char c = str[i];
    if ((c >= '0') && (c <= '9')) {
      parsed_val = (parsed_val*10) + (c-48);
    } else {
      return -1;
    }
  }

  return parsed_val;
}


static char *cur_str;
static int cur_maxlen;
static int cur_len;

void string_build(char* str, int maxlen) {
  cur_str = str;
  cur_maxlen = maxlen;
  cur_len = 0;
  memset(cur_str, 0, cur_maxlen);
}

void string_add(const char *part) {
  int bytes_left = cur_maxlen - cur_len - 1;  // leave 1 byte for '\0'
  int bytes_to_add = strlen(part);
  int to_copy = (bytes_left >= bytes_to_add)?bytes_to_add:bytes_left;
  memcpy(&(cur_str[cur_len]), &(part[0]), to_copy);
  cur_len += to_copy;
  cur_str[cur_len] = '\0';
}

void string_add_substring(const char *part, int start_pos, int end_pos) {
  int bytes_left = cur_maxlen - cur_len - 1;  // leave 1 byte for '\0'
  int bytes_to_add = end_pos - start_pos;
  int to_copy = (bytes_left >= bytes_to_add)?bytes_to_add:bytes_left;
  memcpy(&(cur_str[cur_len]), &(part[start_pos]), to_copy);
  cur_len += to_copy;
  cur_str[cur_len] = '\0';
}


#ifdef __ON_HOST__
int memory_get_available(void) {
  return 0;
}
#else
int memory_get_available(void) {
  int BLOCK_SIZE = 10;
  int total = 0;

  long int *foo = malloc(BLOCK_SIZE);
  long int *bar = malloc(BLOCK_SIZE);
  long int actual_block_size = (int)bar - (int)foo;

  free(bar);
  free(foo);

  // allocate as many blocks as possible
  long int* first = (long int*) malloc(BLOCK_SIZE);
  long int* block;
  long int* prev = first;
  while ((block = malloc(BLOCK_SIZE)) != NULL) {
    block[0] = (long int) prev;    
    prev = block;
    total += actual_block_size;
  }

  block = prev;
  // walk back and release all blocks
  while (block != first) {
    long int* prev = (long int*) block[0];
    free(block);
    block = prev;
  }
  free(first);
 
  return total;  
}
#endif

#ifdef __TEST__
const char *itoa(int num, char* final, int base) {
  // this implementation ignores 'base', and assumes decimal
  char temp[15];
  //char *final = NULL;
  int len = 0;
  
  memset(temp, 0, 15);
  snprintf(temp, 15, "%d", num);
  len = strlen(temp);
  //final = (char*) malloc(len+1);
  memset(final, 0, len+1);
  strncpy(final, temp, len);
  return final;
}
#endif
