// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/************************************************************
 * streaming_callbacks.c                                    *
 ************************************************************
 * Callbacks for network streaming                          *
 * Joris Van Looveren, 03/2017                              *
 *                                                          *
 * These routines implement the functionality to retrieve   *
 * MP3 data from the stream socket, and feed it to the      *
 * Helix decoder.                                           *
 * more_data_cb is called by the player when it wants more  *
 * data. The callback does 3 things:                        *
 * - if the user presses "stop", it returns 0 to signal     *
 *   to the player that it should stop playing.             *
 * - if the buffer contains less data than "requested_size" *
 *   the callback will request a block of data from the     *
 *   socket, and store it in the buffer.                    *
 * - finally, the requested amount of data is copied into   *
 *   buffer, and returned to the player                     *
 * the stream_info_cb callback sends some info from the     *
 * stream to the terminal.                                  *
 ************************************************************/

#include <stdlib.h>
#include <string.h>

#include <simplelink.h>
#include <socket.h>

#include "configuration.h"
#include "streaming_callbacks.h"
#include "hal_mcu.h"
#include "display.h"

#include "circular_data_buffer.h"

#include "messages.h"

extern int iSockID;   // defined in main.c


/****************************************************************
 * streaming data buffer.                                       *
 * this is a thin wrapper around the circular buffer data type. *
 ****************************************************************/

static char* audio_stream_storage = NULL;
static char* tcp_data_buffer = NULL;
static circular_data_buffer audio_stream_buffer;

void streaming_buffer_init() {
  audio_stream_storage = (char*) malloc(NETWORK_BUFFER_SIZE);
  tcp_data_buffer = (char*) malloc(TCP_BUFFER_SIZE);
  circular_data_buffer_init(&audio_stream_buffer, audio_stream_storage, NETWORK_BUFFER_SIZE);
}

void streaming_buffer_clear() {
  circular_data_buffer_clear(&audio_stream_buffer);
}

int streaming_buffer_readahead() {
  return circular_data_buffer_usage(&audio_stream_buffer);
}

int streaming_buffer_spaceleft() {
  return circular_data_buffer_chars_left(&audio_stream_buffer);
}

int streaming_buffer_store(char *block, int block_size) {
  return circular_data_buffer_store_block(&audio_stream_buffer, block, block_size);
}

int streaming_buffer_read(unsigned char *block, int block_size) {
  return circular_data_buffer_read_block(&audio_stream_buffer, (char*)block, block_size);
}

int streaming_buffer_destroy(void) {
  free(audio_stream_storage);
  free(tcp_data_buffer);
  return 1;
}


/****************************************************************
 * Callbacks                                                    *
 ****************************************************************/

// This callback is called when the Helix decoder runs out of data, to provide more.
// If there is no more data, Helix will stop decoding. We can use this behavior to do
// some bookkeeping:
// - if the user presses a "stop" button, we return zero so decoding stops.
// - we check if we have more data left in our TCP buffer. If not, we ask the
//   network socket to provide more data. If we get more data from the socket,
//   we copy data to the data buffer. If not, we return zero so decoding stops.
// - finally, we copy data into the player's buffer and let it continue.
int more_data_cb(uint8_t *buffer, int requested_size) {
  // First, check if the "Stop" button was pressed
  if (button_check(BUTTON_1)) {
    memset(tcp_data_buffer, 0, TCP_BUFFER_SIZE);
    streaming_buffer_clear();
#ifndef NO_TERM
    term_print("Stopping (more_data_cb)\r\n");
#endif
    return 0;
  }
  
  /*  // Then, check if we need to buffer more
  while (streaming_buffer_readahead() < BUFFER_AMOUNT) {
    int length = sl_Recv(iSockID, tcp_data_buffer, TCP_BUFFER_SIZE, 0);
    if (length >= 0) {
      streaming_buffer_store(tcp_data_buffer, length);
    } else {
#ifndef NO_TERM
      term_print("Error fetching data\r\n");
#endif
      return 0;
    }
    }*/

  // Then, check if we need to buffer more
  if (streaming_buffer_readahead() < BUFFER_AMOUNT) {
    while (streaming_buffer_spaceleft() > TCP_BUFFER_SIZE) {
      int length = sl_Recv(iSockID, tcp_data_buffer, TCP_BUFFER_SIZE, 0);
      if (length >= 0) {
	streaming_buffer_store(tcp_data_buffer, length);
      } else {
#ifndef NO_TERM
	term_print("Error fetching data\r\n");
#endif
	return 0;
      }
    }
  }

  // Finally, copy data into the player's buffer and return
  streaming_buffer_read(buffer, requested_size);  

  return 1;
}


// MP3 has a limited number of bitrates. Instead of using a full-fledged
// number-to-string conversion, it's easier to do "index-mapping".
static char *bitrate_str[] = { 
   " 32", " 40", " 48", " 56", " 64", " 80", " 96",
   "112", "128", "160", "192", "224", "256", "320",
   "(?)"
};
static int bitrate_int[] = {
    32,  40,  48,  56,  64,  80,  96,
   112, 128, 160, 192, 224, 256, 320
};
static char *bitrate_to_str(int f) {
  int index = 0;
  for (; index < 14; index++) {
    if (bitrate_int[index] == f) {
      return bitrate_str[index];
    }
  }
  return bitrate_str[index];
}


// This callback is called when connected to a stream. The frames in the stream hold
// information about the bitrate and the sampling frequency of the stream. This
// callback will update the display with that information.
void stream_info_cb(stream_info *str_info) {
  char status[20];
  int bitrate_kbps = str_info->bitrate / 1000;
  int freq_khz = str_info->sample_rate / 1000;
  //int freq_khz_rest = (str_info->sample_rate - freq_khz*1000) / 100;

  int index = 0;
  memset(&status[0], 0, 20);
  if (freq_khz == 32) {
    status[index++] = '3';
    status[index++] = '2';
  } else if (freq_khz == 44) {
    status[index++] = '4';
    status[index++] = '4';
    status[index++] = '.';
    status[index++] = '1';
  } else if (freq_khz == 48) {
    status[index++] = '4';
    status[index++] = '8';
  } else {
    status[index++] = '(';
    status[index++] = '?';
    status[index++] = ')';
  }
  status[index++] = 'k';
  status[index++] = 'H';
  status[index++] = 'z';
  status[index++] = ' ';
  status[index++] = '@';
  status[index++] = ' ';

  memcpy(&status[index], bitrate_to_str(bitrate_kbps), 3);
  index += 3;
  
  status[index++] = 'k';
  status[index++] = 'b';
  status[index++] = 'p';
  status[index++] = 's';
  status[index++] = '\0';

  int width = display_calculate_string_length(status);
  int vpos = 2 * (SCREENH / 3);
  int hpos = (SCREENW / 2) - (width / 2) - 3;
  display_setcursor(hpos, vpos);
  display_clear_buffer_region(hpos, vpos, hpos+width, vpos+6);
  display_print(status);
  display_update_status_bar_keys(MSG_SC_STOP, "", "", "");
  display_draw_buffer();

#ifndef NO_TERM
  term_print("connected; playing.\r\n");
#endif
  
}


