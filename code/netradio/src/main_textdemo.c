// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/************************************************************
 * main.c                                                   *
 ************************************************************
 * Main loop for the netradio implementation.               *
 ************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hal_mcu.h"
#include "display.h"

int main(void) {
  long result;
  
  board_init();
  hal_init();
  
  uart_init();
  term_init(115200);

  term_print("");

  display_init();
  display_initialize();
  display_set_backlight_brightness(3);

  display_clear_buffer();

  display_setcursor(1,1);
  display_print("Letters and digits:\n");
  display_print("ABCDEFGHIJKLMNOPQRSTUVWXYZ\n");
  display_print("abcdefghijklmnopqrstuvwxyz\n");
  display_print("0 1 2 3 4 5 6 7 8 9\n");
  display_setcursor(1,32);
  display_print("Special characters:\n");
  display_print("! , - . / : ; < = > ? @ \n");
  display_setcursor(1,50);
  display_print("UI specific characters:\n");
  display_print("\\ # ^ `\n");
  
  display_draw_buffer();

  term_print("Text demo 2.\r\n");

  getchar();
  
  hal_cleanup();

  return 1;
}
