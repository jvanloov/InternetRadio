// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/************************************************************
 * system_menu_plugin.c                                     *
 ************************************************************
 * Main menu plugin: provides the system menu               *
 * Joris Van Looveren                                       *
 ************************************************************/

#include <stdlib.h>
#include <string.h>

#include "simplelink.h"

#include "menu.h"
#include "system_menu_plugin.h"
#include "display.h"
#include "messages.h"

#include "hal_mcu.h"
#include "user_input.h"

static int system_menu_plugin_initialize(
		      UI_PLUGIN_DATA system_menu_plugin_data,
		      STATION_DATA *global_station_list,
		      MENU* parent_menu);
static int system_menu_plugin_cleanup(
		      UI_PLUGIN_DATA system_menu_plugin_data);
static int system_menu_plugin_terminate(
		      UI_PLUGIN_DATA system_menu_plugin_data);
static char* system_menu_plugin_get_menuitem(
		      UI_PLUGIN_DATA system_menu_plugin_data);
static int system_menu_plugin_get_menuitemactiontype(char* entry_name);
static void* system_menu_plugin_get_menuitemaction(UI_PLUGIN_DATA data, int entry_index);

static void* system_menu_reset_networks(UI_PLUGIN_DATA data, int entry_index);
static void* system_menu_clear_station_list(UI_PLUGIN_DATA data, int entry_index);


static MENU* parent;
static MENU system_menu = { 0, 0, NULL };

int system_menu_plugin_setup(UI_PLUGIN_API* system_menu_plugin_api,
			      UI_PLUGIN_DATA* system_menu_plugin_data) {
  term_print("Setup system menu plugin API functions.\r\n");
  system_menu_plugin_api->initialize = system_menu_plugin_initialize;
  system_menu_plugin_api->cleanup = system_menu_plugin_cleanup;
  system_menu_plugin_api->terminate = system_menu_plugin_terminate;
  system_menu_plugin_api->get_menuitem = system_menu_plugin_get_menuitem;
  system_menu_plugin_api->get_menuitemaction = system_menu_plugin_get_menuitemaction;
  system_menu_plugin_api->get_menuitemactiontype = menu_item_submenu_fun;

  if (system_menu.entry_names == NULL) {
    menu_initialize(&system_menu, MSG_SM_MENUNAME, 2);
    
    system_menu.entry_names[0] = MSG_SM_RESET_ITEM;
    system_menu.entry_names[1] = MSG_SM_CLEAR_ITEM;
    system_menu.entry_actions[0] = system_menu_reset_networks;
    system_menu.entry_actions[1] = system_menu_clear_station_list;
    system_menu.entry_action_types[0] = menu_item_action_fun;
    system_menu.entry_action_types[1] = menu_item_action_fun;
  }

  return 0;
}


static int system_menu_plugin_initialize(UI_PLUGIN_DATA system_menu_plugin_data,
					 STATION_DATA *global_station_list,
					 MENU* parent_menu) {
  term_print("Initialize system menu\r\n");
  parent = parent_menu;
  return 0;
}

static int system_menu_plugin_cleanup(UI_PLUGIN_DATA system_menu_plugin_data) {
  term_print("Cleanup system menu\r\n");
  return 0;
}

static int system_menu_plugin_terminate(UI_PLUGIN_DATA system_menu_plugin_data) {
  term_print("Terminate system menu\r\n");
  menu_terminate(&system_menu);
  return 0;
}

static char* system_menu_plugin_get_menuitem(UI_PLUGIN_DATA system_menu_plugin_data) {
  return MSG_SM_MENUNAME;
}

static int system_menu_plugin_get_menuitemactiontype(char* entry_name) {
  return MENU_SUBMENU;
}


static void* system_menu_plugin_get_menuitemaction(UI_PLUGIN_DATA data, int entry_index) {
  return &system_menu;
}


static void* system_menu_reset_networks(UI_PLUGIN_DATA data, int entry_index) {
  int confirmed = 0;
  
  term_print("Reset networks\r\n");

  display_clear_buffer_region(0,SCREENH-14,SCREENW,SCREENH);
  display_setcursor(1,SCREENH-14);
  display_print(MSG_NETWORK_CONFIRM);
  display_update_status_bar_keys(MSG_NO, "", "", MSG_YES);
  display_draw_buffer();  
  confirmed = user_input_confirm();

  if (confirmed) {
    sl_WlanProfileDel(0xFF);
    display_update_status_bar(MSG_SM_RESET_ITEM_CONFIRMED);
  } else {
    display_update_status_bar(MSG_SM_RESET_ITEM_CANCELLED);
  }
  mcu_delay(500000);
  
  return NULL;
}

static void* system_menu_clear_station_list(UI_PLUGIN_DATA data, int entry_index) {
  term_print("Clear station list\r\n");
  return NULL;
}
