// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/************************************************************
 * player.c                                                 *
 ************************************************************
 * Audio player abstraction.                                *
 * Joris Van Looveren, 12/2016                              *
 ************************************************************
 * This module provides an abstraction for a music player.  *
 * The main entry point is "play_from_inet_stream", which   *
 * takes the connection data, and callbacks for requesting  *
 * more data, and returning stream info.                    *
 * Everything from connecting to the stream, error recovery *
 * etc. is handled in this module.                          *
 * Stop the player gracefully by letting the more_data_cb   *
 * return 0.                                                *
 ************************************************************/

#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

#include "configuration.h"

#include "mp3dec.h"
#include "mp3dec-additions.h"
#include "audio.h"
#include "player.h"
#include "display.h"
#include "misc_utils.h"
#include "network.h"
#include "stations_recent3.h"
#include "messages.h"

static int playing;

int iSockID = 0;
char *spinner[] = { "{", "|", "}", "~" };

HMP3Decoder dec = NULL;

MP3FrameInfo frameInfo;
stream_info str_info;


inline int is_playing(void) {
  return playing;
}


void player_init(void) {
  dec = MP3InitDecoder();
  init_helix_pcm_handler();
}


void player_reset(void) {
  Reset(dec);
}


void player_close(void) {
  MP3FreeDecoder(dec);
}

static int player_result_from_helix_result(int helix_result) {
  if ((-5 <= helix_result) && (helix_result <= 0)) {
    return helix_result;
  } else if ((-9999 < helix_result) && (helix_result <= -6)) {
    return PLAYER_STATUS_DECODE_ERROR;
  } else {
    return PLAYER_STATUS_UNKNOWN_ERROR;
  }
}


int play(get_more_data_callback more_data_cb, stream_info_callback stream_info_cb) {
  int frame_info_sent = 0;
  int frame_info_stored = 0;
  int more_data = 0;
  int helix_result = 0;
  int bytes_left = 0;
  int frame_start = 0;
  int subsequent_successful_frames = -1;

  uint8_t* data_buffer = NULL;
  uint8_t* data_buffer_read_ptr;

  data_buffer = (uint8_t*) malloc(DATA_BUFFER_SIZE);
  
  playing = 0;

  Reset(dec);
  memset(data_buffer, 0, DATA_BUFFER_SIZE);
  data_buffer_read_ptr = data_buffer;
  
  // initial buffer fill
  // re-request data until we find a sync word from which Helix can start
  do {
    more_data = more_data_cb(data_buffer, DATA_BUFFER_SIZE);
    if (more_data) {
      data_buffer_read_ptr = data_buffer;
      frame_start = MP3FindSyncWord(data_buffer_read_ptr, DATA_BUFFER_SIZE);
    } else {
      free(data_buffer);
      return PLAYER_STATUS_MAINDATA_UNDERFLOW;
    }
  } while (frame_start == -1);
  data_buffer_read_ptr += frame_start;  // position the read pointer at the frame start

  // Start the timer for the mock audio-out interrupt handler
  audio_buffer_clear();

  while (helix_result == ERR_MP3_NONE || helix_result == ERR_MP3_MAINDATA_UNDERFLOW) {
    _SlNonOsMainLoopTask();

    // We only call the frame info callback after a few successful frames,
    // when we can be reasonably sure that we've "really" started playing
    // the stream.
    if (frame_info_stored &&
	(subsequent_successful_frames < SUBSEQUENT_SUCCESSES+1)) {
      subsequent_successful_frames++;
    }
    
    // If starting the loop, delay the audio out start to
    // create a head start for the decoder. This is needed to allow
    // time for getting data from the network
    if ((audio_state() != STATE_PLAYING) && (audio_buffer_lead() > 1152)) {
      amplifier_enable();
      audio_start();
    }
    
    int bytes_read = data_buffer_read_ptr - data_buffer;

    // Move data:
    // 1. Shift databuffer contents.
    //    We move the unread data buffer contents to the beginning of the buffer
    //    (overwriting the data that we've processed until now)
    memcpy(data_buffer, data_buffer_read_ptr, DATA_BUFFER_SIZE - bytes_read);
    // 2. Copy new data from storage to the buffer.
    //    We copy enough to fill the data buffer again
    uint8_t *buffer_fill_start_pos = data_buffer + (DATA_BUFFER_SIZE - bytes_read);
    more_data = more_data_cb(buffer_fill_start_pos, bytes_read);
    if (more_data) {
      // 3. Move the read pointer to the beginning of the buffer
      data_buffer_read_ptr = data_buffer;
      // 4. We have again a full buffer
      bytes_left = DATA_BUFFER_SIZE;

      helix_result = MP3Decode(dec, &data_buffer_read_ptr, &bytes_left, buf_callbacks, 0);

      // If we don't know the stream characteristics yet, but we have
      // a successfully decoded frame, store the stream characteristics.
      if (!frame_info_stored &&
	  (helix_result == ERR_MP3_NONE || helix_result == ERR_MP3_MAINDATA_UNDERFLOW)) {
	MP3GetLastFrameInfo(dec, &frameInfo);
	frame_info_stored = 1;
	subsequent_successful_frames = 0;
	audio_setup(frameInfo.samprate);
      }

      // Send the stream info to the main app after a few successfully
      // decoded frames.
      if (!frame_info_sent &&
	  (subsequent_successful_frames > SUBSEQUENT_SUCCESSES)) {
	str_info.channels = frameInfo.nChans;
	str_info.sample_rate = frameInfo.samprate;
	str_info.bitrate = frameInfo.bitrate;
  	stream_info_cb(&str_info);
	frame_info_sent = 1;
	playing = 1;
      }
    } else {
      break;
    }
  }

  playing = 0;
  amplifier_disable();
  audio_stop();
  free(data_buffer);

  if (more_data == 0) {
    return PLAYER_STOPPED;
  } else {
    return player_result_from_helix_result(helix_result);
  }
}


int play_from_inet_stream(int iSockID, SlSockAddrIn_t *pAddr, char *request, get_more_data_callback more_data_cb, stream_info_callback stream_info_cb) {
  int iStatus;
  int trials = 0;
  char* buf = NULL;
  int bufSize = 0;
  
  int iAddrSize = sizeof(SlSockAddrIn_t);

  // connect to the TCP server
  iStatus = sl_Connect(iSockID, (SlSockAddr_t *)pAddr, iAddrSize);
  if (iStatus < 0) {
    return PLAYER_NETWORK_CONNECT_ERROR;
  }

  int connectionAttempts = 0;
  // Copy the request in the TCP buffer
  bufSize = strlen(request) + 1;  // accommodate the final '\0'
  buf = (char*) malloc(bufSize);
  
  memset(buf, 0, bufSize);
  memcpy(buf, request, strlen(request));
  // Try max. 3 times to request the urlName from the given address
  do {
    connectionAttempts += 1;
    iStatus = sl_Send(iSockID, buf, strlen(buf), 0 );
    _SlNonOsMainLoopTask();
  } while ((connectionAttempts < 3) && (iStatus < 0));
  
  if (iStatus < 0) {
    free(buf);
    return PLAYER_NETWORK_SEND_ERROR;
  }

  // try to receive a first few bytes. they are discarded, but if successful, we
  // can start the player. If not, we skip the player and return immediately.
  int length = sl_Recv(iSockID, buf, bufSize, 0);
  free(buf);

  if (length >= 0) {
    // PLAYER_STATUS_DECODE_ERROR should be recoverable; in those cases,
    // we attempt to reconnect. Note that this means first waiting for a
    // sync frame, so interruptions in the sound will be heard.
    int player_result = 0;
    while ((player_result == PLAYER_STATUS_OK) ||
	   ((player_result == PLAYER_STATUS_DECODE_ERROR) &&
	    (trials < 10))) {
      player_result = play(more_data_cb, stream_info_cb);
      trials++;
    }

    return player_result;
  } else {
    return PLAYER_NETWORK_RECV_ERROR;
  }
}

// the buffer should be at least 17 characters longer than
// the max. URL length (STATION_ENTRY_QUERY_LENGTH):
// - it will be prefixed with "GET "
// - it will be suffixed with " HTTP/1.1\r\n\r\n"
#define BUF_LEN 100

void player_play_station(STATION* data) {
  term_print("Play '");
  term_print(data->short_name);
  term_print("' (");
  term_print(data->ip);
  term_print(")\r\n");

  char str_buf[BUF_LEN];

  char *streamShortName = data->short_name;
  char *streamIP = data->ip;
  int streamPort = data->port;

  stations_recent3_add(SELECTED_FILE, streamShortName);
  
#ifndef NO_TERM
  term_print("Saving current selection to file: ");
  term_print(streamShortName);
  term_print("\r\n");
#endif

  string_build(&(str_buf[0]), BUF_LEN);
  string_add("GET ");
  string_add(data->url);
  string_add(" HTTP/1.1\r\n\r\n");
	
  char *streamREQ  = &(str_buf[0]);

#ifndef NO_TERM
  term_print("Selection: ");
  term_print(streamShortName);
  term_print("\r\n");
  term_print("IP:        ");
  term_print(streamIP);
  term_print("\r\n");
  term_print_val("Port:      ", streamPort);
  term_print("Request:   ");
  term_print(streamREQ);
  term_print("\r\n");
#endif
      
  int name_width = display_calculate_string_length(streamShortName);
  int name_x1 = (SCREENW / 2) - (name_width / 2) - 3;
  int name_vpos = (SCREENH / 3) - 5;
  
  display_clear_buffer();
  display_setcursor(name_x1+3, name_vpos+4);
  display_print(streamShortName);
  display_invert_buffer_region(2, name_vpos, SCREENW-2, name_vpos+14);
  display_draw_buffer();
      
  // Clean up MP3 decoder & audio infrastructure
  // All Helix buffers need to be zeroed properly to put the
  // decode back to the initial state
  // 44.1kHz is a default setting; this will be changed when
  // connected to a stream, and the sample rate is known.

#ifndef NO_TERM
  term_print_val("Memory: ", memory_get_available());
#endif
	
  // Set up streaming buffer and MP3 decoder (allocate buffers)
  streaming_buffer_init();
  player_init();	
	
#ifndef NO_TERM
  term_print_val("Memory: ", memory_get_available());
  term_print("audio_init\r\n");
#endif
  audio_init();
#ifndef NO_TERM
  term_print("audio_setup\r\n");
#endif
  audio_setup(44100);
  
#ifndef NO_TERM
  term_print("Attempting to open stream '");
  term_print(streamShortName);
  term_print("'\r\n");
#endif  
  int port = 0;
  unsigned int ip = network_parse_ip(streamIP);
  //port = network_parse_port(streamIP);
  port = streamPort;
  if (port == -1) {
    port = 80;
  }
      
  SlSockAddrIn_t sAddr;
  
  //filling the TCP server socket address
  sAddr.sin_family = SL_AF_INET;
  sAddr.sin_port = sl_Htons((unsigned short)port);
  sAddr.sin_addr.s_addr = sl_Htonl(ip);
      
  // creating a TCP socket
  iSockID = sl_Socket(SL_AF_INET,SL_SOCK_STREAM, 0);
  if (iSockID >= 0) {
    int result = -1;
    int failure_counter = -1;
	
    // Connect to the stream. As long as we receive decoding errors,
    // keep trying; they may be recoverable by reconnecting to the stream.
    // Other errors are not recoverable.
    //term_print("\r\nConnecting to stream..");
    do {
      failure_counter++;
      
      int vpos = 2 * (SCREENH / 3);
      int hpos = (SCREENW / 2) - 5;
      
      display_setcursor(hpos, vpos);
      display_print(spinner[failure_counter % 4]);
      display_draw_buffer();

      result = play_from_inet_stream(iSockID, &sAddr, streamREQ, more_data_cb, stream_info_cb);
    } while ((result == PLAYER_STATUS_DECODE_ERROR) && (failure_counter < 50));
  }
  _SlNonOsMainLoopTask();
  mcu_delay(20000);
  sl_Close(iSockID);

  display_update_status_bar(MSG_PLAYER_CLOSED_CONNECTION);
  display_draw_buffer();
#ifndef NO_TERM
  term_print("Closed connection\r\n");
#endif

  audio_stop();
  audio_close();
  player_close();
  streaming_buffer_destroy();
#ifndef NO_TERM
  term_print("Player closed\r\n");
  term_print_val("Memory: ", memory_get_available());
#endif
}
