#!/bin/sh

# Copyright 2018 Joris Van Looveren
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use, copy,
# modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Build the tests
make -f Makefile-tests clean all

# Run them all one by one. The script stops if
# errors are detected.

bin/circular_data_buffer_test
if [ $? -ne 0 ] ; then  exit ; fi
   
bin/test_utils
if [ $? -ne 0 ] ; then  exit ; fi

bin/stations_data_test
if [ $? -ne 0 ] ; then  exit ; fi

bin/streaming_callbacks_test
if [ $? -ne 0 ] ; then  exit ; fi

echo All tests passed!
