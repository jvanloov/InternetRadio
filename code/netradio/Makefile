# Copyright 2018 Joris Van Looveren
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use, copy,
# modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

### USER CONFIGURABLE SETTINGS ###

# LCD display used.
# This determines the low-level driver interface to include,
# and the screen size.
# nokia = Nokia 5110 LCD:
# - 84x64 pixels
# - PCD8544 driver chip
#DISPLAYTYPE=nokia
# EA DOGL128-6 = Display Visions EA-DOGL128-6 LCD:
# - 128x64 pixels
# - STR565R driver chip
DISPLAYTYPE=ea-dogl128-6

# executable name
PROGNAME=netradio

### BUILD SETTINGS ###

INCDIRS=-Iinc -Iinc/ti -Iinc/ti/inc -Iinc/ti/driverlib -Iinc/oslib -Iinc/ti/simplelink -Iinc/ti/simplelink/include -Iinc-netradio

SRCDIR=src
OBJDIR=bin
LIBDIR=lib
GCCDIR=gcc

CC=arm-none-eabi-gcc
LD=arm-none-eabi-ld
OBJCOPY=arm-none-eabi-objcopy

ifndef DISPLAYTYPE
$(error DISPLAYTYPE is not set.)
endif

CFLAGS_DISPLAY=-DDISPLAY_TYPE=$(DISPLAYTYPE)
CFLAGS=-mthumb             \
       -mcpu=cortex-m4     \
       -ffunction-sections \
       -fdata-sections     \
       -MD                 \
       -std=c99            \
       -O0 		   \
       ${INCDIRS}          \
       $(CFLAGS_DISPLAY)   \
       -DSL_FULL -U__STRICT_ANSI__ -Dgcc -c -g 
#-DNO_TERM \

ifeq ($(DISPLAYTYPE),nokia)
LOWLEVEL_DISPLAY_DRIVER=${OBJDIR}/display_lowlevel_nokia-5110.o
else
LOWLEVEL_DISPLAY_DRIVER=${OBJDIR}/display_lowlevel_ea-dogl128-6.o
endif

LIBGCC:=${shell ${CC} -mthumb ${CFLAGS} -print-libgcc-file-name}
LIBC:=${shell ${CC} ${CFLAGS} -print-file-name=libc.a}
LIBM:=${shell ${CC} ${CFLAGS} -print-file-name=libm.a}
LIBNOSYS:=${shell ${CC} ${CFLAGS} -print-file-name=libnosys.a}

### BUILD RULES ###

all: clean start objects ${OBJDIR}/${PROGNAME}.axf done

clean:
	@rm -f ${OBJDIR}/*.o
	@rm -f ${OBJDIR}/*.d
	@rm -f ${OBJDIR}/*.bin
	@rm -f ${OBJDIR}/*.axf

start:
	@echo Building ${OBJDIR}/${PROGNAME}.\{.axf\|.bin\}

done:
	@echo Done!

${OBJDIR}/%.o: ${SRCDIR}/%.c
	@echo - Building $<
	@${CC} ${INCDIRS} ${CFLAGS} -o $@ $<

${GCCDIR}/%.o: ${GCCDIR}/%.c
	@echo - Building $<
	@${CC} ${INCDIRS} ${CFLAGS} -o $@ $<

objects: ${OBJDIR}/main.o ${OBJDIR}/user_input.o ${OBJDIR}/hal_cc3200.o \
	 ${OBJDIR}/display.o $(LOWLEVEL_DISPLAY_DRIVER) \
	 ${OBJDIR}/misc_utils.o ${OBJDIR}/stations_data.o \
         ${OBJDIR}/circular_data_buffer.o ${OBJDIR}/network.o ${OBJDIR}/simplelink_hooks.o \
         ${OBJDIR}/helix_pcm_handler.o ${OBJDIR}/audio.o \
	 ${OBJDIR}/streaming_callbacks.o ${OBJDIR}/player.o ${OBJDIR}/stations_recent3.o \
	 ${OBJDIR}/menu.o ${OBJDIR}/station_list_plugin.o ${OBJDIR}/system_menu_plugin.o \
	 ${OBJDIR}/most_recent_plugin.o ${GCCDIR}/startup_gcc.o 

${OBJDIR}/${PROGNAME}.axf: ${OBJECTS}
	@echo - Linking
	@${LD} -T gcc/${PROGNAME}.ld               \
	      --entry ResetISR                      \
              --gc-sections -o ${OBJDIR}/${PROGNAME}.axf     \
              $(wildcard ${OBJDIR}/*.o)             \
              $(wildcard ${LIBDIR}/*.o)             \
              $(wildcard ${GCCDIR}/*.o)             \
              ${LIBDIR}/libsimplelink_nonos.a ${LIBDIR}/libdriver.a \
              ${LIBDIR}/libhelixmp3dec.a \
              '${LIBM}' '${LIBC}' '${LIBGCC}' \
              -Map=${OBJDIR}/${PROGNAME}.map
	@echo - Extracting binary
	@${OBJCOPY} -O binary ${@} ${@:.axf=.bin}


