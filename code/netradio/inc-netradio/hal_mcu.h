// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/************************************************************
 * hal_mcu.h                                                *
 ************************************************************
 * Hardware Abstraction Layer declarations                  *
 * Joris Van Looveren, 10/2016                              *
 *                                                          *
 ************************************************************/

#ifndef __HAL_MCU_H
#define __HAL_MCU_H

#include <inttypes.h>
#ifdef __TEST__
#include <stdio.h>
#endif

// 5 buttons: buttons 2 and 5 correspond to the 2 "user" buttons
// on the CC3200 launchpad. Buttons 1-4 are wired on the
// audio/input expansion board.
typedef enum {
  BUTTON_1,
  BUTTON_2,
  BUTTON_3,
  BUTTON_4,
  BUTTON_5  
} BUTTONS;


void hal_init(void);
void hal_cleanup(void);

// Display: we use a Nokia LCD
#define DISPLAY_COMMAND 0
#define DISPLAY_DATA    1
// display_init sets up the GPIO pins used for sending commands
// and data to the display.
void display_init(void);
// display_reset resets the display by pulling the RESET pin low
// for a few ms.
void display_reset(void);
// display_write sends a byte (command or data) to the display.
void display_write(char mode, char msg);
// set the display backlight brightness. level = 1..5
void display_set_backlight_brightness(int level);

// Buttons
// Set up the GPIO pins for the 5 buttons
void button_init();
// Check the state of the specified button.
// Returns -1 if the button is not known.
int button_check(BUTTONS button);

// SPI
// We use SPI for outputting sound samples to the DAC.
void spi_setup(void);
void spi_enable(void);
void spi_send(void *bytes, int byte_count);

// Timer.
// We use TIMER_A0 in periodic mode to trigger each
// sound sample output to the DAC.
void timer_setup(int timer_val, void (*handler)(void));
void timer_enable();
void timer_disable();
void timer_interrupt_disable();
void timer_interrupt_enable();
void timer_interrupt_clear();

// Generic delay function; the delay is approximate.
void mcu_delay(int delay_us);

// Initial set-up of the board; called once on power-on.
void board_init(void);

#ifndef NO_TERM
void uart_init(void);

void term_init(int baud_rate);
void term_print(const char *str);
void term_print_int(int val);

void term_print_val(char *msg, int val);
void term_print_hex(uint8_t val, char header);
#endif

void led_init(void);
void led_toggle(void);
void led2_toggle(void);

// Enable/disable the amplifier. The amplifier should only be
// enabled when it's actually playing music.
void amplifier_init(void);
void amplifier_enable(void);
void amplifier_disable(void);

// Filesystem

typedef struct file_handle {
#ifndef __TEST__  
  long int handle;
#else
  FILE* handle;
#endif
  int current_offset;
} FILE_HANDLE;

typedef enum {
  FILE_OK,
  FILE_ERROR
} FILE_STATE;

FILE_STATE file_open_for_write(const char* name, FILE_HANDLE* handle);
FILE_STATE file_open_for_read(const char* name, FILE_HANDLE* handle);
FILE_STATE file_write_block(FILE_HANDLE* handle, char* block, int block_size);
FILE_STATE file_read_block(FILE_HANDLE* handle, char* block, int block_size, int* chars_read);
FILE_STATE file_close(FILE_HANDLE* handle);
FILE_STATE file_delete(const char* name);

#endif
