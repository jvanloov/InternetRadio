// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/************************************************
 * audio.h                                      *
 ************************************************
 * Audio driver for CC3200 and MCP4921 DAC      *
 * Joris Van Looveren, 10/2016                  *
 *                                              *
 * This driver uses timer A0 to drive the       *
 * MCP at the desired sample rate. The MCP has  *
 * LDAC connected to GND, so it immediately     *
 * outputs when CS goes high.                   *
 *                                              *
 * The MCP is driven over SPI. See audio.c for  *
 * the exact connections on the BoosterPack     *
 * headers.                                     *  
 ************************************************/

// Sample to return when there is nothing available in the queue
// For the MCP4921 (12-bit, i.e. values 0 .. 4096), return 2048
#define SILENCE ((uint16_t)2048)

// Options to audio_buffer_store.
#define BUFFER_BLOCK      1
#define BUFFER_NO_BLOCK   0

// Return values for audio_buffer_store.
#define BUFFER_OK         1
#define BUFFER_FULL       2

// Driver states
#define STATE_PLAYING     1
#define STATE_STOPPED     2

// Initialize the audio interface
// This allocates the auto out buffer.
void audio_init(void);

// Set up the audio interface for the desired sample rate.
void audio_setup(uint32_t sample_rate);

// Current state of the driver.
// Possible values are the STATE_* values defined above.
int audio_state(void);

// Start audio output.
// The timer interrupt is enabled.
// If the buffer does not contain samples, the driver will
// still run, but play "silence" samples.
// (For the MCP4921 12-bit DAC, the value 2048 is output.)
void audio_start(void);

// Stop the audio output.
// The timer interrupt is disabled.
void audio_stop(void);

// Close the audio output.
// This deallocates the audio buffer.
void audio_close(void);

// Reset the buffer
void audio_buffer_clear();

// How many samples "lead" do we have, i.e. how many unplayed
// samples are there in the buffer? This is an approximate value!
int audio_buffer_lead();

// Store a sample in the buffer.
// If block == BUFFER_NO_BLOCK, the return value will
// be BUFFER_FULL or BUFFER_OK
// If block == BUFFER_BLOCK, the return value will be BUFFER_OK
int audio_buffer_store(uint16_t sample, int block);


// Store a block of samples in the buffer.
// If block == BUFFER_NO_BLOCK, the return value will
// be BUFFER_FULL or BUFFER_OK
// If block == BUFFER_BLOCK, the return value will be BUFFER_OK
int audio_buffer_store_block(uint16_t *samples, int numSamples, int block);
