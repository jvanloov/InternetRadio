// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/************************************************************
 * menu.h                                                   *
 ************************************************************
 * Declarations for the menu data structure.                *
 * Joris Van Looveren, 02/2018                              *
 ************************************************************/

#ifndef _MENU_H
#define _MENU_H

#define MENU_SUBMENU 0
#define MENU_ACTION 1

typedef struct menu MENU;

typedef int (*menu_action_type_fun)(void);
typedef void* (*menu_action_fun)(void* plugin_data, int entry_index);

struct menu {
  char *title;
  char **entry_names;
  menu_action_type_fun* entry_action_types;
  menu_action_fun* entry_actions;
  void** entry_data;
  int num_entries;
  int current_selection;
  struct menu* parent_menu;
};

// Default function to return MENU_ACTION
int menu_item_action_fun(void);

// Default function to return MENU_SUBMENU
int menu_item_submenu_fun(void);

// Initialize the menu.
// The title (in 'name') will be copied; all list members will be
// allocated and zeroed.
void menu_initialize(MENU* menu, const char* name, int number_of_items);

// Terminate the menu.
// All allocated memory will be freed.
void menu_terminate(MENU* menu);

#endif
