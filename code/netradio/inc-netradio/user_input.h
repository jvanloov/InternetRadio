// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/************************************************************
 * user_input.h                                             *
 ************************************************************
 * User input routines                                      *
 * Joris Van Looveren, 02/2017                              *
 *                                                          *
 * This module provides a few routines to get input from    *
 * the user using the buttons on the prototype PCB.         *
 * These include getting a no/yes style confirmation (left- *
 * most or rightmost button), and a multi-character word.   *
 ************************************************************/

#include "configuration.h"

// Get a string from the user through the 4-button user interface.
// The UI uses 2 modes:
//
//     Mode  |       0          |          1
// Button    |                  |      
// ----------+------------------+-----------------------
//   1       |  ^  previous     |  <  previous
//           |     character    |     position
//   2       |  v  next         |  >  next
//           |     character    |     position
//   3       | nxt confirm char | del delete char
//           |     + next pos   |     shift rest to left
//   4       | ... switch to    | cnf confirm string
//           |     mode 1       |
//
// The default mode is mode 0. After switching to mode 1, not
// pressing any button for 2-3 seconds will make the input switch
// back to mode 0.
// If the string becomes longer than the width of the screen,
// it scrolls along depending on the cursor position. Markers
// '<' and '>' will be shown to indicate that part(s) of the
// string are not shown on the screen:
//
//      +-------------------------+
//      |                         |
//      | <his is a partial stri> |
//      |                 ^       |
//      +-------------------------+
//
// ( ^ indicates the cursor position)
void user_input_get_string(char *inputstring, int max_inputstring_length);

// yes-no confirmation
int user_input_confirm(void);

// Dimensions to use for the list selector. (All dimensions
// are in pixels.)
#define LIST_LEFT 0
#define LIST_RIGHT SCREENW
#define LIST_TOP  10
#define LIST_ITEM_HEIGHT 7
// Number of rows to show.
// List height in pixels = LIST_ITEMS * LIST_ITEM_HEIGHT
#if DISPLAY_TYPE == nokia
#define LIST_ITEMS 4
#else
#define LIST_ITEMS 6
#endif

// Select an entry from a given list of entries.
// Inputs:
// - list: a list of strings
// - entry_len: the maximum length of each entry.
// - selected_entry: the index of the entry that is pre-selected.
//                   Set to -1 for no pre-selection; in this case
//                   the first entry will be pre-selected.
// Returns: the index of the selected entry. -1 is returned if the
//          user cancelled.
// The list will show LIST_ITEMS entries. If the list contains more
// entries, arrows will be shown ('^' and 'v') on the top and bottom
// right of the screen to indicate that there are more items than
// shown.
// If the user scrolls past the bottom (top) of the list, the
// selection returns to the top (bottom) of the list.
int user_input_select_from_list(char **list, int num_entries, int entry_len, int selected_entry);
