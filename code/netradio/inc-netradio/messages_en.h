// Copyright 2019 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/************************************************
 * messages.h                                   *
 ************************************************
 * All strings for netradio                     *
 ************************************************/

#ifndef _MESSAGES_H
#define _MESSAGES_H

#define MSG_YES  "yes"
#define MSG_NO   "no"

// main.c
#define MSG_LOADING_SL_COULD_NOT_LOAD  "Could not load\n"
#define MSG_LOADING_SL_CANNOT_CONTINUE "cannot continue"

#define MSG_MAIN_MENU "NetRadio"

// user_input.c
#define MSG_INPUT_STATUS_UP     "^"
#define MSG_INPUT_STATUS_DOWN   "`"
#define MSG_INPUT_STATUS_NEXT   "next"
#define MSG_INPUT_STATUS_MORE   "..."
#define MSG_INPUT_STATUS_LEFT   "<"
#define MSG_INPUT_STATUS_RIGHT  ">"
#define MSG_INPUT_STATUS_DEL    "del"
#define MSG_INPUT_STATUS_DONE   "done"
#define MSG_INPUT_STATUS_BACK   "back"
#define MSG_INPUT_STATUS_SELECT "sel"

// network.c
#define MSG_NETWORK_CHOOSE    "Choose your network:"
#define MSG_NETWORK_ENTER_PWD "Enter the password:"
#define MSG_NETWORK_CONFIRM   "Confirm?"
#define MSG_NETWORK_TRYING_TO_CONNECT "Connecting to:"
#define MSG_NETWORK_CONNECTION_OK     "# connected"
#define MSG_NETWORK_STORING_NETWORK   "# storing network"
#define MSG_NETWORK_CONNECTION_FAILED "X connection failed"
// "signal" sign
#define MSG_NETWORK_WIFI_NETWORK      "\\"

// most_recent_plugin.c
#define MSG_MRP_NO_SELECTION     "<no selection>"
#define MSG_MRP_NO_MOST_RECENT_1 "No most recent"
#define MSG_MRP_NO_MOST_RECENT_2 "station to play!"

// streaming_callbacks.c
#define MSG_SC_STOP "stop"

// player.c
#define MSG_PLAYER_CLOSED_CONNECTION "closed connection"

// system_menu_plugin.c
#define MSG_SM_MENUNAME "System menu"
#define MSG_SM_RESET_ITEM "Reset networks"
#define MSG_SM_RESET_ITEM_CONFIRMED "Deleted all profiles"
#define MSG_SM_RESET_ITEM_CANCELLED "Cancelled"
#define MSG_SM_CLEAR_ITEM "Clear station list"

#endif
