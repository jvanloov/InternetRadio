// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/************************************************************
 * network.h                                                *
 ************************************************************
 * Networking routines                                      *
 * Joris Van Looveren, 02/2017                              *
 *                                                          *
 * This module provides a number of routines related to the *
 * networking component. These include both lower-level     *
 * SimpleLink routines, and higher-level routines for       *
 * selecting a WiFi network, and the startup routine of     *
 * trying to connect to a known network, and prompting the  *
 * user if no known network can be connected to.            *
 ************************************************************/

#include "common.h"
#include "simplelink.h"
#include "configuration.h"


// Application specific status/error codes
typedef enum{
    // Choosing -0x7D0 to avoid overlap w/ host-driver's error codes
    LAN_CONNECTION_FAILED = -0x7D0,       
    INTERNET_CONNECTION_FAILED = LAN_CONNECTION_FAILED - 1,
    DEVICE_NOT_IN_STATION_MODE = INTERNET_CONNECTION_FAILED - 1,
    NETWORK_CONNECTED_AUTO = DEVICE_NOT_IN_STATION_MODE - 1,

    STATUS_CODE_MAX = -0xBB8
} e_AppStatusCodes;


void network_init(void);
long network_start(void);
long network_stop(void);
long network_configure_default_state(void);
long network_scan_wifi_networks(void);
long network_connect(char *ssid, char *key, int sec_type);


int network_get_wifi_list();
void network_select_from_list(char *network_name, int num_networks, int max_name_len);
void network_connect_or_select();

// parse_ip and parse_port
// Given a string like this one: "5.48.54.214:6587", parse out the IP address
// respectively the port number, and return as a number. If any errors are
// encountered during parsing, -1 is returned.
// (The return types are explicitly signed to be able to return -1.)
signed long network_parse_ip(char *ipstr);
signed int network_parse_port(char *ipstr);
