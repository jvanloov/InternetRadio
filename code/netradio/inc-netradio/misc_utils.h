// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/************************************************************
 * misc_utils.h                                             *
 ************************************************************
 * Miscellaneous utility routines                           *
 * Joris Van Looveren, 04/2017                              *
 *                                                          *
 * This file provides a collection of useful routines, to   *
 * be used in the other files.                              *
 ************************************************************/

// Parse an unsigned number from a string. 
// The parameter maxlen specifies the maximum number of digits
// that will be parsed from the string, even if it contains
// more digits.
// If any other character besides 0 .. 9 is encountered during
// parsing, parsing is interrupted and -1 is returned.
// If more than 9 characters characters are requested (string
// length or maxlen), we return -1. (We don't want to risk
// going over INT_MAX for a 32-bit signed number.)
int parse_unsigned_num(char *str, int maxlen);

// Helper functions to construct lines, e.g. for saving files.
// string_build initializes the process. Give it a pointer to
// a character array and a maxlen. It will initialize this buffer
// to all-zeroes. Repeatedly calling string_add will append
// 'part' to the buffer, until the buffer is full. When the
// buffer is full, no more characters are added to it.
void string_build(char* str, int maxlen);
void string_add(const char *part);
void string_add_substring(const char *part, int start_pos, int end_pos);

int memory_get_available(void);
