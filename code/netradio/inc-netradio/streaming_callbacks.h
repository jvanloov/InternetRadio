// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/************************************************************
 * streaming_callbacks.h                                    *
 ************************************************************
 * Callbacks for network streaming                          *
 * Joris Van Looveren, 03/2017                              *
 *                                                          *
 * These routines implement the functionality to retrieve   *
 * MP3 data from the stream socket, and feed it to the      *
 * Helix decoder.                                           *
 * more_data_cb is called by the player when it wants more  *
 * data. The callback does 3 things:                        *
 * - if the user presses "stop", it returns 0 to signal     *
 *   to the player that it should stop playing.             *
 * - if the buffer contains less data than "requested_size" *
 *   the callback will request a block of data from the     *
 *   socket, and store it in the buffer.                    *
 * - finally, the requested amount of data is copied into   *
 *   buffer, and returned to the player                     *
 * the stream_info_cb callback sends some info from the     *
 * stream to the terminal.                                  *
 ************************************************************/

#ifndef _STREAMING_CALLBACKS_H
#define _STREAMING_CALLBACKS_H

#include <inttypes.h>

typedef struct {
  int sample_rate;
  int bitrate;
  int channels;
} stream_info;


typedef int (*get_more_data_callback)(uint8_t *buffer, int num_bytes);
typedef void (*stream_info_callback)(stream_info *str_info);

void streaming_buffer_init(void);
int streaming_buffer_readahead();
int streaming_buffer_store(char *block, int block_size);
int streaming_buffer_destroy(void);

int more_data_cb(uint8_t *buffer, int requested_size);
void stream_info_cb(stream_info *str_info);

#endif
