// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef _letters_h
#define _letters_h

/*
 * Font definition.
 * Conventions:
 * - Every character is max. 1 byte "wide" and always 6 bytes "high" (6 rows)
 *   (most characters are 4 bits wide, some are 3 bits wide, and some 5.)
 * - The LSB of the first byte of each character denotes whether the character
 *   is always followed by an empty column (value = 1; typically used for digits), 
 *   or whether "variable spacing" mode is active (value = 0).
 * - The LSBs of the five other bytes of each character represent the "length"
 *   of the character in bits (e.g. 0,0,0,1,1 = 3)
 */

unsigned char alc[6] = {0b00000000,0b01100000,0b00010000,0b01110001,0b10010000,0b01110000};
unsigned char blc[6] = {0b10000000,0b11100000,0b10010000,0b10010001,0b10010000,0b01100000};
unsigned char clc[6] = {0b00000000,0b01100000,0b10010000,0b10000001,0b10010000,0b01100000};
unsigned char dlc[6] = {0b00010000,0b01110000,0b10010000,0b10010001,0b10010000,0b01100000};
unsigned char elc[6] = {0b00000000,0b01100000,0b10010000,0b11110001,0b10000000,0b01100000};
unsigned char flc[6] = {0b01100000,0b10000000,0b11000000,0b10000000,0b10000001,0b10000001};
unsigned char glc[6] = {0b00000000,0b01100000,0b10010000,0b01110001,0b00010000,0b11100000};
unsigned char hlc[6] = {0b10000000,0b11100000,0b10010000,0b10010001,0b10010000,0b10010000};
unsigned char ilc[6] = {0b10000000,0b00000000,0b10000000,0b10000000,0b10000000,0b10000001};
unsigned char jlc[6] = {0b00010000,0b00000000,0b00010000,0b00010001,0b10010000,0b01100000};
unsigned char klc[6] = {0b10000000,0b10010000,0b10100000,0b11000001,0b10100000,0b10010000};
unsigned char llc[6] = {0b10000000,0b10000000,0b10000000,0b10000000,0b10000001,0b01100001};
unsigned char mlc[6] = {0b00000000,0b11110000,0b10101000,0b10101001,0b10101000,0b10101001};
unsigned char nlc[6] = {0b00000000,0b11100000,0b10010000,0b10010001,0b10010000,0b10010000};
unsigned char olc[6] = {0b00000000,0b01100000,0b10010000,0b10010001,0b10010000,0b01100000};
unsigned char plc[6] = {0b00000000,0b11100000,0b10010000,0b11100001,0b10000000,0b10000000};
unsigned char qlc[6] = {0b00000000,0b01110000,0b10010000,0b01110001,0b00010000,0b00010000};
unsigned char rlc[6] = {0b00000000,0b10110000,0b11000000,0b10000001,0b10000000,0b10000000};
unsigned char slc[6] = {0b00000000,0b01110000,0b10000000,0b01100001,0b00010000,0b11100000};
unsigned char tlc[6] = {0b10000000,0b11100000,0b10000000,0b10000000,0b10000001,0b01100001};
unsigned char ulc[6] = {0b00000000,0b10010000,0b10010000,0b10010001,0b10010000,0b01100000};
unsigned char vlc[6] = {0b00000000,0b10100000,0b10100000,0b10100000,0b10100001,0b01000001};
unsigned char wlc[6] = {0b00000000,0b10001000,0b10001000,0b10101001,0b10101000,0b01010001};
unsigned char xlc[6] = {0b00000000,0b10100000,0b10100000,0b01000000,0b10100001,0b10100001};
unsigned char ylc[6] = {0b00000000,0b10010000,0b10010000,0b01110001,0b00010000,0b01100000};
unsigned char zlc[6] = {0b00000000,0b11110000,0b00100000,0b01000001,0b10000000,0b11110000};

unsigned char au[6] = {0b01100000,0b10010000,0b10010000,0b11110001,0b10010000,0b10010000};
unsigned char bu[6] = {0b11100000,0b10010000,0b11100000,0b10010001,0b10010000,0b11100000};
unsigned char cu[6] = {0b01100000,0b10010000,0b10000000,0b10000001,0b10010000,0b01100000};
unsigned char du[6] = {0b11100000,0b10010000,0b10010000,0b10010001,0b10010000,0b11100000};
unsigned char eu[6] = {0b11110000,0b10000000,0b11100000,0b10000001,0b10000000,0b11110000};
unsigned char fu[6] = {0b11110000,0b10000000,0b11100000,0b10000001,0b10000000,0b10000000};
unsigned char gu[6] = {0b01100000,0b10010000,0b10000000,0b10110001,0b10010000,0b01100000};
unsigned char hu[6] = {0b10010000,0b10010000,0b11110000,0b10010001,0b10010000,0b10010000};
unsigned char iu[6] = {0b11100000,0b01000000,0b01000000,0b01000000,0b01000001,0b11100001};
unsigned char ju[6] = {0b00110000,0b00010000,0b00010000,0b00010001,0b10010000,0b01100000};
unsigned char ku[6] = {0b10010000,0b10100000,0b11000000,0b10100001,0b10010000,0b10010000};
unsigned char lu[6] = {0b10000000,0b10000000,0b10000000,0b10000000,0b10000001,0b11110001};
unsigned char mu[6] = {0b10001000,0b11011000,0b10101000,0b10001001,0b10001000,0b10001001};
unsigned char nu[6] = {0b10010000,0b11010000,0b10110000,0b10010001,0b10010000,0b10010000};
unsigned char ou[6] = {0b01100000,0b10010000,0b10010000,0b10010001,0b10010000,0b01100000};
unsigned char pu[6] = {0b11100000,0b10010000,0b10010000,0b11100001,0b10000000,0b10000000};
unsigned char qu[6] = {0b01100000,0b10010000,0b10010000,0b10010001,0b10100000,0b01010000};
unsigned char ru[6] = {0b11100000,0b10010000,0b11100000,0b10010001,0b10010000,0b10010000};
unsigned char su[6] = {0b01110000,0b10000000,0b01100000,0b00010001,0b00010000,0b11100000};
unsigned char tu[6] = {0b11100000,0b01000000,0b01000000,0b01000000,0b01000001,0b01000001};
unsigned char uu[6] = {0b10010000,0b10010000,0b10010000,0b10010001,0b10010000,0b01100000};
unsigned char vu[6] = {0b10100000,0b10100000,0b10100000,0b10100000,0b10100001,0b01000001};
unsigned char wu[6] = {0b10001000,0b10001000,0b10001000,0b10101001,0b10101000,0b01010001};
unsigned char xu[6] = {0b10100000,0b10100000,0b01000000,0b10100000,0b10100001,0b10100001};
unsigned char yu[6] = {0b10100000,0b10100000,0b10100000,0b01000000,0b01000001,0b01000001};
unsigned char zu[6] = {0b11110000,0b00010000,0b00100000,0b01000001,0b10000000,0b11110000};

unsigned char space[6] = {0b00000000,0b00000000,0b00000000,0b00000000,0b00000001,0b00000001};
unsigned char space_smallc[6] = {0b00000000,0b00000000,0b00000000,0b00000000,0b00000001,0b00000000};
unsigned char dot[6] = {0b00000000,0b00000000,0b00000000,0b00000000,0b00000001,0b01000000};
unsigned char quote[6] = {0b01000000,0b01000000,0b10000000,0b00000000,0b00000001,0b00000000};
unsigned char comma[6] = {0b00000000,0b00000000,0b00000000,0b00000000,0b01000001,0b10000000};
unsigned char colon[6] = {0b00000001,0b00000000,0b10000000,0b00000000,0b10000000,0b00000001};
unsigned char semicolon[6] = {0b00000000,0b00000000,0b01000000,0b00000000,0b01000001,0b10000001};
unsigned char exclamation[6] = {0b01000000,0b11100000,0b11100000,0b01000000,0b00000001,0b01000001};
unsigned char question[6] = {0b01100000,0b00010000,0b01100000,0b01000001,0b00000000,0b01000000};
unsigned char dash[6] = {0b00000000,0b00000000,0b00000000,0b11100000,0b00000001,0b00000001};
unsigned char at[6] = {0b00000000,0b01100000,0b10110000,0b10110001,0b10000000,0b01100000};
unsigned char equal[6] = {0b00000000,0b00000000,0b11100000,0b00000000,0b11100001,0b00000001 };
unsigned char up_arr[6] = {0b00100000,0b01110000,0b10101000,0b00100001,0b00100000,0b00000001 };
unsigned char down_arr[6] = {0b00000000,0b00100000,0b00100000,0b10101001,0b01110000,0b00100001 };
unsigned char left_arr[6] = {0b00000000,0b00100000,0b01000000,0b11111001,0b01000000,0b00100001 };
unsigned char right_arr[6] = {0b00000000,0b00100000,0b00010000,0b11111001,0b00010000,0b00100001 };

unsigned char spinner1[6] = {0b00000000,0b00000000,0b11111000,0b00000001,0b00000000,0b00000001 };
unsigned char spinner2[6] = {0b10000000,0b01000000,0b00100000,0b00010001,0b00001000,0b00000001 };
unsigned char spinner3[6] = {0b00100000,0b00100000,0b00100000,0b00100001,0b00100000,0b00000001 };
unsigned char spinner4[6] = {0b00001000,0b00010000,0b00100000,0b01000001,0b10000000,0b00000001 };

unsigned char signal[6] = {0b00000000,0b01000000,0b10010000,0b10100101,0b10010001,0b01000001 };
unsigned char checkmark[6] = {0b00000000,0b00000010,0b00000100,0b10001001,0b01010001,0b00100001 };

unsigned char n1[6] = {0b00100001,0b01100000,0b10100000,0b00100000,0b00100001,0b00100001};
unsigned char n2[6] = {0b01100001,0b10010000,0b00010000,0b01100001,0b10000000,0b11110000};
unsigned char n3[6] = {0b01100001,0b10010000,0b00100000,0b00010001,0b10010000,0b01100000};
unsigned char n4[6] = {0b00100001,0b01100000,0b10100000,0b11110001,0b00100000,0b00100000};
unsigned char n5[6] = {0b11110001,0b10000000,0b11100000,0b00010001,0b10010000,0b01100000};
unsigned char n6[6] = {0b01100001,0b10000000,0b11100000,0b10010001,0b10010000,0b01100000};
unsigned char n7[6] = {0b11110001,0b00010000,0b00100000,0b00100001,0b00100000,0b00100000};
unsigned char n8[6] = {0b01100001,0b10010000,0b01100000,0b10010001,0b10010000,0b01100000};
unsigned char n9[6] = {0b01100001,0b10010000,0b01110000,0b00010001,0b10010000,0b01100000};
unsigned char n0[6] = {0b01100001,0b10010000,0b10010000,0b10010001,0b10010000,0b01100000};

unsigned char *characters[] = {
	space, exclamation, comma, dash, dot, n0, n1,
	n2, n3, n4, n5, n6, n7, n8, n9, colon, semicolon,
	at, au, bu, cu, du, eu,	fu, gu, hu, iu, ju, ku,
	lu, mu, nu, ou, pu, qu, ru, su, tu, uu, vu, wu,
	xu, yu, zu, alc, blc, clc, dlc, elc, flc, glc, hlc, ilc,
	jlc, klc, llc, mlc, nlc, olc, plc, qlc, rlc, slc, tlc, ulc,
	vlc, wlc, xlc, ylc, zlc, question, equal, up_arr, down_arr,
        spinner1, spinner2, spinner3, spinner4, left_arr, right_arr,
        signal, checkmark};

// ASCII SET
// The original ASCII table can be used to construct strings. They
// will then print using the "custom" character set.
// Original ASCII table (for reference):
//   0-  9  NUL SOH STX ETX EOT ENQ ACK BEL BS  TAB
//  10- 19  LF  VT  FF  CR  SO  SI  DLE DC1 DC2 DC3
//  20- 29  DC4 NAK SYN ETB CAN EM  SUB ESC FS  GS
//  30- 39  RS  US  SPC  !   "   #   $   %   &   '
//  40- 49   (   )   *   +   ,   -   .   /   0   1
//  50- 59   2   3   4   5   6   7   8   9   :   ;
//  60- 69   <   =   >   ?   @   A   B   C   D   E
//  70- 79   F   G   H   I   J   K   L   M   N   O
//  80- 89   P   Q   R   S   T   U   V   W   X   Y
//  90- 99   Z   [   \   ]   ^   _   `   a   b   c
// 100-109   d   e   f   g   h   i   j   k   l   m
// 110-119   n   o   p   q   r   s   t   u   v   w
// 120-127   x   y   z   {   |   }   ~  DEL
// Our character set:
//   0-  9   
//  10- 19
//  20- 29
//  30- 39          SPC  !      chk 
//  40- 49                   ,   -   .   /   0   1
//  50- 59   2   3   4   5   6   7   8   9   :   ;
//  60- 69   <   =   >   ?   @   A   B   C   D   E
//  70- 79   F   G   H   I   J   K   L   M   N   O
//  80- 89   P   Q   R   S   T   U   V   W   X   Y
//  90- 99   Z      sig     uar     dar  a   b   c
// 100-109   d   e   f   g   h   i   j   k   l   m
// 110-119   n   o   p   q   r   s   t   u   v   w
// 120-127   x   y   z  sp1 sp2 sp3 sp4
// dar = down arrow
// uar = up arrow
// sp? = 4 states for the "spinner"

short ascii_set[] = {
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1,   //   0-  9
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1,   //  10- 19
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1,   //  20- 29
	-1, -1,  0,  1, -1, 81, -1, -1, -1, -1,   //  30- 39
	-1, -1, -1, -1,  2,  3,  4, 77,  5,  6,   //  40- 49
	 7,  8,  9, 10, 11, 12, 13, 14, 15, 16,   //  50- 59
	78, 71, 79, 70, 17, 18, 19, 20, 21, 22,   //  60- 69
	23, 24, 25, 26, 27, 28, 29, 30, 31, 32,   //  70- 79
	33, 34, 35, 36, 37, 38, 39, 40, 41, 42,   //  80- 89
	43, -1, 80, -1, 72, -1, 73, 44, 45, 46,   //  90- 99
	47, 48, 49, 50, 51, 52, 53, 54, 55, 56,   // 100-109
	57, 58, 59, 60, 61, 62, 63, 64, 65, 66,   // 110-119
	67, 68, 69, 74, 75, 76, 77, -1            // 120-127
};

#endif /* _letters_h */

