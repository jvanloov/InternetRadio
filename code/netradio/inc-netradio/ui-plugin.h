// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/************************************************************
 * ui-plugin.h                                              *
 ************************************************************
 * UI plugin declarations                                   *
 * Joris Van Looveren, 02/2018                              *
 *                                                          *
 * The UI / menu system is composed of a set of "plugins".  *
 ************************************************************/

#ifndef _UI_PLUGIN_H
#define _UI_PLUGIN_H

#include "menu.h"
#include "stations_data.h"

// Internal state data for the UI plugin.
// It is declared as a void* so it can hold basically anything.
typedef void* UI_PLUGIN_DATA;

// The API for a plugin.
// This is a set of functions that are called at different times when the
// user is interacting with the UI.
typedef struct {
  // Initialize
  // This function is called each time the main menu is shown, e.g. when the
  // radio is turned on, or when "back" is pressed in a lower-level menu.
  // In this function, the plugin should initialize its internal data.
  int (*initialize)(UI_PLUGIN_DATA plugin_data, STATION_DATA* global_station_list, MENU* parent_menu);
  
  // Cleanup
  int (*cleanup)(UI_PLUGIN_DATA plugin_data);

  // Terminate
  // This function is called when the UI exits. On the device, this should not
  // happen. In the simulator, this happens when the user hits 'q' in a menu.
  int (*terminate)(UI_PLUGIN_DATA plugin_data);

  // Get_menuitem
  // This function returns the label that should be displayed for this
  // menu item. This can be a static text ("System menu") or a dynamic text
  // (e.g. a station name)
  char* (*get_menuitem)(UI_PLUGIN_DATA plugin_data);

  // Get_menuactiontype
  // This function is executed by the menu system when the menu item for this
  // plugin is selected. This function should return one of two values:
  // - MENU_SUBMENU: this indicates that executing the function returned by
  //                 get_menuitemaction will return a MENU*, which will then be
  //                 displayed by the menu system.
  // - MENU_ACTION:  executing the function returned by get_menuitemaction
  //                 will perform an action, such as playing a stream. The return
  //                 value of this function will be ignored.
  menu_action_type_fun get_menuitemactiontype;

  // Get_menuitemaction
  // This function will either return a submenu, or perform an actual action.
  menu_action_fun get_menuitemaction;
} UI_PLUGIN_API;

#endif
