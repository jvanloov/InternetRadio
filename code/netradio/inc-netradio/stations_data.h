// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/************************************************************
 * stations_data.h                                          *
 ************************************************************
 * Read and write station data to/from file                 *
 * Joris Van Looveren                                       *
 ************************************************************/

#ifndef _STATIONS_DATA_H
#define _STATIONS_DATA_H

#include "hal_mcu.h"

#include "configuration.h"
#include "circular_data_buffer.h"


typedef struct station_entry {
  int index;
  char short_name[STATION_ENTRY_NAME_LENGTH];
  char ip[STATION_ENTRY_IP_LENGTH];
  int port;
  char url[STATION_ENTRY_QUERY_LENGTH];
} STATION;

typedef struct station_data {
  char category_name[STATION_ENTRY_NAME_LENGTH];
  STATION *stations;
  int num_stations;
  int max_stations;
  // Temporary buffer, used during load
  circular_data_buffer *buffer;
} STATION_DATA;


// Initialize the station list data structure
// After initialization, the FDS expects to read the favorites data
// from text data. The text data can be passed as arbitrary chunks of
// text. The FDS will process all chunks that are passed to it.
// - The station list data structure uses a circular buffer internally
//   during initialization. buffer and buffer_size provide the storage
//   for this circular buffer.
// - station_list and max_stations provide the storage for the actual
//   station list.
int stations_init(STATION_DATA *station_data, int max_stations);

// Clean up allocated memory
void stations_destroy(STATION_DATA* station_data);

// Read a chunk of text. This chunk can be of arbitrary length (e.g.
// as received from a TCP socket); this method will buffer the text into
// the internal circular buffer, and split it into lines. The lines are
// passed to data_process_line to extract the actual data.
int stations_read_chunk(STATION_DATA *station_data, const char *chunk, int chunk_size);

// This function processes the favorites data line-by-line. 
void stations_process_line(STATION_DATA *station_data,
		       const char *line,
		       int line_len);

// Return 1 if there is a full line present in the buffer; return 0 otherwise
int stations_has_next_line(STATION_DATA *station_data);

// Retrieve the next line from the buffer. The line is stored in the line_buffer buffer.
int stations_get_next_line(STATION_DATA *station_data, char *line_buffer, int *line_buffer_len);

// Retrieve the list of stations from the station_data
void stations_get_stations(STATION_DATA *station_data, STATION **favorites, int *num_favorites);

// Load the list of stations from file
FILE_STATE stations_load(STATION_DATA *station_data, const char* file_name);

// Save the list of stations to file
void stations_save(STATION_DATA *station_data, const char* file_name);



#endif
