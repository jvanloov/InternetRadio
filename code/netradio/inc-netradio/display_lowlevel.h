// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/************************************************************
 * display_nokia-5110_lowlevel.c                            *
 ************************************************************
 * PCD8544-based LCD driver layer for CC3200                *
 * Joris Van Looveren, 03/2017                              *
 *                                                          *
 * This display driver provides controller routines for     *
 * a PCD8544 LCD controller. Connections:                   *
 * - GPIO 60: clock signal (CLK)                            *
 * - GPIO 58: data signal (DIN)                             *
 * - GPIO 53: data/command selection (D/C)                  *
 * - GPIO 50: chip select (CS)                              *
 * - GPIO 63: reset (RESET)                                 *
 *                                                          *
 * Inspired from                                            *
 * https://github.com/sparkfun/GraphicLCD_Nokia_5110/blob/  *
 *   master/Firmware/Nokia-5100-LCD-Example/LCD_Functions.h *
 ************************************************************/

/*** LOW-LEVEL INTERFACE ***/

// Do the lowlevel initialization of the display. 
void display_init_lowlevel(void);

// Send the buffer contents to the LCD driver chip.
void display_send_buffer(void);
void display_erase_buffer(void);
// Clear the display directly, by sending a string of 0's to the
// driver chip. Also reset the hardware cursor.
void display_erase(void);
void display_set_buffer_pixel(unsigned char x, unsigned char y,
			      unsigned char on);
int display_get_buffer_pixel(unsigned char x, unsigned char y);

