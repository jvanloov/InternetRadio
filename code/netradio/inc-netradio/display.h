// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/************************************************************
 * display.h                                                *
 ************************************************************
 * PCD8544-based LCD driver layer for CC3200                *
 * Joris Van Looveren, 03/2017                              *
 *                                                          *
 * This display driver provides controller routines for     *
 * a PCD8544 LCD controller. Connections:                   *
 * - GPIO 60: clock signal (CLK)                            *
 * - GPIO 58: data signal (DIN)                             *
 * - GPIO 53: data/command selection (D/C)                  *
 * - GPIO 50: chip select (CS)                              *
 * - GPIO 63: reset (RESET)                                 *
 *                                                          *
 ************************************************************/

// Initialize the display. The controller chip is reset,
// and the init command sequence is sent to the chip.
void display_initialize(void); 

// Clear the display. The controller buffer is overwritten
// with 0s, and the cursor is reset to position (0,0)
void display_clear(void);

// Set the controller's cursor position
void display_set_hw_cursor(unsigned char x, unsigned char y);

// Clear the display buffer
void display_clear_buffer(void);

// Clear a specific region of the display buffer. 
void display_clear_buffer_region(unsigned char x1, unsigned char y1,
			         unsigned char x2, unsigned char y2);

// Invert a specific region of the display buffer.
void display_invert_buffer_region(unsigned char x1, unsigned char y1,
				  unsigned char x2, unsigned char y2);

// Send the display buffer to the LCD controller
void display_draw_buffer(void);

// draw a bitmap at (left_x, top_y)
// bitmap encoding ...
void display_draw_bitmap(unsigned char left_x, unsigned char top_y,
			 unsigned char bitmap[],
			 int size_x, int size_y);

// Draw a rectangle (x1,y1)-(x2,y2) in the buffer 
void display_rectangle(unsigned char x1, unsigned char y1,
		       unsigned char x2, unsigned char y2);

// Set the cursor to the given (X,Y) position
// X = 0 .. 84
// Y = 0 .. 46
void display_setcursor(unsigned char x, unsigned char y);

int display_getcursor_x(void);
int display_getcursor_y(void);

// Print a string of characters, starting from the current
// cursor position. 
void display_print(char *text);

// Calculate the screen width of the given string.
int display_calculate_string_length(char *str);

// Calculate the screen width of the substring of str
// contained between start_pos and end_pos.
int display_calculate_substring_length(char *str, int start_pos, int end_pos);

// Update the status bar (= lowermost row) to show the
// give status text.
void display_update_status_bar(char *status);
// Update the status bar with a label for each key
void display_update_status_bar_keys(char *key1, char *key2, char *key3, char *key4);
