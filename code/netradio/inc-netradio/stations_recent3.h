// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/***********************************************
 * stations_recent3.h                          *
 ***********************************************
 * Keep the 3 most recent station names.       *
 ***********************************************/

#ifndef _STATIONS_RECENT3_H
#define _STATIONS_RECENT3_H

// Initialize the station list.
// If a file name is passed, the 3 station names are loaded
// from this file. If NULL is passed, the 3 station names
// are initialized to NULL.
void stations_recent3_init(const char* file_name);

// Add a station name to the list.
// If the name is already in the list, nothing happens.
// If not, it is inserted in the appropriate place, according
// to alphabetical order, as defined by the 'strcmp' C function.
// The station name is copied into a new memory area.
void stations_recent3_add(const char* file_name, char *station_name_orig);

// Get the station name from the list, as identified by 'pos'.
// 'pos' can have the values 1, 2 or 3.
char *stations_recent3_get(int pos);

// Print the station names in the terminal.
void stations_recent3_print(void);

// Clean up the recent3 list. This will free all
// allocated strings.
void stations_recent3_cleanup(void);

#endif
