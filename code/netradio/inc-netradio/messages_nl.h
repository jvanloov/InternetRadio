// Copyright 2019 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/************************************************
 * messages.h                                   *
 ************************************************
 * All strings for netradio                     *
 ************************************************/

#ifndef _MESSAGES_H
#define _MESSAGES_H

#define MSG_YES  "ja"
#define MSG_NO   "nee"

// main.c
#define MSG_LOADING_SL_COULD_NOT_LOAD  "Kan bestand niet laden:\n"
#define MSG_LOADING_SL_CANNOT_CONTINUE "Kan niet verder gaan"

#define MSG_MAIN_MENU "NetRadio"

// user_input.c
#define MSG_INPUT_STATUS_UP     "^"
#define MSG_INPUT_STATUS_DOWN   "`"
#define MSG_INPUT_STATUS_NEXT   "volgende"
#define MSG_INPUT_STATUS_MORE   "..."
#define MSG_INPUT_STATUS_LEFT   "<"
#define MSG_INPUT_STATUS_RIGHT  ">"
#define MSG_INPUT_STATUS_DEL    "wis"
#define MSG_INPUT_STATUS_DONE   "klaar"
#define MSG_INPUT_STATUS_BACK   "terug"
#define MSG_INPUT_STATUS_SELECT "kies"

// network.c
#define MSG_NETWORK_CHOOSE    "Kies WiFi netwerk:"
#define MSG_NETWORK_ENTER_PWD "Geef het paswoord in:"
#define MSG_NETWORK_CONFIRM   "Bevestigen?"
#define MSG_NETWORK_TRYING_TO_CONNECT "Verbinden met:"
#define MSG_NETWORK_CONNECTION_OK     "# verbonden"
#define MSG_NETWORK_STORING_NETWORK   "# netwerk opgeslagen"
#define MSG_NETWORK_CONNECTION_FAILED "X verbinding mislukt"
// "signal" sign
#define MSG_NETWORK_WIFI_NETWORK      "\\"

// most_recent_plugin.c
#define MSG_MRP_NO_SELECTION     "<geen keuze>"
#define MSG_MRP_NO_MOST_RECENT_1 "Geen recentste zender"
#define MSG_MRP_NO_MOST_RECENT_2 "om mee te verbinden!"

// streaming_callbacks.c
#define MSG_SC_STOP "stop"

// player.c
#define MSG_PLAYER_CLOSED_CONNECTION "verbinding verbroken"

// system_menu_plugin.c
#define MSG_SM_MENUNAME "Systeemmenu"
#define MSG_SM_RESET_ITEM "Vergeet WiFi netwerken"
#define MSG_SM_RESET_ITEM_CONFIRMED "Netwerken verwijderd"
#define MSG_SM_RESET_ITEM_CANCELLED "Geannuleerd"
#define MSG_SM_CLEAR_ITEM "Verwijder zenders"

#endif
