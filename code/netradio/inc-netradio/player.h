// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/************************************************************
 * player.c                                                 *
 ************************************************************
 * Audio player abstraction.                                *
 * Joris Van Looveren, 12/2016                              *
 ************************************************************
 * This module provides an abstraction for a music player.  *
 * The main entry point is "play_from_inet_stream", which   *
 * takes the connection data, and callbacks for requesting  *
 * more data, and returning stream info.                    *
 * Everything from connecting to the stream, error recovery *
 * etc. is handled in this module.                          *
 * Stop the player gracefully by letting the more_data_cb   *
 * return 0.                                                *
 ************************************************************/

//#include "mp3dec.h"
#include "simplelink.h"

#include "stations_data.h"
#include "streaming_callbacks.h"

// Map Helix error codes to Player error codes.
// For now, we just map one-on-one

enum {
  PLAYER_STATUS_OK                  =  0,     // ERR_MP3_NONE
  PLAYER_STATUS_INDATA_IUNDERFLOW   = -1,     // ERR_MP3_INDATA_UNDERFLOW
  PLAYER_STATUS_MAINDATA_UNDERFLOW  = -2,     // ERR_MP3_MAINDATA_UNDERFLOW
  PLAYER_STATUS_FREE_BITRATE_SYNC   = -3,     // ERR_MP3_FREE_BITRATE_SYNC
  PLAYER_STATUS_OUT_OF_MEMORY       = -4,     // ERR_MP3_OUT_OF_MEMORY
  PLAYER_STATUS_NULL_POINTER        = -5,     // ERR_MP3_NULL_POINTER
  PLAYER_STATUS_DECODE_ERROR        = -6,     // ERR_MP3_INVALID_FRAMEHEADER,
                                              // ERR_MP3_INVALID_SIDEINFO,
                                              // ERR_MP3_INVALID_SCALEFACT,
                                              // ERR_MP3_INVALID_HUFFCODES,
                                              // ERR_MP3_INVALID_DEQUANTIZE,
                                              // ERR_MP3_INVALID_IMDCT,
                                              // ERR_MP3_INVALID_SUBBAND
  PLAYER_STATUS_UNKNOWN_ERROR       = -9999,  // ERR_MP3_UNKNOWN

  PLAYER_NETWORK_CONNECT_ERROR      = -20000, // SimpleLink network connection error
  PLAYER_NETWORK_SEND_ERROR         = -20001, // SimpleLink sl_Send error
  PLAYER_NETWORK_RECV_ERROR         = -20002, // SimpleLink sl_Recv error
  PLAYER_STOPPED                    = -20003
};


void player_init(void);
void player_reset(void);
void player_close(void);

int is_playing(void);
int play(get_more_data_callback more_data_cb, stream_info_callback stream_info_cb);
int play_from_inet_stream(int iSockID, SlSockAddrIn_t *pAddr, char *request, get_more_data_callback more_data_cb, stream_info_callback stream_info_cb);

void player_play_station(STATION* data);
