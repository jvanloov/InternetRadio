// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/****************************************************************
 * PCM buffer callbacks                                         *
 * Joris Van Looveren, 12/2016                                  *
 ****************************************************************
 * This file declares a struct of five callback functions. They *
 * handle the PCM output of the modified Helix library.         *
 * The goal is to avoid having to allocate two 4.5KB PCM output *
 * buffers in order to be able to do double-buffering.          *
 * In "plain" form, Helix outputs all samples from an MP3 frame *
 * after each other in a large PCM output buffer (2304 2-byte   *
 * samples for a stereo MP3 stream).                            *
 * With these callbacks, we can start a new PCM buffer for every*
 * granule (= 64 samples (stereo) or 32 samples (mono).         *
 * We keep track of when a frame starts (and ends), because     *
 * in case of error, Helix wants to zero out the frame that is  *
 * in progress.                                                 *
 * - frame_start_callback                                       *
 *   lets the application record when a new frame starts        *
 * - frame_end_callback                                         *
 *   lets the application record when a frame ends              *
 * - clear_current_frame_callback                               *
 *   lets the application zero out from the start of the last   *
 *   frame up to the current position. This corresponds to the  *
 *   MP3ClearBadFrame function in the Helix code.               *
 * - get_buffer_callback(int stereo)                            *
 *   this function lets Helix request a new, fresh buffer in    *
 *   which it can write PCM samples. Using the 'stereo'         *
 *   argument, Helix will request a buffer sized for stereo     *
 *   output (64 samples) or mono output (32 samples)            *
 * - return_buffer_callback(int16_t *buf, int stereo)           *
 *   this function notifies the application when Helix has done *
 *   writing PCM output. The application can then handle the    *
 *   new samples (e.g., copy them to an actual output buffer)   *
 ****************************************************************/

#ifndef _HELIX_PCM_HANDLER_H
#define _HELIX_PCM_HANDLER_H


#define BUF_MONO       0
#define BUF_STEREO     1


typedef struct _pcm_buffer_callbacks {
  void (*frame_start_callback)(void);
  void (*frame_end_callback)(void);
  void (*clear_current_frame_callback)(void);
  int16_t *(*get_buffer_callback)(int stereo); 
  void (*return_buffer_callback)(int16_t *buf, int stereo);
} pcm_buffer_callbacks;


pcm_buffer_callbacks buf_callbacks;


void init_helix_pcm_handler(void);


#endif
