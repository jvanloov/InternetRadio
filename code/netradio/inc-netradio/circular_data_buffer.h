// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/************************************************************
 * circular_data_buffer.h                                   *
 ************************************************************
 * Simple ring buffer implementation                        *
 * Joris Van Looveren                                       *
 ************************************************************/


typedef struct circ_data_buf_s {
  char *data_buffer;
  int data_buffer_size;
  int read_pos;
  int write_pos;
  char wrapped;
  char empty;
} circular_data_buffer;

// Clear the storage buffer, and initialize all status data.
void circular_data_buffer_init(circular_data_buffer *d_buf,
			       char *d_buf_storage,
			       int d_buf_storage_size);
void circular_data_buffer_clear(circular_data_buffer *d_buf);
int circular_data_buffer_chars_left(circular_data_buffer *d_buf);
int circular_data_buffer_usage(circular_data_buffer *d_buf);
int circular_data_buffer_store(circular_data_buffer *d_buf, char ch);
int circular_data_buffer_store_block(circular_data_buffer *d_buf,
				     const char *block,
				     int block_size);
int circular_data_buffer_read(circular_data_buffer *d_buf);
int circular_data_buffer_read_block(circular_data_buffer *d_buf,
				    char *block,
				    int block_size);
int circular_data_buffer_contains_char(circular_data_buffer *d_buf,
				       char block);
