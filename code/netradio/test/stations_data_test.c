// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>   // for memset

#include "hal_mcu.h"
#include "stations_data.h"
#include "stations_recent3.h"
#include "configuration.h"

#define MAX_LINE_LENGTH 100

// To be tested still:
// - too long lines


/****** HELPER CODE *************************/

char test_buffer[10];
char test2_buffer[200];

static STATION favorites[GLOBAL_STATIONS_ENTRIES];
// This buffer is used to extract lines from the circular buffer
static char line_buffer[REMOTE_DATA_LINE_BUFFER_SIZE];
static int line_buffer_pos = 0;

static circular_data_buffer buffer;
static STATION_DATA test_data;

static char *test_file_name = "../../data/netradio-streams.txt";
static char *test2_file_name = "../../data/netradio-streams-2.txt";
static char *test3a_file_name = "../../data/most_recent_3a.txt";
static char *test3b_file_name = "../../data/most_recent_3b.txt";
static char *test3c_file_name = "../../data/most_recent_3c.txt";

static FILE *test_file = NULL;


int test_data_from_file_start() {
  test_file = fopen(test_file_name, "r");
  printf("Opening file (test)\n");
  return (test_file != NULL);
}

int test_data_from_file_start_2() {
  test_file = fopen(test2_file_name, "r");
  printf("Opening file (test2)\n");
  return (test_file != NULL);
}

int test_data_from_file_close() {
  if (test_file != NULL) {
    fclose(test_file);
    printf("Closed file\n");
  }
  return 1;
}


int test_data_from_file_load(char *buffer, int *buffer_size) {
  int max_buffer_size = *buffer_size;
  int chars_read = 0;
  char c = 0;

  for (chars_read=0; chars_read<max_buffer_size; chars_read++) {
    c = getc(test_file);
    //printf("%d ", c);
    if ((c == EOF) || (c == -1)) {   // stop at end of file or on error
      break;
    }
    buffer[chars_read] = c;    
  }

  *buffer_size = chars_read;
  printf("Read block (max: %d; read: %d)\n", max_buffer_size, chars_read);

  if (((c == EOF) || (c == -1)) && (chars_read == 0)) {
    return 0;
  } else {
    return 1;
  }
}


void data_show() {
  STATION *favorites;
  int num_favorites;

  stations_get_stations(&test_data, &favorites, &num_favorites);
  for (int i=0; i<num_favorites; i++) {
    printf("* %s (%s) [%s]\n", favorites[i].short_name, favorites[i].ip, favorites[i].url);
  }
}

FILE_STATE file_open_for_write(const char* name, FILE_HANDLE* handle) {
  FILE* f = NULL;

  f = fopen (name, "w");
  if (f != NULL) {
    handle->handle = f;
    handle->current_offset = 0;
    return FILE_OK;
  } else {
    return FILE_ERROR;
  }
}

FILE_STATE file_open_for_read(const char* name, FILE_HANDLE* handle) {
  FILE* f = NULL;

  f = fopen (name, "r");
  if (f != NULL) {
    handle->handle = f;
    handle->current_offset = 0;
    return FILE_OK;
  } else {
    return FILE_ERROR;
  }
}

FILE_STATE file_write_block(FILE_HANDLE* handle, char* block, int block_size) {
  size_t bytes_written = fwrite(block, 1, block_size, handle->handle);
  if (bytes_written == block_size) {
    return FILE_OK;
  } else {
    return FILE_ERROR;
  }
}

FILE_STATE file_read_block(FILE_HANDLE* handle, char* block, int block_size, int* chars_read) {
  size_t bytes_read = 0;
  bytes_read = fread (block, 1, block_size, handle->handle);
  if (bytes_read > 0) {
    handle->current_offset += bytes_read;
    *chars_read = bytes_read;
    return FILE_OK;
  } else {
    return FILE_ERROR;
  }
}

FILE_STATE file_close(FILE_HANDLE* handle) {
  if (fclose(handle->handle) == 0) {
    return FILE_OK;
  } else {
    return FILE_ERROR;
  }
}

FILE_STATE file_delete(const char* name) {
  return FILE_OK;
}


void term_print(const char *str) {
  printf("%s\n", str);
}

void term_print_val(char *str, int val) {
  printf("%s %d\n", str, val);
}

// From http://www.strudel.org.uk/itoa/
/**
 * C++ version char* style "itoa":
 */
/*
void strreverse(char* begin, char* end) {
  char aux;
  
  while(end>begin)
    aux=*end, *end--=*begin, *begin++=aux;
}

char* itoa( int value, char* result, int base ) {
  // check that the base if valid
  if (base < 2 || base > 16) { *result = 0; return result; }
	
  char* out = result;
  int quotient = value;
	
  do {
    *out = "0123456789abcdef"[ abs( quotient % base ) ];
    ++out;
    quotient /= base;
  } while ( quotient );
	
  // Only apply negative sign for base 10
  if ( value < 0 && base == 10) *out++ = '-';
	
  strreverse( result, out );
  *out = 0;
  return result;
}
*/

// -- this code initializes the buffer. this is
// -- the same as in stations_load. 
void set_up_buffer(STATION_DATA* test_data, circular_data_buffer* buffer) {
  int buffer_size = REMOTE_BUFFER_SIZE;

  char* buffer_storage;

  buffer_storage = (char*) malloc(buffer_size * sizeof(char));
  memset(&(buffer_storage[0]), 0, buffer_size);
  circular_data_buffer_init(buffer, &(buffer_storage[0]), buffer_size);

  test_data->buffer = buffer;
}

void tear_down_buffer(STATION_DATA* test_data) {
  free(test_data->buffer->data_buffer);
}


/****** TEST FUNCTIONS *************************
 * expected return values:                     *
 * 1 == success                                *
 * 0 == failure                                *
 ***********************************************/

#define ASSERT(x) (tests_OK = tests_OK && (x))

// to implement:
// - malformed data?
// - missing data in section?
// - multiple sections?
// - strip down the "http request"
// - ...

int test_has_next_line(void) {
  int tests_OK = 1;
  int res = 0;
  
  stations_init(&test_data, GLOBAL_STATIONS_ENTRIES);
  set_up_buffer(&test_data, &buffer);
  
  // in this call, has_next_line should return 0
  stations_read_chunk(&test_data, "abc", 3);
  res = stations_has_next_line(&test_data);
  ASSERT(res == 0);

  stations_read_chunk(&test_data, "abc\ndef", 7);
  res = stations_has_next_line(&test_data);
  ASSERT(res == 1);

  tear_down_buffer(&test_data);
  
  return tests_OK;
}


int test_get_next_line_nofullline(void) {
  int tests_OK = 1;
  int res = 0;
  
  stations_init(&test_data, GLOBAL_STATIONS_ENTRIES);
  set_up_buffer(&test_data, &buffer);
  
  // in this call, has_next_line should return 0
  stations_read_chunk(&test_data, "abc", 3);
  res = stations_get_next_line(&test_data, &(line_buffer[0]), &line_buffer_pos);  // no line -> res must be 0

  tear_down_buffer(&test_data);
  
  ASSERT(res == 0);
  
  return tests_OK;
}

int test_get_next_line_onefullline(void) {
  int tests_OK = 1;
  int res = 0;
  
  stations_init(&test_data, GLOBAL_STATIONS_ENTRIES);
  set_up_buffer(&test_data, &buffer);
  
  // in this call, has_next_line should return 1
  stations_read_chunk(&test_data, "abc", 3);
  stations_read_chunk(&test_data, "abc\n", 4);
  res = stations_get_next_line(&test_data, &(line_buffer[0]), &line_buffer_pos);  // line found -> res must be 1

  tear_down_buffer(&test_data);
  
  ASSERT(res == 1);
  
  return tests_OK;
}


int test_read_file(void) {
  int tests_OK = 1;
  int res = 0;
  
  stations_init(&test_data, GLOBAL_STATIONS_ENTRIES);
  set_up_buffer(&test_data, &buffer);
  
  stations_load(&test_data, "../../data/favorites.txt");

  ASSERT(res == 0);
  
  return tests_OK;
}

/*int test_save_selected3(void) {
  int tests_OK = 1;
  int res = 0;
  
  stations_recent3_save(test3a_file_name, "test1", NULL, NULL);
  stations_recent3_save(test3b_file_name, "test1", "test2", NULL);
  stations_recent3_save(test3c_file_name, "test1", "test2", "test3");

  char* name1 = NULL;
  char* name2 = NULL;
  char* name3 = NULL;
  
  stations_recent3_load(test3a_file_name, &name1, &name2, &name3);

  ASSERT(strcmp(name1, "test1") == 0);
  ASSERT(name2 == NULL);
  ASSERT(name3 == NULL);

  free(name1);
  free(name2);
  free(name3);

  stations_recent3_load(test3b_file_name, &name1, &name2, &name3);
  
  ASSERT(strcmp(name1, "test1") == 0);
  ASSERT(strcmp(name2, "test2") == 0);
  ASSERT(name3 == NULL);

  free(name1);
  free(name2);
  free(name3);

  stations_recent3_load(test3c_file_name, &name1, &name2, &name3);
  
  ASSERT(strcmp(name1, "test1") == 0);
  ASSERT(strcmp(name2, "test2") == 0);
  ASSERT(strcmp(name3, "test3") == 0);

  free(name1);
  free(name2);
  free(name3);

  return tests_OK;
}


int test_add_selected3(void) {
  int tests_OK = 1;

  stations_recent3_init(NULL);

  printf("name1: %d\n", (stations_recent3_get(1) == NULL));

  // add station to empty list. must be placed in position 1
  stations_recent3_add(NULL, "VRT Radio 1");
  ASSERT(strcmp(stations_recent3_get(1), "VRT Radio 1") == 0);
  ASSERT(stations_recent3_get(2) == NULL);
  ASSERT(stations_recent3_get(3) == NULL);  
  // re-add same station. re-add in position 1
  stations_recent3_add(NULL, "VRT Radio 1");
  ASSERT(strcmp(stations_recent3_get(1), "VRT Radio 1") == 0);
  ASSERT(stations_recent3_get(2) == NULL);
  ASSERT(stations_recent3_get(3) == NULL);  
  // Studio Brussel must be added first, shifting VRT Radio 1 to second place
  stations_recent3_add(NULL, "Studio Brussel");
  ASSERT(strcmp(stations_recent3_get(1), "Studio Brussel") == 0);
  ASSERT(strcmp(stations_recent3_get(2), "VRT Radio 1") == 0);
  ASSERT(stations_recent3_get(3) == NULL);  
  // Studio Brussel must be added first, shifting VRT Radio 1 to second place
  stations_recent3_add(NULL, "Studio Brussel");
  ASSERT(strcmp(stations_recent3_get(1), "Studio Brussel") == 0);
  ASSERT(strcmp(stations_recent3_get(2), "VRT Radio 1") == 0);
  ASSERT(stations_recent3_get(3) == NULL);  
  // Ketnet Radio must be added first, shifting the other two one place down
  stations_recent3_add(NULL, "Ketnet Radio");
  ASSERT(strcmp(stations_recent3_get(1), "Ketnet Radio") == 0);
  ASSERT(strcmp(stations_recent3_get(2), "Studio Brussel") == 0);
  ASSERT(strcmp(stations_recent3_get(3), "VRT Radio 1") == 0);
  // Zen FM should replace VRT Radio 1
  stations_recent3_add(NULL, "Zen FM");
  ASSERT(strcmp(stations_recent3_get(1), "Ketnet Radio") == 0);
  ASSERT(strcmp(stations_recent3_get(2), "Studio Brussel") == 0);
  ASSERT(strcmp(stations_recent3_get(3), "Zen FM") == 0);
  // 
  stations_recent3_add(NULL, "Klara Continuo");
  ASSERT(strcmp(stations_recent3_get(1), "Ketnet Radio") == 0);
  ASSERT(strcmp(stations_recent3_get(2), "Klara Continuo") == 0);
  ASSERT(strcmp(stations_recent3_get(3), "Studio Brussel") == 0);
  // 
  stations_recent3_add(NULL, "Joe FM");
  stations_recent3_print();
  ASSERT(strcmp(stations_recent3_get(1), "Joe FM") == 0);
  ASSERT(strcmp(stations_recent3_get(2), "Ketnet Radio") == 0);
  ASSERT(strcmp(stations_recent3_get(3), "Klara Continuo") == 0);


  return tests_OK;
}
*/

/****** TEST RUNNER ***************************/

struct test {
  char *name;
  int (*test_function)(void);
};

struct test all_tests[] = {
  { .name = "test data_has_next_line",
    .test_function = test_has_next_line },
  { .name = "test data_get_next_line (no full line)",
    .test_function = test_get_next_line_nofullline },
  { .name = "test data_get_next_line (one full line)",
    .test_function = test_get_next_line_onefullline },
  { .name = "test stations_load)",
    .test_function = test_read_file },
  //  { .name = "test save_selected3",
  //  .test_function = test_save_selected3 },
  //  { .name = "test add_selected3",
  //  .test_function = test_add_selected3 },
};

int main(int argc, const char* argv[]) {
  int num_tests = sizeof(all_tests)/sizeof(all_tests[0]);
  int successes = 0;

  for (int i=0; i<num_tests; i++) {
    struct test this_test = all_tests[i];
    int result = 0;
    
    printf(">>>> test: %s\n", this_test.name);
    result = this_test.test_function();
    printf("<<<< test: %s (%s)\n", this_test.name, ((result == 1)?"\x1B[32mOK\x1B[30m":"\x1B[31mFAILURE\x1B[30m"));
    successes += (result == 1);
  }

  printf("Tests:     %d\n", num_tests);
  printf("Successes: %d\n", successes);

  return num_tests - successes;
}
