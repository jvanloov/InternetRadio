// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <stdio.h>
#include <string.h>

#include "misc_utils.h"

extern void itoa(int value, char* str, int base);

/****** TEST FUNCTIONS *************************
 * expected return values:                     *
 * 1 == success                                *
 * 0 == failure                                *
 ***********************************************/

int test_p_u_n_maxlen_shorterthan_strlen(void) {
  int res = parse_unsigned_num("123", 2);
  return res == 12;
}

int test_p_u_n_maxlen_equalto_strlen(void) {
  int res = parse_unsigned_num("123", 3);
  return res == 123;
}

int test_p_u_n_maxlen_longerthan_str(void) {
  int res = parse_unsigned_num("123", 4);
  return res == 123;
}

int test_p_u_n_emptystring_1(void) {
  int res = parse_unsigned_num("", 0);
  return res == 0;
}

int test_p_u_n_emptystring_2(void) {
  int res = parse_unsigned_num("", 1);
  return res == 0;
}

int test_p_u_n_invalidchar(void) {
  int res = parse_unsigned_num("12a8", 4);
  return res == -1;
}

int test_build_string_init(void) {
  int test_OK = 1;
  
  char test[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
  string_build(&(test[0]), 10);

  // everything should be zeroed
  for (int i=0; i<10; i++) {
    test_OK = test_OK && (test[i] == '\0');
  }
  
  return test_OK;
}


int test_build_string_add(void) {
  int test_OK = 1;
  
  char test[10];
  string_build(&(test[0]), 10);

  string_add("a");
  test_OK = test_OK && (strcmp(test, "a") == 0);
  string_add("b");
  test_OK = test_OK && (strcmp(test, "ab") == 0);
  string_add("cde");
  test_OK = test_OK && (strcmp(test, "abcde") == 0);
  
  return test_OK;
}

int test_build_string_add_maxlen(void) {
  int test_OK = 1;
  
  char test[10];
  string_build(&(test[0]), 10);

  // add first part: within limits -> OK
  string_add("abcdefgh");
  test_OK = test_OK && (strcmp(test, "abcdefgh") == 0);
  // add additional part: only the part that fits is copied
  string_add("abcdefgh");
  test_OK = test_OK && (strcmp(test, "abcdefgha") == 0);
  // try to add more: nothing should be added 
  string_add("abcdefgh");
  test_OK = test_OK && (strcmp(test, "abcdefgha") == 0);
  
  return test_OK;
}

int test_build_string_add_substring(void) {
  int test_OK = 1;
  
  char test[10];
  string_build(&(test[0]), 10);

  string_add_substring("abcdef", 2, 4);
  test_OK = test_OK && (strcmp(test, "cd") == 0);
  string_add_substring("abcdef", 3, 5);
  test_OK = test_OK && (strcmp(test, "cdde") == 0);
  
  return test_OK;
}

static int test_res;

int display_draw_char(char c, int cur_x, int cur_y) {
  //printf("%c", c);
  test_res = test_res*10 + (c - 48);
  return cur_x + 1;
}

int display_draw_number(unsigned int num, int cur_x, int cur_y) {
  char num_str[10] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
  char c;
  int i=0;
  
  itoa(num, num_str, 10);
  while ((c = num_str[i]) != 0) {
    cur_x = display_draw_char(c, cur_x, cur_y);
    i += 1;
  }

  return cur_x;
}

int test_number_to_string(void) {
  int test_OK = 1;
  int num[3] = { 100, 1000, 45896 };
  int len = 0;

  for (int idx=0; idx<sizeof(num)/sizeof(num[0]); idx++) {
    int number = 0, digits = 1, comp = 1;

    number = num[idx];
    while ((number=number/10) != 0) {
      digits += 1;
    }  
    
    number = num[idx];
    test_res = 0;
    len = display_draw_number(number, 0, 1);
    //printf("\n");
    test_OK = test_OK && (len == digits);
    test_OK = test_OK && (test_res == number);}
    

  return test_OK;
}

/****** TEST RUNNER ** *************************/

struct test {
  char *name;
  int (*test_function)(void);
};

struct test all_tests[] = {
  { .name = "unsigned number: maxlen shorter than strlen",
    .test_function = test_p_u_n_maxlen_shorterthan_strlen },
  { .name = "unsigned number: maxlen equal to strlen",
    .test_function = test_p_u_n_maxlen_equalto_strlen },
  { .name = "unsigned number: maxlen longer than strlen",
    .test_function = test_p_u_n_maxlen_longerthan_str },
  { .name = "unsigned number: empty string/maxlen 0",
    .test_function = test_p_u_n_emptystring_1 },
  { .name = "unsigned number: empty string/maxlen 1",
    .test_function = test_p_u_n_emptystring_2 },
  { .name = "unsigned number: non-digit character",
    .test_function = test_p_u_n_invalidchar },
  { .name = "build_string: zeroed after init",
    .test_function = test_build_string_init },
  { .name = "build_string: add parts",
    .test_function = test_build_string_add },
  { .name = "build_string: add parts, exceed maxlen",
    .test_function = test_build_string_add_maxlen },
  { .name = "build_string: add parts (substring)",
    .test_function = test_build_string_add_substring },
  { .name = "number_to_string",
    .test_function = test_number_to_string }
};

int main(int argc, const char* argv[]) {
  int num_tests = sizeof(all_tests)/sizeof(all_tests[0]);
  int successes = 0;

  for (int i=0; i<num_tests; i++) {
    struct test this_test = all_tests[i];
    int result = 0;
    
    printf(">>>> test: %s\n", this_test.name);
    result = this_test.test_function();
    printf("<<<< test: %s (%s)\n", this_test.name, ((result == 1)?"\x1B[32mOK\x1B[30m":"\x1B[31mFAILURE\x1B[30m"));
    successes += (result == 1);
  }

  printf("Tests:     %d\n", num_tests);
  printf("Successes: %d\n", successes);

  return num_tests - successes;
}

