// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <stdio.h>
#include <string.h>

#include "configuration.h"
#include "streaming_callbacks.h"


/****** Mocks for functions used in the tested functions ******/

#define _i16 int16_t

int iSockID;
char buf[TCP_BUFFER_SIZE];

typedef enum {
  BUTTON_1,
  BUTTON_2,
  BUTTON_3,
  BUTTON_4,
  BUTTON_5  
} BUTTONS;

BUTTONS button_pressed = 0;

int button_check(BUTTONS button) {
  return button_pressed;
}

void display_draw_buffer(void) {}
void display_setcursor(unsigned char x, unsigned char y) {}
void display_print(char *text) {}
void term_print(const char *str) {}

// Helper method for testing the info callback:
// the string to be displayed is copied into 'buf', so
// that it can be evaluated using ASSERT
void display_update_status_bar(char *status) {
  int len = TCP_BUFFER_SIZE-1;  // leave room for final NUL char
  memset(buf, 0, TCP_BUFFER_SIZE);

  int str_len = strlen(status);
  if (str_len < len) {
    len = str_len;
  }
  memcpy(buf, status, str_len);
}


char *alphabet = "abcdefghijklmnopqrstuvwxyz";

_i16 sl_Recv(_i16 sd, void *buf, _i16 Len, _i16 flags) {
  memcpy(buf, alphabet, 26);
  return 26;
}



/****** TEST FUNCTIONS *************************
 * expected return values:                     *
 * 1 == success                                *
 * 0 == failure                                *
 ***********************************************/

#define ASSERT(x) (tests_OK = tests_OK && (x))

// Callback tests:
// - more_data_cb
//   * streaming buffer is empty -> will trigger fetch from network until BUFFER_AMOUNT is exceeded, then copy to buffer
//   BUFFER_AMOUNT << streaming buffer size
//   * streaming buffer > BUFFER_AMOUNT -> no fetch from network; only copy to buffer
//   * streaming buffer == BUFFER_AMOUNT -> no fetch from network; only copy to buffer
//   * streaming buffer < BUFFER_AMOUNT -> fetch from network; then copy to buffer
//   * streaming buffer has less than requested amount of data -> fetch from network until full enough; then copy to buffer
//   BUFFER_AMOUNT > streaming buffer size -> error on creation?
//   * more data requested from network than space available in streaming buffer -> error?
//   ...
// - streaming_info_cb
//   * run with various instances of str_info
//     invalid sample rate -> '?? kHz'
//     invalid bitrate -> ''?? kbps'
//   * implement display_update_status_bar to check the value

int test_streaming_callbacks() {
  int tests_OK = 1;
  
  memset(buf, 0, TCP_BUFFER_SIZE);
  streaming_buffer_init();

  // Fetch data from the network. 
  more_data_cb((unsigned char*)buf, 20);

  ASSERT(streaming_buffer_readahead() == 8196);

  buf[20] = 'a';
  ASSERT(memcmp(buf, alphabet, 20) == 0);
  ASSERT(memcmp(buf, alphabet, 21) < 0);
  
  return tests_OK;
}


int test_info_callback_valid_bitrates(void) {
  int tests_OK = 1;
  char temp_str[100];
  int bitrates[] = { 32,  40,  48,  56,  64,  80,  96, 112, 128, 160, 192, 224, 256, 320 };
  
  stream_info si;

  for (int i=0; i<sizeof(bitrates)/sizeof(bitrates[0]); i++) {
    si.bitrate = bitrates[i] * 1000;
    si.sample_rate = 44100;

    sprintf(temp_str, "%2.1fkHz @ %3dkbps", (((float)si.sample_rate)/1000), si.bitrate/1000);
    //printf("%s\n", temp_str);
    stream_info_cb(&si);
    ASSERT(strcmp(buf, temp_str) == 0);
  }

  return tests_OK;
}

int test_info_callback_invalid_bitrates(void) {
  int tests_OK = 1;
  char temp_str[100];
  
  stream_info si;

  si.bitrate = 16000;
  si.sample_rate = 44100;

  stream_info_cb(&si);

  ASSERT(strcmp(buf, "44.1kHz @ (?)kbps") == 0);

  si.bitrate = 496000;
  si.sample_rate = 44100;

  stream_info_cb(&si);

  ASSERT(strcmp(buf, "44.1kHz @ (?)kbps") == 0);

  return tests_OK;
}

int test_info_callback_invalid_samplerates(void) {
  int tests_OK = 1;
  char temp_str[100];
  
  stream_info si;

  si.bitrate = 96000;
  si.sample_rate = 42000;

  stream_info_cb(&si);

  //printf("%s\n", buf);
  ASSERT(strcmp(buf, "(?)kHz @  96kbps") == 0);

  si.bitrate = 96000;
  si.sample_rate = 420;

  stream_info_cb(&si);

  //printf("%s\n", buf);
  ASSERT(strcmp(buf, "(?)kHz @  96kbps") == 0);

  return tests_OK;
}

/****** TEST RUNNER ***************************/

struct test {
  char *name;
  int (*test_function)(void);
};

struct test all_tests[] = {
  { .name = "Streaming callbacks",
    .test_function = test_streaming_callbacks },
  { .name = "Info callback: valid bitrates",
    .test_function = test_info_callback_valid_bitrates },
  { .name = "Info callback: invalid bitrates",
    .test_function = test_info_callback_invalid_bitrates },
  { .name = "Info callback: invalid samplerates",
    .test_function = test_info_callback_invalid_samplerates }
};

int main(int argc, const char* argv[]) {
  int num_tests = sizeof(all_tests)/sizeof(all_tests[0]);
  int successes = 0;

  for (int i=0; i<num_tests; i++) {
    struct test this_test = all_tests[i];
    int result = 0;
    
    printf(">>>> test: %s\n", this_test.name);
    result = this_test.test_function();
    printf("<<<< test: %s (%s)\n", this_test.name, ((result == 1)?"\x1B[32mOK\x1B[30m":"\x1B[31mFAILURE\x1B[30m"));
    successes += (result == 1);
  }

  printf("Tests:     %d\n", num_tests);
  printf("Successes: %d\n", successes);

  return num_tests - successes;
}
