// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/************************************************************
 * simplelink_mock.c                                        *
 ************************************************************
 * Texas Instruments SimpleLink API abstraction.            *
 * Joris Van Looveren, 11/2017                              *
 ************************************************************
 * This module provides mock implementations for the TI     *
 * SimpleLink API functions used in the netradio.           *
 ************************************************************/


void sl_WlanProfileDel(int id) {
}

unsigned short sl_Htons(unsigned short val) {
  return val;
}

unsigned long sl_Htonl(unsigned long val) {
  return val;
}

int sl_Socket(int Domain, int Type, int Protocol) {
  return 1;
}

void sl_Close(int iSockID) {
}

int sl_Recv(int iSockID, char* tcp_data_buffer, int size, int flags) {
  return 1;
}

void _SlNonOsMainLoopTask(void) {
}
