// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/************************************************************
 * hal_mock.c                                               *
 ************************************************************
 * Hardware Abstraction Layer declarations                  *
 * Joris Van Looveren, 11/2017                              *
 ************************************************************
 * This module implements the HAL to run the netradio code  *
 * on the development machine.                              *
 * It initializes NCursus for use by the display code and   *
 * the terminal printing code, and implements keys 'j',     *
 * 'k', 'l', and 'm' for the four buttons.                  *
 * It also implements the file API for loading the stations *
 * and configuration from text files.                       *
 ************************************************************/

#include <stdio.h>
#include <ncurses.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>

#include "hal_mcu.h"
#include "configuration.h"

void hal_init() {
    // set up ncurses
  initscr();      /* initialize the curses library */
  keypad(stdscr, TRUE);  /* enable keyboard mapping */
  nonl();         /* tell curses not to do NL->CR/NL on output */
  cbreak();       /* take input chars one at a time, no wait for \n */
  noecho();
  nodelay(stdscr, TRUE);
  scrollok(stdscr, TRUE);
}

void hal_cleanup(void) {
  endwin();
}

void led_init() {
}

void uart_init() {
}

void board_init(void) {
}


// Buttons
// Set up the GPIO pins for the 5 buttons
void button_init() {
}

// this assumes ncurses to have been initialized!
int kbhit(void)
{
    int ch = getch();

    if (ch != ERR) {
        ungetch(ch);
        return 1;
    } else {
        return 0;
    }
}


// Check the state of the specified button.
// Returns -1 if the button is not known.
int button_check(BUTTONS button) {
  if (kbhit()) {
    char ch = getch();
    if (((button == BUTTON_1) &&
	 (ch == 'j')) ||
	((button == BUTTON_2) &&
	 (ch == 'k')) ||
	((button == BUTTON_3) &&
	 (ch == 'l')) ||
	((button == BUTTON_4) &&
	 (ch == 'm')) ||
	((button == BUTTON_5) &&
	 (ch == 'q'))) {
      return 1;
    } else {
      // If it's one of the valid characters, put it back;
      // otherwise, swallow it
      if ((ch == 'j') ||
	  (ch == 'k') ||
	  (ch == 'l') ||
	  (ch == 'm') ||
	  (ch == 'q')) {
	ungetch(ch);
      }
    }
  }
  return 0;
}

// Generic delay function; the delay is approximate.
void mcu_delay(int delay_us) {
  nanosleep((const struct timespec[]){{0, delay_us*1000}}, NULL);
}

static WINDOW* term_win;

static char* scrub_str(const char *str) {
  char* new_str = malloc(strlen(str)+1);
  int cur_pos = 0;
  for (int i=0; i<strlen(str); i++) {
    if (str[i] != '\r') {
      new_str[cur_pos] = str[i];
      cur_pos++;
    }
  }
  new_str[cur_pos] = '\0';
  return new_str;
}

void term_init(int baud_rate) {
  term_win = newwin(SCREENH, 60, 0, SCREENW+1);
  scrollok(term_win,TRUE);
}

void term_print(const char *str) {
  char* scrubbed_str = scrub_str(str);
  wprintw(term_win, "%s", scrubbed_str);
  free(scrubbed_str);
  wrefresh(term_win);
  refresh();
}

void term_print_int(int val) {
  wprintw(term_win, "%d", val);
  wrefresh(term_win);
  refresh();
}

void term_print_val(char *msg, int val) {
  char* scrubbed_str = scrub_str(msg);
  wprintw(term_win, "%s%d\n", scrubbed_str, val);
  free(scrubbed_str);
  wrefresh(term_win);
  refresh();
}

void term_print_hex(uint8_t val, char header) {
  wprintw(term_win, "%x", val);
  wrefresh(term_win);
  refresh();
}


void amplifier_init(void) {
  term_print("Initialize amplifier\r\n");
}

void amplifier_enable(void) {
  term_print("Enabled amplifier\r\n");
}

void amplifier_disable(void) {
  term_print("Disabled amplifier\r\n");
}

void display_set_backlight_brightness(int level) {
}


FILE_STATE file_open_for_write(const char* name, FILE_HANDLE* handle) {
  FILE* f = NULL;

  f = fopen (name, "w");
  if (f != NULL) {
    handle->handle = f;
    handle->current_offset = 0;
    return FILE_OK;
  } else {
    return FILE_ERROR;
  }
}

FILE_STATE file_open_for_read(const char* name, FILE_HANDLE* handle) {
  FILE* f = NULL;

  f = fopen (name, "r");
  if (f != NULL) {
    handle->handle = f;
    handle->current_offset = 0;
    return FILE_OK;
  } else {
    return FILE_ERROR;
  }
}

FILE_STATE file_write_block(FILE_HANDLE* handle, char* block, int block_size) {
  size_t bytes_written = fwrite(block, 1, block_size, handle->handle);
  if (bytes_written == block_size) {
    return FILE_OK;
  } else {
    return FILE_ERROR;
  }
}

FILE_STATE file_read_block(FILE_HANDLE* handle, char* block, int block_size, int* chars_read) {
  size_t bytes_read = 0;
  bytes_read = fread (block, 1, block_size, handle->handle);
  if (bytes_read > 0) {
    handle->current_offset += bytes_read;
    *chars_read = bytes_read;
    return FILE_OK;
  } else {
    return FILE_ERROR;
  }
}

FILE_STATE file_close(FILE_HANDLE* handle) {
  if (fclose(handle->handle) == 0) {
    return FILE_OK;
  } else {
    return FILE_ERROR;
  }
}

FILE_STATE file_delete(const char* name) {
  return FILE_OK;
}


