// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/************************************************************
 * network_mock.c                                           *
 ************************************************************
 * Network API mock implementation.                         *
 * Joris Van Looveren, 11/2017                              *
 ************************************************************
 * This module provides a mock implementation of some       *
 * network-related routines.                                *
 ************************************************************/

#include <string.h>

#include "wlan.h"

#include "hal_mcu.h"
#include "network.h"
#include "display.h"
#include "misc_utils.h"
#include "user_input.h"

#include "messages.h"

char g_ucConnectionSSID[SSID_LEN_MAX+1]; //Connection SSID
char g_ucConnectionBSSID[BSSID_LEN_MAX]; //Connection BSSID
Sl_WlanNetworkEntry_t netEntries[WLAN_SCAN_COUNT]; // Wlan Network Entry


_i16 sl_WlanProfileAdd(const _i8*  pName,const  _i16 NameLen,const _u8 *pMacAddr,const SlSecParams_t* pSecParams ,const SlSecParamsExt_t* pSecExtParams,const _u32 Priority,const _u32  Options) {
  return 0;
}


void network_init(void) {
}

long network_start(void) {
  return 0;
}

long network_stop(void) {
  return 0;
}

int network_get_wifi_list(void) {
  strcpy((char*)&(netEntries[0].ssid[0]), "Doos van Pandora");
  netEntries[0].ssid_len = strlen((char*)&(netEntries[0].ssid[0]));
  return 1;
}

long network_connect(char *ssid, char *key, int sec_type) {
  return SUCCESS;
}

void network_select_from_list(char *network_name, int num_networks, int max_name_len) {
  int index = 0;

  // Clear the name
  memset(&network_name[0], '\0', max_name_len);
  // Copy the network SSIDs into a string array, for display/selection
  char *network_names[num_networks];
  for (int i=0; i<num_networks; i++) {
    network_names[i] = (char*)netEntries[i].ssid;
  }
  // Select
  index = user_input_select_from_list(network_names, num_networks, max_name_len, -1);
  // Copy the SSID into the dedicated string variable
  memcpy(&network_name[0], netEntries[index].ssid, netEntries[index].ssid_len);
}

long network_configure_default_state(void) {
  memset(g_ucConnectionSSID, 0, SSID_LEN_MAX+1);
  strcpy(g_ucConnectionSSID, "Doos van Pandora");
  mcu_delay(750000);
  return NETWORK_CONNECTED_AUTO;
  //return LAN_CONNECTION_FAILED; 
}

void network_connect_or_select() {
  int result = network_configure_default_state();
  
  if (result != NETWORK_CONNECTED_AUTO) {
#ifndef NO_TERM
    term_print("No known network found.\r\n");
#endif
    
    int connected = 0;
    while (!connected) {
      char network_name[MAXIMAL_SSID_LENGTH+1];
      int num_networks = network_get_wifi_list();
      char passwd[25]; // we allow 24 characters + \0 at the end

      // Select a network
      display_clear_buffer();
      display_setcursor(1,0);
      display_print(MSG_NETWORK_CHOOSE);
#ifndef NO_TERM
      term_print("Select the WiFi network to connect to.\r\n");
#endif
      network_select_from_list(network_name, num_networks, MAXIMAL_SSID_LENGTH);

      // Assume WPA2
      // Get password
      int password_confirmed = 0;
      while (!password_confirmed) {
	memset(&passwd[0], '\0', 25);

	display_clear_buffer();
	display_setcursor(1,7);
	display_print(MSG_NETWORK_ENTER_PWD);
	display_draw_buffer();
#ifndef NO_TERM
	term_print("Enter the password for network '");
	term_print(network_name);
	term_print("'\r\n");
#endif
	user_input_get_string(&passwd[0], 25);

	display_clear_buffer_region(0,34,84,48);
	display_setcursor(1,34);
	display_print(MSG_NETWORK_CONFIRM);
	display_update_status_bar_keys(MSG_NO, "", "", MSG_YES);
	display_draw_buffer();
#ifndef NO_TERM
	term_print("'");
	term_print(passwd);
	term_print("' -- Correct? (left = no, right = yes)");
#endif
	password_confirmed = user_input_confirm();

#ifndef NO_TERM
	term_print("\r");
	for (int i=0; i<70; i++) {
	  term_print(" ");
	}
	term_print("\r");
#endif
      }
#ifndef NO_TERM
      term_print("\r\n");
#endif
      
      // After scan, reset the network interface state
      // network_configure_default_state();
      display_clear_buffer();
      display_setcursor(1,0);
      display_print(MSG_NETWORK_TRYING_TO_CONNECT);
      display_setcursor(5,11);
      display_print(network_name);
      display_draw_buffer();
#ifndef NO_TERM
      term_print("Trying to connect to '");
      term_print(network_name);
      term_print("'... ");
#endif
      network_start();
      int result = network_connect(network_name, passwd, SL_SEC_TYPE_WPA_WPA2);
      if (result == SUCCESS) {
	display_setcursor(1,21);
	display_print(MSG_NETWORK_CONNECTION_OK);
	display_setcursor(1,28);
	display_print(MSG_NETWORK_STORING_NETWORK);
	display_draw_buffer();
#ifndef NO_TERM
	term_print("connected!\r\n");
	term_print("Remembering '");
	term_print(network_name);
	term_print("'.\r\n");
#endif
	SlSecParams_t secParams;
	secParams.Key = (signed char*) passwd;
	secParams.KeyLen = strlen(passwd);
	secParams.Type = SL_SEC_TYPE_WPA_WPA2;
	int lRetVal = sl_WlanProfileAdd((signed char*)network_name,strlen(network_name),0,&secParams,0,1,0);
	connected = 1;
      } else {
	display_setcursor(1,21);
	display_print(MSG_NETWORK_CONNECTION_FAILED);
	display_draw_buffer();
	//term_print("connection failed.\r\n");
	// for the next iteration, the function to reset the NWP to its default
	// state will start the network, so we have to stop it here.
	network_stop();
      }
    }
  } else {
    // We allocate 2 bytes extra at the start of the ssid string.
    // This allows us to prepend a "signal" character and a space
    // to the SSID name for display in the status bar.
    char ssid[SSID_DISPLAY_LEN+3];
    int len = strlen(g_ucConnectionSSID);
    if (len > SSID_DISPLAY_LEN-3) {
      // use max length - 2 to signal that the original name is
      // longer than length - 3. We can then add "..." to the
      // display string
      len = SSID_DISPLAY_LEN-2;  
    }
    memset(ssid, 0, SSID_DISPLAY_LEN+3);
    memcpy(&(ssid[2]), g_ucConnectionSSID, len);
    if (len == SSID_DISPLAY_LEN-2) {
      len -= 1;
      strncpy(&(ssid[(SSID_DISPLAY_LEN+2)-3]), "...", 3);
    }

    ssid[0] = MSG_NETWORK_WIFI_NETWORK[0];
    ssid[1] = ' ';
    display_update_status_bar(ssid);
    display_draw_buffer();
#ifndef NO_TERM
    term_print("Connected to known network '");
    term_print(g_ucConnectionSSID);
    term_print("'\r\n");
#endif
  }
}


// Networking utilities
signed long network_parse_ip(char *ipstr) {
  int ip1, ip2, ip3, ip4;
  // make a local copy of the ip, since strtok modifies its argument
  char ip_cpy[25];
  memset(ip_cpy, 0, 24); // keep at least the final \0, so the string is always terminated.
  strcpy(ip_cpy, ipstr);

  char *ip1_str = strtok(ip_cpy, ".");
  ip1 = parse_unsigned_num(ip1_str, 3);
  if ((ip1 != -1) && (ip1 >= 0) && (ip1 <= 255)) {
    char *ip2_str = strtok(NULL, ".");
    ip2 = parse_unsigned_num(ip2_str, 3);
    if ((ip2 != -1) && (ip2 >= 0) && (ip2 <= 255)) {
      char *ip3_str = strtok(NULL, ".");
      ip3 = parse_unsigned_num(ip3_str, 3);
      if ((ip3 != -1)  && (ip3 >= 0) && (ip3 <= 255)) {
	char *ip4_str = strtok(NULL, ":");  // port number separator
	ip4 = parse_unsigned_num(ip4_str, 3);
	if ((ip4 != -1) && (ip4 >= 0) && (ip4 <= 255)) {
	  return (ip1 << 24) + (ip2 << 16) + (ip3 << 8) + ip4;
	}
      }
    }
  }
  return -1;
}

signed int network_parse_port(char *ipstr) {
  int colon_index = -1;
  int len = strlen(ipstr);
  
  // check if colon is present
  for (int i=0; i<len; i++) {
    if (ipstr[i] == ':') {
      colon_index = i;
      break;
    }
  }
  
  if ((colon_index >= 0) && (len > colon_index+1)) {
    char *portstr = &ipstr[colon_index+1];
    int port = parse_unsigned_num(portstr, 5);
    if ((port >= 0) && (port <= 65535)) {
      return port;
    }
  }

  return -1;
}

