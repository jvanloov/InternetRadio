// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>   // for memset

#include "misc_utils.h"

signed int network_parse_port(char *ipstr) {
  int colon_index = -1;
  int len = strlen(ipstr);
  
  // check if colon is present
  for (int i=0; i<len; i++) {
    if (ipstr[i] == ':') {
      colon_index = i;
      break;
    }
  }
  
  if ((colon_index >= 0) && (len > colon_index+1)) {
    char *portstr = &ipstr[colon_index+1];
    int port = parse_unsigned_num(portstr, 5);
    if ((port >= 0) && (port <= 65535)) {
      return port;
    }
  }

  return -1;
}


/****** TEST FUNCTIONS *************************
 * expected return values:                     *
 * 1 == success                                *
 * 0 == failure                                *
 ***********************************************/

#define ASSERT(x) (tests_OK = tests_OK && (x))


int test_network_parse_port(void) {
  int tests_OK = 1;
  signed int port = 0;
    
  port = network_parse_port("64.202.98.33:3120");
  
  ASSERT(port == 3120);
  
  return tests_OK;
}



/****** TEST RUNNER ***************************/

struct test {
  char *name;
  int (*test_function)(void);
};

struct test all_tests[] = {
  { .name = "test network_parse_port)",
    .test_function = test_network_parse_port },
};

int main(int argc, const char* argv[]) {
  int num_tests = sizeof(all_tests)/sizeof(all_tests[0]);
  int successes = 0;

  for (int i=0; i<num_tests; i++) {
    struct test this_test = all_tests[i];
    int result = 0;
    
    printf(">>>> test: %s\n", this_test.name);
    result = this_test.test_function();
    printf("<<<< test: %s (%s)\n", this_test.name, ((result == 1)?"\x1B[32mOK\x1B[30m":"\x1B[31mFAILURE\x1B[30m"));
    successes += (result == 1);
  }

  printf("Tests:     %d\n", num_tests);
  printf("Successes: %d\n", successes);

  return num_tests - successes;
}
