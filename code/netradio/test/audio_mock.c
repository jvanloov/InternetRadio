// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/************************************************************
 * audio_mock.c                                             *
 ************************************************************
 * Audio abstraction.                                       *
 * Joris Van Looveren, 11/2017                              *
 ************************************************************
 * This module provides mock implementations for the audio  *
 * API. The only functionality it provides is keeping a     *
 * playing/stopped state.                                   *
 ************************************************************/

#include <inttypes.h>

#include "audio.h"


static int aud_state = STATE_STOPPED;

void audio_init(void) {
}

void audio_setup(uint32_t sample_rate) {
}

int audio_state(void) {
  return aud_state;
}

void audio_start(void) {
  aud_state = STATE_PLAYING;
}

void audio_stop(void) {
  aud_state = STATE_STOPPED;
}

void audio_close(void) {
}
