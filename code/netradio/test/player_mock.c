// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/************************************************************
 * player_mock.c                                            *
 ************************************************************
 * Audio player abstraction.                                *
 * Joris Van Looveren, 11/2017                              *
 ************************************************************
 * This module provides an abstraction for a music player.  *
 * It's a mock implementation, for testing. It pretends     *
 * to run the player, by calling the streaminfo callback    *
 * after 500ms, and then waiting for the "stop" button to   *
 * be pressed.                                              *
 ************************************************************/

#include "hal_mcu.h"
#include "player.h"

#include "network.h"
#include "audio.h"
#include "display.h"
#include "misc_utils.h"
#include "stations_recent3.h"
#include "messages.h"

char *spinner[] = { "{", "|", "}", "~" };


void player_init(void) {
}


int play_from_inet_stream(int iSockID, SlSockAddrIn_t *pAddr, char *request, get_more_data_callback more_data_cb, stream_info_callback stream_info_cb) {
  stream_info str_info;
  
  mcu_delay(500000);
  
  str_info.channels = 2;
  str_info.sample_rate = 44100;
  str_info.bitrate = 128000;
  stream_info_cb(&str_info);

  while (button_check(BUTTON_1) == 0) {
    mcu_delay(500000);
  }
  return PLAYER_STOPPED;
}


void player_close(void) {
}


int iSockID;

void player_play_station(STATION* data) {
  term_print("Play '");
  term_print(data->short_name);
  term_print("' (");
  term_print(data->ip);
  term_print(")\n");

  char str_buf[50];

  char *streamShortName = data->short_name;
  char *streamIP = data->ip;

  stations_recent3_add(SELECTED_FILE, streamShortName);
  
#ifndef NO_TERM
  term_print("Saving current selection to file: ");
  term_print(streamShortName);
  term_print("\r\n");
#endif

  string_build(&(str_buf[0]), 50);
  string_add("GET ");
  string_add(data->url);
  string_add(" HTTP/1.1\r\n\r\n");
	
  char *streamREQ  = &(str_buf[0]);

#ifndef NO_TERM
  term_print("Selection: ");
  term_print(streamShortName);
  term_print("\r\n");
  term_print("IP:        ");
  term_print(streamIP);
  term_print("\r\n");
  term_print("Request:   ");
  term_print(streamREQ);
  term_print("\r\n");
#endif
      
  int name_width = display_calculate_string_length(streamShortName);
  int name_x1 = (SCREENW / 2) - (name_width / 2) - 3;
  int name_vpos = (SCREENH / 3) - 5;
      
  display_clear_buffer();
  display_setcursor(name_x1+3,14);
  display_print(streamShortName);
  display_invert_buffer_region(2, 10, SCREENW-2, 24);
  display_draw_buffer();
      
  // Clean up MP3 decoder & audio infrastructure
  // All Helix buffers need to be zeroed properly to put the
  // decode back to the initial state
  // 44.1kHz is a default setting; this will be changed when
  // connected to a stream, and the sample rate is known.

#ifndef NO_TERM
  term_print_val("Memory: ", memory_get_available());
#endif
	
  // Set up streaming buffer and MP3 decoder (allocate buffers)
  streaming_buffer_init();
  player_init();	
	
#ifndef NO_TERM
  term_print_val("Memory: ", memory_get_available());
  term_print("audio_init\r\n");
#endif
  audio_init();
#ifndef NO_TERM
  term_print("audio_setup\r\n");
#endif
  audio_setup(44100);
  
#ifndef NO_TERM
  term_print("Attempting to open stream '");
  term_print(streamShortName);
  term_print("'\r\n");
#endif  
  int port = 0;
  unsigned int ip = network_parse_ip(streamIP);
  port = network_parse_port(streamIP);
  if (port == -1) {
    port = 80;
  }
      
  SlSockAddrIn_t sAddr;
  
  //filling the TCP server socket address
  sAddr.sin_family = SL_AF_INET;
  sAddr.sin_port = sl_Htons((unsigned short)port);
  sAddr.sin_addr.s_addr = sl_Htonl(ip);
      
  // creating a TCP socket
  iSockID = sl_Socket(SL_AF_INET,SL_SOCK_STREAM, 0);
  if (iSockID >= 0) {
    int result = -1;
    int failure_counter = -1;
	
    // Connect to the stream. As long as we receive decoding errors,
    // keep trying; they may be recoverable by reconnecting to the stream.
    // Other errors are not recoverable.
    //term_print("\r\nConnecting to stream..");
    do {
      failure_counter++;
      
      int vpos = 2 * (SCREENH / 3);
      int hpos = (SCREENW / 2) - 5;
      
      display_setcursor(hpos, vpos);
      display_print(spinner[failure_counter % 4]);
      display_draw_buffer();

      result = play_from_inet_stream(iSockID, &sAddr, streamREQ, more_data_cb, stream_info_cb);
    } while ((result == PLAYER_STATUS_DECODE_ERROR) && (failure_counter < 50));
  }
  _SlNonOsMainLoopTask();
  mcu_delay(20000);
  sl_Close(iSockID);

  display_update_status_bar(MSG_PLAYER_CLOSED_CONNECTION);
  display_draw_buffer();
#ifndef NO_TERM
  term_print("Closed connection\r\n");
#endif

  audio_stop();
  audio_close();
  player_close();
  streaming_buffer_destroy();
#ifndef NO_TERM
  term_print("Player closed\r\n");
  term_print_val("Memory: ", memory_get_available());
#endif
  
}
