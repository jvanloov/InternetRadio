// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/************************************************************
 * display_mock.c                                           *
 ************************************************************
 * Display API mock implementation.                         *
 * Joris Van Looveren, 11/2017                              *
 ************************************************************
 * This module provides a mock display implementation.      *
 * The display API draws on a NCurses window of 84x48,      *
 * which is the same size as the "real" Nokia LCD.          *
 * The display uses the "letters.h" font definition, like   *
 * the real implementation.                                 *
 ************************************************************/

#include <stdio.h>
#include <string.h>

#include "configuration.h"  // for screen width and height
#include "display.h"
#include "letters.h"

#include "ncurses.h"

static int pos_x = 0;
static int pos_y = 0;

static WINDOW* screen_win;

// no hardware to set up in the mock
void display_init(void) {
}

// Initialize the display. 
void display_initialize(void) {
  screen_win = newwin(SCREENH,SCREENW, 0,0);
}


// Clear the display. 
void display_clear(void) {
  for (int x=0; x<SCREENW; x++) {
    for (int y=0; y<SCREENH; y++) {
      mvwaddch(screen_win, y, x, ' ');
    }
  }
  wrefresh(screen_win);
  refresh();
}


static void display_set_buffer_pixel(unsigned char x, unsigned char y, unsigned char on) {
  if (on) {
    wattron(screen_win, A_REVERSE);
    mvwaddch(screen_win, y, x, ' ');
    wattroff(screen_win, A_REVERSE);
  } else {
    mvwaddch(screen_win, y, x, ' ');
  }
}

static int display_get_buffer_pixel(unsigned char x, unsigned char y) {
  chtype ch = mvwinch(screen_win, y, x);
  return (ch & A_ATTRIBUTES) == A_REVERSE;
}

static int display_draw_char(unsigned char c, int cur_x, int cur_y) {
  int charlen = 0;
  if (ascii_set[c] != -1) {
    short index = ascii_set[c];
    unsigned char *letter = characters[index];
    int varspace = letter[0] & 0x01;
    for (int cnt=1; cnt<6; cnt++) {
      charlen = (charlen << 1) + (letter[cnt] & 0x01);
    }
    for (int x=0; x<charlen; x++) {
      int shiftval = 0x80 >> x;
      for (int y=0; y<6; y++) {
	if ((letter[y] & shiftval) > 0) {
	  display_set_buffer_pixel(cur_x+x, cur_y+y, 1);
	} else {
	  display_set_buffer_pixel(cur_x+x, cur_y+y, 0);
	}
      }
    }
  }
  return cur_x + charlen + 1;
}

static int display_char_width(unsigned char c) {
  int charlen = 0;
  if (ascii_set[c] != 0) {
    short index = ascii_set[c];
    unsigned char *letter = characters[index];
    int varspace = letter[0] & 0x01;
    for (int cnt=1; cnt<6; cnt++) {
      charlen = (charlen << 1) + (letter[cnt] & 0x01);
    }
  }
  return charlen;
}

// Calculate the length in pixels that the string in str
// would take up on the screen. (This depends on the font
// used on the display.)
int display_calculate_string_length(char *str) {
  char c;
  int i=0, x=0;
  while ((c = str[i]) != 0) {
    x += display_char_width(c) + 1;
    i++;
  }
  return x;  
} 

// Calculate the length in pixels that the string in str
// would take up on the screen. (This depends on the font
// used on the display.)
int display_calculate_substring_length(char *str, int start_pos, int end_pos) {
  if (start_pos < 0) {
    start_pos = 0;
  }
  if (end_pos > strlen(str)) {
    end_pos = strlen(str);
  }
  char c;
  int x=0;
  for (int i=start_pos; i<end_pos; i++) {
    c = str[i];
    x += display_char_width(c) + 1;
  }
  return x;  
} 


// Write a null-terminated character string into the buffer, starting
// at position (cur_x, cur_y)
int display_draw_string(char *str, int cur_x, int cur_y) {
  int x = cur_x;
  char c;
  int i=0;
  while ((c = str[i]) != 0) {
    x = display_draw_char(c, x, cur_y);
    i++;
  }
  return x;
}


void display_print(char *str) {
  int x = pos_x;
  int c;
  int i=0;
  while ((c = str[i]) > 0) {
    if (c != '\n') {
      x = display_draw_char(c, x, pos_y);
      if (x > SCREENW) {
	x = 0;
	pos_y += 7;
      }
    } else {
      x = 0;
      pos_y += 7;
    }
    i++;
  }
  pos_x = x;
}

// Clear the display buffer
void display_clear_buffer(void) {
  for (int x=0; x<SCREENW; x++) {
    for (int y=0; y<SCREENH; y++) {
      mvwaddch(screen_win, y, x, ' ');
    }
  }
  pos_x = 0;
  pos_y = 0;
}

/*void display_draw_bitmap(unsigned char left_x, unsigned char top_y,
			 unsigned char **bitmap,
			 int size_x, int size_y) {
  for (int x=0; x<size_x; x++) {
    int shiftval = 0x80 >> (x % 8);
    int column_idx = x / 8;
    for (int y=0; y<size_y; y++) {
      unsigned char *row = bitmap[y];
      unsigned char byte = row[column_idx];
      if ((byte & shiftval) > 0) {
	display_set_buffer_pixel(left_x+x, top_y+y, 1);
      } else {
	display_set_buffer_pixel(left_x+x, top_y+y, 0);
      }
    }
  }
}*/

void display_draw_bitmap(unsigned char left_x, unsigned char top_y,
			 unsigned char bitmap[],
			 int size_x, int size_y) {
  for (int x=0; x<size_x; x++) {
    int shiftval = 0x80 >> (x % 8);
    int column_idx = x / 8;
    for (int y=0; y<size_y; y++) {
      if ((bitmap[y*(size_x/8)+column_idx] & shiftval) > 0) {
	display_set_buffer_pixel(left_x+x, top_y+y, 1);
      } else {
	display_set_buffer_pixel(left_x+x, top_y+y, 0);
      }
    }
  }
}


// Draw a rectangle in the screen buffer
void display_rectangle(unsigned char x1, unsigned char y1,
		       unsigned char x2, unsigned char y2) {
  for (int x=x1; x<x2; x++) {
    display_set_buffer_pixel(x, y1, 1);
    display_set_buffer_pixel(x, y2, 1);
  }
  for (int y=y1; y<y2; y++) {
    display_set_buffer_pixel(x1, y, 1);
    display_set_buffer_pixel(x2, y, 1);
  }
  display_set_buffer_pixel(x2, y2, 1);
}


void display_draw_buffer(void) {
  wrefresh(screen_win);
  refresh();
}


// Set the cursor to the given (X,Y) position
// X = 0 .. 84
// Y = 0 .. 46
// Since we're only doing text here, approximate the character position
// using avg. char width = 5; char height = 6
void display_setcursor(unsigned char x, unsigned char y) {
  pos_x = x;
  pos_y = y;
  wmove(screen_win, y, x);
}

int display_getcursor_x(void) {
  return pos_x;
}
int display_getcursor_y(void) {
  return pos_y;
}

// Put the string in status into the status bar of the screen (= the lowermost row)
void display_update_status_bar(char *status) {
  int width = display_calculate_string_length(status);
  if (width > SCREENW) { width = SCREENW; }
  
  display_clear_buffer_region(0,SCREENH-6,SCREENW,SCREENH);
  display_setcursor((SCREENW / 2) - (width / 2), SCREENH-6);
  display_print(status);
}


// Invert the rectangular region (x1,y1)-(x2,y2) in the buffer:
// all pixel values are flipped.
void display_invert_buffer_region(unsigned char x1, unsigned char y1,
				  unsigned char x2, unsigned char y2) {
  for (int x=x1; x<x2; x++) {
    for (int y=y1; y<y2; y++) {
      display_set_buffer_pixel(x, y, 1 - display_get_buffer_pixel(x, y));
    }
  }
}


// Clear the rectangular region (x1,y1)-(x2,y2) in the buffer:
// all pixel values are zeroed.
void display_clear_buffer_region(unsigned char x1, unsigned char y1,
			         unsigned char x2, unsigned char y2) {
  for (int x=x1; x<x2; x++) {
    for (int y=y1; y<y2; y++) {
      display_set_buffer_pixel(x, y, 0);
    }
  }
}


// Set the controller's cursor position
void display_set_hw_cursor(unsigned char x, unsigned char y) {
  move(y, x);
}

void display_update_status_bar_keys(char *key1, char *key2, char *key3, char *key4) {
  int len1 = display_calculate_string_length(key1);
  int len2 = display_calculate_string_length(key2);
  int len3 = display_calculate_string_length(key3);
  int len4 = display_calculate_string_length(key4);
#if DISPLAY_TYPE == nokia
  int center_pos_1 = 10;
  int center_pos_2 = 30;
  int center_pos_3 = 50;
  int center_pos_4 = 70;
#else
  int center_pos_1 = 8;
  int center_pos_2 = 45;
  int center_pos_3 = 83;
  int center_pos_4 = 120;
#endif
  int pos_1 = center_pos_1 - len1/2;
  if (pos_1 < 0) { pos_1 = 0; }
  int pos_4 = center_pos_4 - len4/2;
  if (pos_4+len4 > SCREENW) { pos_4 = SCREENW - len4; }
  
  int button_width = SCREENW / 4 - 5;
  display_clear_buffer_region(0,SCREENH-6,SCREENW,SCREENH);
  display_setcursor(pos_1, SCREENH-6);
  display_print(key1);
  display_setcursor(center_pos_2 - len2/2, SCREENH-6);
  display_print(key2);
  display_setcursor(center_pos_3 - len3/2, SCREENH-6);
  display_print(key3);
  display_setcursor(pos_4, SCREENH-6);
  display_print(key4);
}
