// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <stdio.h>

#include "circular_data_buffer.h"


/****** TEST FUNCTIONS *************************
 * expected return values:                     *
 * 1 == success                                *
 * 0 == failure                                *
 ***********************************************/

#define ASSERT(x) (tests_OK = tests_OK && (x))

#define TEST_BUF_SIZE 10
static circular_data_buffer testbuf;
static char testbuf_storage[TEST_BUF_SIZE] = { 'A', 'B', 'C', 'D', 'E',
					       'F', 'G', 'H', 'I', 'J' };

/****************************
 * Set up an new, empty buffer
 * Test whether all member variables of the
 * buffer struct are properly initialized.
 ****************************/
int test_circbuf_init(void) {
  int tests_OK = 1;

  ASSERT(testbuf_storage[3] == 'D');  
  // initialize buffer
  circular_data_buffer_init(&testbuf, &testbuf_storage[0], TEST_BUF_SIZE);
  // buffer should point to the storage space
  ASSERT(testbuf.data_buffer == &testbuf_storage[0]);
  // storage space should have been zeroed
  ASSERT(testbuf.data_buffer[3] == '\0');
  ASSERT(testbuf_storage[3] == '\0');
  // buffer pointers must be zero
  ASSERT(testbuf.read_pos == testbuf.write_pos == testbuf.wrapped == 0);
  // buffer is empty
  ASSERT(testbuf.empty == 1);
  // buffer size must be set correctly
  ASSERT(testbuf.data_buffer_size == TEST_BUF_SIZE);

  return tests_OK;
}


/****************************
 * empty buffer should have all space left, and
 * no space used.
 ****************************/
int test_circbuf_char_left_count_empty(void) {
  int tests_OK = 1;

  // reset the buffer to initial state
  test_circbuf_init();

  ASSERT(circular_data_buffer_chars_left(&testbuf) == 10);
  ASSERT(circular_data_buffer_usage(&testbuf) == 0);

  return tests_OK;
}

/****************************
 * Are the counts updated properly when progressively
 * more data is stored in the buffer?
 ****************************/
int test_circbuf_char_left_count_after_store(void) {
  int tests_OK = 1;

  // reset the buffer to initial state
  test_circbuf_init();

  // repeatedly add chars to the buffer and check counts
  circular_data_buffer_store(&testbuf, 'a');  
  ASSERT(circular_data_buffer_chars_left(&testbuf) == 9);
  ASSERT(circular_data_buffer_usage(&testbuf) == 1);
  circular_data_buffer_store(&testbuf, 'a');  
  ASSERT(circular_data_buffer_chars_left(&testbuf) == 8);
  ASSERT(circular_data_buffer_usage(&testbuf) == 2);
  circular_data_buffer_store(&testbuf, 'a');  
  circular_data_buffer_store(&testbuf, 'a');
  ASSERT(circular_data_buffer_chars_left(&testbuf) == 6);
  ASSERT(circular_data_buffer_usage(&testbuf) == 4);

  return tests_OK;
}

/****************************
 * Are the counts updated properly when progressively
 * more data is stored in the buffer and read from the
 * buffer?
 ****************************/
int test_circbuf_char_left_count_after_stores_and_reads(void) {
  int tests_OK = 1;

  // reset the buffer to initial state
  test_circbuf_init();

  // repeatedly add chars to the buffer and read chars from the
  // buffer and check counts 
  circular_data_buffer_store(&testbuf, 'a');  
  circular_data_buffer_store(&testbuf, 'a');  
  circular_data_buffer_store(&testbuf, 'a');  
  circular_data_buffer_store(&testbuf, 'a');
  ASSERT(circular_data_buffer_chars_left(&testbuf) == 6);
  ASSERT(circular_data_buffer_usage(&testbuf) == 4);
  circular_data_buffer_read(&testbuf);
  ASSERT(circular_data_buffer_chars_left(&testbuf) == 7);
  ASSERT(circular_data_buffer_usage(&testbuf) == 3);
  circular_data_buffer_read(&testbuf);
  circular_data_buffer_read(&testbuf);
  ASSERT(circular_data_buffer_chars_left(&testbuf) == 9);
  ASSERT(circular_data_buffer_usage(&testbuf) == 1);

  return tests_OK;
}


/****************************
 * Are the counts updated properly when progressively
 * more data is stored in the buffer and read from the
 * buffer, using block-store and block-read functions?
 ****************************/
int test_circbuf_char_left_count_after_stores_and_reads_blocks(void) {
  int tests_OK = 1;
  char testblock[] = { 'a', 'b' };

  // reset the buffer to initial state
  test_circbuf_init();

  // repeatedly add chars to the buffer and read chars from the
  // buffer and check counts 
  circular_data_buffer_store_block(&testbuf, &testblock[0], 2);  
  circular_data_buffer_store_block(&testbuf, &testblock[0], 2);  
  circular_data_buffer_store_block(&testbuf, &testblock[0], 2);  
  circular_data_buffer_store_block(&testbuf, &testblock[0], 2);
  ASSERT(circular_data_buffer_chars_left(&testbuf) == 2);
  ASSERT(circular_data_buffer_usage(&testbuf) == 8);
  circular_data_buffer_read_block(&testbuf, &testblock[0], 1);
  ASSERT(circular_data_buffer_chars_left(&testbuf) == 3);
  ASSERT(circular_data_buffer_usage(&testbuf) == 7);
  circular_data_buffer_read_block(&testbuf, &testblock[0], 3);
  circular_data_buffer_read_block(&testbuf, &testblock[0], 2);
  ASSERT(circular_data_buffer_chars_left(&testbuf) == 8);
  ASSERT(circular_data_buffer_usage(&testbuf) == 2);

  return tests_OK;
}


/****************************
 * Do we get a proper error code when we try to read
 * more data from the buffer than what's in there?
 ****************************/
int test_circbuf_read_past_end(void) {
  int tests_OK = 1;

  // reset the buffer to initial state
  test_circbuf_init();

  // Put stuff in, read the same amount of stuff
  circular_data_buffer_store(&testbuf, 'a');  
  circular_data_buffer_store(&testbuf, 'a');  
  circular_data_buffer_store(&testbuf, 'a');  
  circular_data_buffer_store(&testbuf, 'a');
  ASSERT(circular_data_buffer_chars_left(&testbuf) == 6);
  ASSERT(circular_data_buffer_usage(&testbuf) == 4);

  circular_data_buffer_read(&testbuf);  
  circular_data_buffer_read(&testbuf);  
  circular_data_buffer_read(&testbuf);  
  circular_data_buffer_read(&testbuf);
  ASSERT(circular_data_buffer_chars_left(&testbuf) == 10);
  ASSERT(circular_data_buffer_usage(&testbuf) == 0);

  // the offending read
  int res = circular_data_buffer_read(&testbuf);
  ASSERT(res == -1);
  
  return tests_OK;
}


/****************************
 * Do we get a proper error code when we try to read
 * more data from the buffer than what's in there?
 ****************************/
int test_circbuf_read_past_end_with_wrap(void) {
  int tests_OK = 1;

  // reset the buffer to initial state
  test_circbuf_init();

  // Put stuff in, read the same amount of stuff
  circular_data_buffer_store(&testbuf, 'a');  
  circular_data_buffer_store(&testbuf, 'a');  
  circular_data_buffer_store(&testbuf, 'a');  
  circular_data_buffer_store(&testbuf, 'a');
  circular_data_buffer_store(&testbuf, 'a');  
  circular_data_buffer_store(&testbuf, 'a');  
  circular_data_buffer_store(&testbuf, 'a');  
  circular_data_buffer_store(&testbuf, 'a');  
  ASSERT(circular_data_buffer_chars_left(&testbuf) == 2);
  ASSERT(circular_data_buffer_usage(&testbuf) == 8);

  circular_data_buffer_read(&testbuf);  
  circular_data_buffer_read(&testbuf);  
  circular_data_buffer_read(&testbuf);  
  circular_data_buffer_read(&testbuf);
  ASSERT(circular_data_buffer_chars_left(&testbuf) == 6);
  ASSERT(circular_data_buffer_usage(&testbuf) == 4);

  circular_data_buffer_store(&testbuf, 'a');
  circular_data_buffer_store(&testbuf, 'a');  
  circular_data_buffer_store(&testbuf, 'a');  
  circular_data_buffer_store(&testbuf, 'a');  
  circular_data_buffer_store(&testbuf, 'a');

  ASSERT(circular_data_buffer_chars_left(&testbuf) == 1);
  ASSERT(circular_data_buffer_usage(&testbuf) == 9);

  // the offending read
  int res = circular_data_buffer_read(&testbuf);
  ASSERT(res != -1);
  
  return tests_OK;
}


/****************************
 * Do we get a proper error code when we try to read
 * more data from the buffer than what's in there?
 ****************************/
int test_circbuf_read_past_end_block(void) {
  int tests_OK = 1;
  char testblock[2];
  
  // reset the buffer to initial state
  test_circbuf_init();

  // Put stuff in, read the same amount of stuff
  circular_data_buffer_store(&testbuf, 'a');  
  circular_data_buffer_store(&testbuf, 'a');  
  circular_data_buffer_store(&testbuf, 'a');  
  circular_data_buffer_store(&testbuf, 'a');
  ASSERT(circular_data_buffer_chars_left(&testbuf) == 6);
  ASSERT(circular_data_buffer_usage(&testbuf) == 4);

  circular_data_buffer_read_block(&testbuf, &testblock[0], 2);  
  circular_data_buffer_read_block(&testbuf, &testblock[0], 2);  
  ASSERT(circular_data_buffer_chars_left(&testbuf) == 10);
  ASSERT(circular_data_buffer_usage(&testbuf) == 0);

  // the offending read
  int res = circular_data_buffer_read_block(&testbuf, &testblock[0], 2);
  ASSERT(res == -1);
  
  return tests_OK;
}


/****************************
 * Do we get a proper error code when we try to read
 * more data from the buffer than what's in there?
 ****************************/
int test_circbuf_read_past_end_block_with_wrap(void) {
  int tests_OK = 1;
  char testblock[2];
  
  // reset the buffer to initial state
  test_circbuf_init();

  // Put stuff in, read the same amount of stuff
  circular_data_buffer_store(&testbuf, 'a');
  circular_data_buffer_store(&testbuf, 'a');
  circular_data_buffer_store(&testbuf, 'a');
  circular_data_buffer_store(&testbuf, 'a');
  circular_data_buffer_store(&testbuf, 'a');
  circular_data_buffer_store(&testbuf, 'a');
  circular_data_buffer_store(&testbuf, 'a');
  circular_data_buffer_store(&testbuf, 'a');
  ASSERT(circular_data_buffer_chars_left(&testbuf) == 2);
  ASSERT(circular_data_buffer_usage(&testbuf) == 8);

  circular_data_buffer_read_block(&testbuf, &testblock[0], 2);  
  circular_data_buffer_read_block(&testbuf, &testblock[0], 2);  
  ASSERT(circular_data_buffer_chars_left(&testbuf) == 10);
  ASSERT(circular_data_buffer_usage(&testbuf) == 0);

  // the offending read
  int res = circular_data_buffer_read_block(&testbuf, &testblock[0], 2);
  ASSERT(res == -1);
  
  return tests_OK;
}


/****************************
 * Do we get a proper error code when we try to store 
 * more than the buffer's capacity?
 ****************************/
int test_circbuf_store_past_end_singlechar(void) {
  int tests_OK = 1;

  // reset the buffer to initial state
  test_circbuf_init();

  // fill the buffer
  for (int i=0; i<TEST_BUF_SIZE-1; i++) {
    int res = circular_data_buffer_store(&testbuf, 'a');
    ASSERT(res == 0);
  }

  // store one character more than the buffer capacity
  int res = circular_data_buffer_store(&testbuf, 'a');
  ASSERT(res == -1);
  
  return tests_OK;
}


/****************************
 * We should be able to fill the buffer to capacity.
 ****************************/
int test_circbuf_store_until_end_singlechar(void) {
  int tests_OK = 1;

  // reset the buffer to initial state
  test_circbuf_init();

  for (int i=0; i<TEST_BUF_SIZE-2; i++) {
    int res = circular_data_buffer_store(&testbuf, 'a');
    ASSERT(res == 0);
  }

  // buffer capacity = buffer size - 1
  // (read and write pointers must not become equal)
  int res = circular_data_buffer_store(&testbuf, 'a');
  ASSERT(res == 0);
  
  return tests_OK;
}

/****************************
 * Do we get a proper error code when we try to store 
 * more than the buffer's capacity using the block-store
 * function?
 ****************************/
int test_circbuf_store_past_end_block(void) {
  int tests_OK = 1;
  char test[] = { 'a', 'b', 'c', 'd' } ;
  
  // reset the buffer to initial state
  test_circbuf_init();

  for (int i=0; i<TEST_BUF_SIZE-2; i++) {
    int res = circular_data_buffer_store(&testbuf, 'a');
    ASSERT(res == 0);
  }

  // store one character more than the buffer capacity
  int res = circular_data_buffer_store_block(&testbuf, &test[0], 4);
  ASSERT(res == -1);
  
  return tests_OK;
}

/****************************
 * We should be able to fill the buffer to capacity using
 * the block-store method.
 ****************************/
int test_circbuf_store_until_end_block(void) {
  int tests_OK = 1;
  char test[] = { 'a', 'b', 'c', 'd' } ;
  
  // reset the buffer to initial state
  test_circbuf_init();

  //printf("Filling buffer\n");
  for (int i=0; i<TEST_BUF_SIZE-4; i++) {
    int res = circular_data_buffer_store(&testbuf, 'a');
    ASSERT(res == 0);
  }

  // buffer capacity = buffer size - 1
  // (read and write pointers must not become equal)
  int res = circular_data_buffer_store_block(&testbuf, &test[0], 3);
  ASSERT(res == 0);
  
  return tests_OK;
}


/****************************
 * Test the function to find the (relative) position at which
 * a char is found in the buffer.
 ****************************/
int test_circbuf_contains_char(void) {
  int res;
  int tests_OK = 1;
  char test[] = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j' } ;
  
  // reset the buffer to initial state
  test_circbuf_init();

  //printf("Filling buffer\n");
  for (int i=0; i<TEST_BUF_SIZE-1; i++) {
    res = circular_data_buffer_store(&testbuf, test[i]);
    ASSERT(res == 0);
  }

  res = circular_data_buffer_contains_char(&testbuf, 'a');
  ASSERT(res == 0);
  res = circular_data_buffer_contains_char(&testbuf, 'd');
  ASSERT(res == 3);
  res = circular_data_buffer_contains_char(&testbuf, 'i');
  ASSERT(res == 8);

  circular_data_buffer_read(&testbuf);
  circular_data_buffer_read(&testbuf);
  circular_data_buffer_read(&testbuf);

  res = circular_data_buffer_contains_char(&testbuf, 'd');
  ASSERT(res == 0);
  res = circular_data_buffer_contains_char(&testbuf, 'i');
  ASSERT(res == 5);
  res = circular_data_buffer_contains_char(&testbuf, 'z');
  ASSERT(res == -1);  // not found
  
  return tests_OK;
}

// more tests:
// - store + wrap
// - read + wrap
// - store block in 1 piece
// - store block in wrapped (2 pieces)


/****** TEST RUNNER ***************************/

struct test {
  char *name;
  int (*test_function)(void);
};

struct test all_tests[] = {
  { .name = "circular buffer: initialization",
    .test_function = test_circbuf_init },
  { .name = "circular buffer: chars left in empty buffer",
    .test_function = test_circbuf_char_left_count_empty },
  { .name = "circular buffer: chars left in buffer after store",
    .test_function = test_circbuf_char_left_count_after_store },
  { .name = "circular buffer: chars left in buffer after stores and reads (single chars)",
    .test_function = test_circbuf_char_left_count_after_stores_and_reads },
  { .name = "circular buffer: chars left in buffer after stores and reads (blocks)",
    .test_function = test_circbuf_char_left_count_after_stores_and_reads_blocks },
  { .name = "circular buffer: store until buffer end (single char)",
    .test_function = test_circbuf_store_until_end_singlechar },
  { .name = "circular buffer: store until buffer end (block)",
    .test_function = test_circbuf_store_until_end_block },
  { .name = "circular buffer: read past buffer end (single char)",
    .test_function = test_circbuf_read_past_end },
  { .name = "circular buffer: read past buffer end with wrap (single char)",
    .test_function = test_circbuf_read_past_end_with_wrap },
  { .name = "circular buffer: read past buffer end (block)",
    .test_function = test_circbuf_read_past_end_block },
  { .name = "circular buffer: store past buffer end (single char)",
    .test_function = test_circbuf_store_past_end_singlechar },
  { .name = "circular buffer: store past buffer end (block)",
    .test_function = test_circbuf_store_past_end_block },
  { .name = "circular buffer: peek for char",
    .test_function = test_circbuf_contains_char }
};

int main(int argc, const char* argv[]) {
  int num_tests = sizeof(all_tests)/sizeof(all_tests[0]);
  int successes = 0;

  for (int i=0; i<num_tests; i++) {
    struct test this_test = all_tests[i];
    int result = 0;
    
    printf(">>>> test: %s\n", this_test.name);
    result = this_test.test_function();
    printf("<<<< test: %s (%s)\n", this_test.name, ((result == 1)?"\x1B[32mOK\x1B[30m":"\x1B[31mFAILURE\x1B[30m"));
    successes += (result == 1);
  }

  printf("Tests:     %d\n", num_tests);
  printf("Successes: %d\n", successes);

  return num_tests - successes;
}
