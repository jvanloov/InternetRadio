// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/************************************************************
 * configuration.h                                          *
 ************************************************************
 * Configuration constants.                                 *
 * Joris Van Looveren, 4/2017                               *
 ************************************************************
 *                                                          *
 ************************************************************/

// Name of the favorites file
#ifndef __TEST__
#define GLOBAL_STATIONS_FILE "favorites.txt"
#else
#define GLOBAL_STATIONS_FILE "../../data/favorites.txt"
#endif

// Name of the selected file
#ifndef __TEST__
#define SELECTED_FILE "selected.txt"
#else
#define SELECTED_FILE "../../data/selected.txt"
#endif

// TCP buffer size (bytes)
// This is the size of the buffer used for fetching streaming data from the network.
// The MP3 decoder requests data whenever it needs more. If we don't have enough data
// stored locally, we fetch a new buffer-full of data from the network. This size
// is also the size we request from the network socket.
// (See: streaming_callbacks.c)
//#define TCP_BUFFER_SIZE  4096
#define TCP_BUFFER_SIZE  16384

// Network data buffer size (bytes)
// This buffer stores the data read from the network (using the TCP buffer).
// (See: streaming_callbacks.c)
//#define NETWORK_BUFFER_SIZE 16384
//#define NETWORK_BUFFER_SIZE 32768
#define NETWORK_BUFFER_SIZE 40000

// Smallest amount of data required in the network data buffer (bytes)
// If the unread data in the network data buffer drops below this number, we
// trigger network requests for more data to replenish the network data buffer.
// (See: streaming_callbacks.c)
//#define BUFFER_AMOUNT 8192
#define BUFFER_AMOUNT 6144

// Data buffer size (bytes)
// This buffer is used to supply data to the MP3 decoder. It contains the next frame
// (or few frames, depending on a particular stream's frame size). Whenever the decoder
// requests data for a new frame, the new data is moved towards the beginning of the
// buffer, so that the decoder always starts reading a new frame from position 0.
// Before submitting the buffer to the decoder, a callback is used to fill the remainder
// of the buffer.
// This buffer is sized to hold the largest possible MP3 buffer size (1440-1441) + some
// slack (1024+512)
// (See: player.c)
#define DATA_BUFFER_SIZE 1536

// Successful frame counter: how many before we trust we're actually playing
// the stream?
// When connecting to a stream, it might take some time before the decoder is able
// to decode subsequent frames successfully. This number indicates how many frames we
// want to have been decoded successfully, before starting sound output to the
// speakers and updating the display to show  the stream info.
// (See: player.c)
#define SUBSEQUENT_SUCCESSES  50

// Output buffer size (in samples; each sample is 2 bytes)
// This is the size of the buffer that contains audio samples for playing.
// This is a circular buffer
// (See: audio.c)
//#define OUTPUT_BUFFER_SIZE 3072
#define OUTPUT_BUFFER_SIZE 5120

// Maximum number of entries to store when scanning for WiFi networks within range.
// This will be used to present a list of networks to the user when first connecting
// to a network, or when the known networks have been reset.
// (See: network.c)
#define WLAN_SCAN_COUNT 20

// LCD screen width and height in pixels
#if DISPLAY_TYPE == nokia
// Nokia 3310 LCD screen
// (See: display.c)
#define SCREENW     84
#define SCREENH     48
#define SSID_DISPLAY_LEN 15
#else
// EA-DOGL128-6 LCD screen
// (See: display_ea-dogl128-6.c)
#define SCREENW     128
#define SCREENH     64
#define SSID_DISPLAY_LEN 20
#endif

// Size of the buffer that is used to load favorites data from
// a remote (network) location. This limits the line length of the
// favorites data, not the total size.
// (See: remote_data.c)
#define REMOTE_BUFFER_SIZE    256
// Buffer for line-by-line processing. This determines the max.
// line length.
// (See: remote_data.c)
#define REMOTE_DATA_LINE_BUFFER_SIZE 100

// Internal sizes for station entries.
// (See: remote_data.h)
#define STATION_ENTRY_NAME_LENGTH 20
#define STATION_ENTRY_IP_LENGTH 16
#define STATION_ENTRY_QUERY_LENGTH 75 

#define GLOBAL_STATIONS_ENTRIES 10
