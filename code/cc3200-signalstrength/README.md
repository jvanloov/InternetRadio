CC3200 signal strength dumper

This simple program uses the CC3200 SDK's functionality for scanning
the WiFi network, and dumping the network names and RSSI values onto
the UART.

(Use ``screen /dev/<device-id> 115200`` to see the UART output.) 

The original purpose of this program was to compare signal strengths
for different configurations of the antenna section of the CC3200MOD
breakout board
(http://joris.van-looveren.net/projects/cc3200-breakout/)
(with/without external RF filter, with/without pigtail antenna).

The output of this program looks like this:

    Welcome to CC3200 signal strength dumper
    Starting...
    -- network:
       ssid:    <network name>
       bssid:   <network identifier>
       rssi:    <network strength>
       security:<network security>
    -- network:
       ...
	   
It lists all networks the CC3200MOD can see, one ``-- network:``
section for each visible WiFi network.
