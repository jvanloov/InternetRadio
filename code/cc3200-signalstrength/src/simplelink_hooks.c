// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/************************************************************
 * simplelink_hooks.c                                       *
 ************************************************************
 * Event handlers for SimpleLink events.                    *
 * Joris Van Looveren, 02/2017                              *
 *                                                          *
 * We currently don't really use this; the callbacks are    *
 * mostly used to print information to the terminal, if the *
 * diagnostic output is turned on.                          *
 ************************************************************/

#include <string.h>

#include "common.h"
#include "netapp.h"

#include "hal_mcu.h"


extern unsigned long  g_ulStatus ;
extern unsigned long  g_ulGatewayIP;
extern unsigned char  g_ucConnectionSSID[SSID_LEN_MAX+1];
extern unsigned char  g_ucConnectionBSSID[BSSID_LEN_MAX];


//*****************************************************************************
// SimpleLink Asynchronous Event Handlers -- Start
//*****************************************************************************

#ifndef NO_TERM
static void term_print_bssid(void) {
  term_print_int(g_ucConnectionBSSID[0]);
  term_print(":");
  term_print_int(g_ucConnectionBSSID[1]);
  term_print(":");
  term_print_int(g_ucConnectionBSSID[2]);
  term_print(":");
  term_print_int(g_ucConnectionBSSID[3]);
  term_print(":");
  term_print_int(g_ucConnectionBSSID[4]);
  term_print(":");
  term_print_int(g_ucConnectionBSSID[5]);
}

static void term_print_ipv4(char b3, char b2, char b1, char b0) {
  term_print_int(b3);
  term_print(".");
  term_print_int(b2);
  term_print(".");
  term_print_int(b1);
  term_print(".");
  term_print_int(b0);
}
#endif


//*****************************************************************************
//
//! \brief The Function Handles WLAN Events
//!
//! \param[in]  pWlanEvent - Pointer to WLAN Event Info
//!
//! \return None
//!
//*****************************************************************************
void SimpleLinkWlanEventHandler(SlWlanEvent_t *pWlanEvent)
{
    switch(pWlanEvent->Event)
    {
        case SL_WLAN_CONNECT_EVENT:
        {
            SET_STATUS_BIT(g_ulStatus, STATUS_BIT_CONNECTION);
            
            //
            // Information about the connected AP (like name, MAC etc) will be
            // available in 'slWlanConnectAsyncResponse_t'-Applications
            // can use it if required
            //
            //  slWlanConnectAsyncResponse_t *pEventData = NULL;
            // pEventData = &pWlanEvent->EventData.STAandP2PModeWlanConnected;
            //
            
            // Copy new connection SSID and BSSID to global parameters
            memcpy(g_ucConnectionSSID,pWlanEvent->EventData.
                   STAandP2PModeWlanConnected.ssid_name,
                   pWlanEvent->EventData.STAandP2PModeWlanConnected.ssid_len);
            memcpy(g_ucConnectionBSSID,
                   pWlanEvent->EventData.STAandP2PModeWlanConnected.bssid,
                   SL_BSSID_LENGTH);

 #ifndef NO_TERM
            term_print("[WLAN EVENT] STA Connected to the AP: ");
	    term_print(g_ucConnectionSSID);
	    term_print(", BSSID: ");
	    term_print_bssid();
	    term_print("\n\r");
#endif
        }
        break;

        case SL_WLAN_DISCONNECT_EVENT:
        {
            slWlanConnectAsyncResponse_t*  pEventData = NULL;

            CLR_STATUS_BIT(g_ulStatus, STATUS_BIT_CONNECTION);
            CLR_STATUS_BIT(g_ulStatus, STATUS_BIT_IP_AQUIRED);

            pEventData = &pWlanEvent->EventData.STAandP2PModeDisconnected;

            // If the user has initiated 'Disconnect' request, 
            //'reason_code' is SL_WLAN_DISCONNECT_USER_INITIATED_DISCONNECTION 
            if(SL_WLAN_DISCONNECT_USER_INITIATED_DISCONNECTION == pEventData->reason_code)
            {
#ifndef NO_TERM
	      term_print("Disconnected from wireless network '");
	      term_print(g_ucConnectionSSID);
	      term_print("'\r\n");
	      term_print(", BSSID: ");
	      term_print_bssid();
	      term_print(" on application's request \n\r");
#endif
	    }
            else
            {
#ifndef NO_TERM
	      term_print("Disconnected from wireless network '");
	      term_print(g_ucConnectionSSID);
	      term_print("'\r\n");
	      term_print(", BSSID: ");
	      term_print_bssid();
	      term_print(" due to an error.\r\n");
#endif
            }
            memset(g_ucConnectionSSID,0,sizeof(g_ucConnectionSSID));
            memset(g_ucConnectionBSSID,0,sizeof(g_ucConnectionBSSID));
        }
        break;

        default:
        {
#ifndef NO_TERM
	  term_print("[WLAN EVENT] Unexpected event ");
	  term_print_int(pWlanEvent->Event);
	  term_print("\r\n");
#endif
        }
        break;
    }
}

//*****************************************************************************
//
//! \brief This function handles network events such as IP acquisition, IP
//!           leased, IP released etc.
//!
//! \param[in]  pNetAppEvent - Pointer to NetApp Event Info 
//!
//! \return None
//!
//*****************************************************************************
void SimpleLinkNetAppEventHandler(SlNetAppEvent_t *pNetAppEvent)
{
    switch(pNetAppEvent->Event)
    {
        case SL_NETAPP_IPV4_IPACQUIRED_EVENT:
        {
            SlIpV4AcquiredAsync_t *pEventData = NULL;

            SET_STATUS_BIT(g_ulStatus, STATUS_BIT_IP_AQUIRED);
            
            //Ip Acquired Event Data
            pEventData = &pNetAppEvent->EventData.ipAcquiredV4;
            
            //Gateway IP address
            g_ulGatewayIP = pEventData->gateway;

#ifndef NO_TERM
            term_print("[NETAPP EVENT] IP Acquired: IP=");
	    term_print_ipv4(SL_IPV4_BYTE(pNetAppEvent->EventData.ipAcquiredV4.ip,3),
	    		    SL_IPV4_BYTE(pNetAppEvent->EventData.ipAcquiredV4.ip,2),
	    		    SL_IPV4_BYTE(pNetAppEvent->EventData.ipAcquiredV4.ip,1),
	    		    SL_IPV4_BYTE(pNetAppEvent->EventData.ipAcquiredV4.ip,0));
	    term_print(" Gateway=");
	    term_print_ipv4(SL_IPV4_BYTE(pNetAppEvent->EventData.ipAcquiredV4.gateway,3),
	    		    SL_IPV4_BYTE(pNetAppEvent->EventData.ipAcquiredV4.gateway,2),
	    		    SL_IPV4_BYTE(pNetAppEvent->EventData.ipAcquiredV4.gateway,1),
	    		    SL_IPV4_BYTE(pNetAppEvent->EventData.ipAcquiredV4.gateway,0));
	    term_print("\r\n");
#endif
	}
        break;

        default:
        {
#ifndef NO_TERM
	  term_print("[NETAPP EVENT] Unexpected event ");
          term_print_int(pNetAppEvent->Event);
	  term_print("\r\n");
#endif
        }
        break;
    }
}


//*****************************************************************************
//
//! \brief This function handles HTTP server events
//!
//! \param[in]  pServerEvent - Contains the relevant event information
//! \param[in]    pServerResponse - Should be filled by the user with the
//!                                      relevant response information
//!
//! \return None
//!
//****************************************************************************
void SimpleLinkHttpServerCallback(SlHttpServerEvent_t *pHttpEvent,
                                  SlHttpServerResponse_t *pHttpResponse)
{
    // Unused in this application
}

//*****************************************************************************
//
//! \brief This function handles General Events
//!
//! \param[in]     pDevEvent - Pointer to General Event Info 
//!
//! \return None
//!
//*****************************************************************************
void SimpleLinkGeneralEventHandler(SlDeviceEvent_t *pDevEvent)
{
  //
  // Most of the general errors are not FATAL are are to be handled
  // appropriately by the application
  //
#ifndef NO_TERM
  term_print("[GENERAL EVENT] - ID=[");
  term_print_int(pDevEvent->EventData.deviceEvent.status);
  term_print("] Sender=[");
  term_print_int(pDevEvent->EventData.deviceEvent.sender);
  term_print("]\r\n");
#endif
}


//*****************************************************************************
//
//! This function handles socket events indication
//!
//! \param[in]      pSock - Pointer to Socket Event Info
//!
//! \return None
//!
//*****************************************************************************
void SimpleLinkSockEventHandler(SlSockEvent_t *pSock)
{
    //
    // This application doesn't work w/ socket - Events are not expected
    //
    switch( pSock->Event )
    {
        case SL_SOCKET_TX_FAILED_EVENT:
            switch( pSock->socketAsyncEvent.SockTxFailData.status)
            {
                case SL_ECLOSE: 
#ifndef NO_TERM
		  term_print("[SOCK ERROR] - close socket (");
		  term_print_int(pSock->socketAsyncEvent.SockTxFailData.sd);
		  term_print(") operation failed to transmit all queued packets\r\n");
#endif
		  break;
                default: 
#ifndef NO_TERM
                  term_print("[SOCK ERROR] - TX FAILED  :  socket ");
		  term_print_int(pSock->socketAsyncEvent.SockTxFailData.sd);
		  term_print(", reason (");
		  term_print_int(pSock->socketAsyncEvent.SockTxFailData.status);
		  term_print(")\r\n");
#endif
		  break;
            }
            break;

        default:
#ifndef NO_TERM
	  term_print("[SOCK EVENT] - Unexpected Event [");
	  term_print_int(pSock->Event);
	  term_print("]\r\n");
#endif
          break;
    }

}


//*****************************************************************************
// SimpleLink Asynchronous Event Handlers -- End
//*****************************************************************************
