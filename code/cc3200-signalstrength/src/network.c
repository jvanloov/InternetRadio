// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/************************************************************
 * network.c                                                *
 ************************************************************
 * Networking routines                                      *
 * Joris Van Looveren, 02/2017                              *
 *                                                          *
 * This module provides a number of routines related to the *
 * networking component. These include both lower-level     *
 * SimpleLink routines, and higher-level routines for       *
 * selecting a WiFi network, and the startup routine of     *
 * trying to connect to a known network, and prompting the  *
 * user if no known network can be connected to.            *
 ************************************************************/

#include <string.h>

#include "network.h"
#include "rom_map.h"
#include "utils.h"
#include "misc_utils.h"

#include "hal_mcu.h"

//*****************************************************************************
//                 GLOBAL VARIABLES -- Start
//*****************************************************************************

unsigned long  g_ulStatus = 0;//SimpleLink Status
unsigned long  g_ulGatewayIP = 0; //Network Gateway IP address
unsigned char  g_ucConnectionSSID[SSID_LEN_MAX+1]; //Connection SSID
unsigned char  g_ucConnectionBSSID[BSSID_LEN_MAX]; //Connection BSSID

Sl_WlanNetworkEntry_t netEntries[WLAN_SCAN_COUNT]; // Wlan Network Entry

//*****************************************************************************
//! \brief Initialize global network (SimpleLink) variables
//! \param   none
//! \return  none
//*****************************************************************************

void network_init(void) {
  g_ulStatus = 0;
  g_ulGatewayIP = 0;
  memset(g_ucConnectionSSID,0,sizeof(g_ucConnectionSSID));
  memset(g_ucConnectionBSSID,0,sizeof(g_ucConnectionBSSID));
}


long network_start(void) {
  return sl_Start(0, 0, 0);
}


long network_stop(void) {
  return sl_Stop(SL_STOP_TIMEOUT);
}


//*****************************************************************************
//! \brief This function puts the device in its default state. It:
//!           - Set the mode to STATION
//!           - Configures connection policy to Auto and AutoSmartConfig
//!           - Deletes all the stored profiles
//!           - Enables DHCP
//!           - Disables Scan policy
//!           - Sets Tx power to maximum
//!           - Sets power policy to normal
//!           - Unregister mDNS services
//!           - Remove all filters
//!
//! (JVL: adapted from the TI CC3200 SDK.)
//!
//! \param   none
//! \return  Connected by auto policy: NETWORK_CONNECTED_AUTO
//!          Not connected, but successfully configured: SUCCESS
//!          Error: FAILURE
//*****************************************************************************

long network_configure_default_state(void) {
    SlVersionFull   ver = {0};
    _WlanRxFilterOperationCommandBuff_t  RxFilterIdMask = {0};

    unsigned char ucVal = 1;
    unsigned char ucConfigOpt = 0;
    unsigned char ucConfigLen = 0;
    unsigned char ucPower = 0;

    long lRetVal = -1;
    long lMode = -1;
    long errorCode = 0;

    lMode = sl_Start(0, 0, 0);
    if (lMode < 0) { goto error; }

    // If the device is not in station-mode, try configuring it in station-mode 
    if (ROLE_STA != lMode)
    {
        if (ROLE_AP == lMode)
        {
            // If the device is in AP mode, we need to wait for this event 
            // before doing anything 
            while(!IS_IP_ACQUIRED(g_ulStatus))
            {
              _SlNonOsMainLoopTask(); 
            }
        }

        // Switch to STA role and restart 
        lRetVal = sl_WlanSetMode(ROLE_STA);
	if (lMode < 0) { goto error; }

        lRetVal = sl_Stop(0xFF);
	if (lMode < 0) { goto error; }

        lRetVal = sl_Start(0, 0, 0);
	if (lMode < 0) { goto error; }

        // Check if the device is in station again 
        if (ROLE_STA != lRetVal)
        {
            // We don't want to proceed if the device is not coming up in STA-mode 
	    errorCode = DEVICE_NOT_IN_STATION_MODE;
	    goto error;
        }
    }
    
    // Try connecting in auto mode (from a stored profile)
    lRetVal = sl_WlanPolicySet(SL_POLICY_CONNECTION,  SL_CONNECTION_POLICY(1, 0, 0, 0, 0), NULL, 0);
    if (lMode < 0) { goto error; }

    //wait until IP is acquired
#ifndef NO_TERM
    term_print("Trying to connect to a known network...\r\n");
#endif
    int timeout = 50;
    while ((!IS_CONNECTED(g_ulStatus) || !IS_IP_ACQUIRED(g_ulStatus)) && (timeout > 0)) {
        _SlNonOsMainLoopTask();
	mcu_delay(100000);
	timeout -= 1;
    }
 
    if (IS_CONNECTED(g_ulStatus) && IS_IP_ACQUIRED(g_ulStatus)) {
      return NETWORK_CONNECTED_AUTO;
    }
    
    // Enable DHCP client
    lRetVal = sl_NetCfgSet(SL_IPV4_STA_P2P_CL_DHCP_ENABLE,1,1,&ucVal);
    if (lMode < 0) { goto error; }

    // Disable scan
    ucConfigOpt = SL_SCAN_POLICY(0);
    lRetVal = sl_WlanPolicySet(SL_POLICY_SCAN , ucConfigOpt, NULL, 0);
    if (lMode < 0) { goto error; }

    // Set Tx power level for station mode
    // Number between 0-15, as dB offset from max power - 0 will set max power
    ucPower = 0;
    lRetVal = sl_WlanSet(SL_WLAN_CFG_GENERAL_PARAM_ID, WLAN_GENERAL_PARAM_OPT_STA_TX_POWER, 1, (unsigned char *)&ucPower);
    if (lMode < 0) { goto error; }

    // Set PM policy to normal
    lRetVal = sl_WlanPolicySet(SL_POLICY_PM , SL_NORMAL_POLICY, NULL, 0);
    if (lMode < 0) { goto error; }

    // Unregister mDNS services
    lRetVal = sl_NetAppMDNSUnRegisterService(0, 0);
    if (lMode < 0) { goto error; }

    // Remove  all 64 filters (8*8)
    memset(RxFilterIdMask.FilterIdMask, 0xFF, 8);
    lRetVal = sl_WlanRxFilterSet(SL_REMOVE_RX_FILTER, (_u8 *)&RxFilterIdMask,
                       sizeof(_WlanRxFilterOperationCommandBuff_t));
    if (lMode < 0) { goto error; }

    lRetVal = sl_Stop(SL_STOP_TIMEOUT);
    if (lMode < 0) { goto error; }

    return lRetVal; // Success

 error:

    if (errorCode < 0) {
      return errorCode;
    } else {
      return FAILURE;
    }
}


//*****************************************************************************
//! \brief Scan for wireless networks
//!        The network list is populated in the netEntries array.
//!
//! \param   none
//! \return  the number of networks found, or FAILURE (-1)
//*****************************************************************************

long network_scan_wifi_networks(void) {
    long lRetVal = -1;
    long errorCode = 0;
    int numEntries = 0;
    
    unsigned short ucIndex;
    unsigned char ucpolicyOpt;
    union {
        unsigned char ucPolicy[4];
        unsigned int uiPolicyLen;
    } policyVal;

    // Assumption is that the device is configured in station mode already and it is in its default state
    lRetVal = sl_Start(0, 0, 0);
    if (lRetVal < 0 || ROLE_STA != lRetVal) {
      goto error;
    }

    // make sure the connection policy is not set (so no scan is run in the background)
    ucpolicyOpt = SL_CONNECTION_POLICY(0, 0, 0, 0,0);
    lRetVal = sl_WlanPolicySet(SL_POLICY_CONNECTION , ucpolicyOpt, NULL, 0);
    if(lRetVal != 0) { goto error; }
    
    ucpolicyOpt = SL_SCAN_POLICY(1);  // enable scan
    policyVal.uiPolicyLen = 15;       // set scan cycle to 15 seconds 
    // start the scan 
    lRetVal = sl_WlanPolicySet(SL_POLICY_SCAN , ucpolicyOpt,
                               (unsigned char*)(policyVal.ucPolicy), sizeof(policyVal));
    if(lRetVal!=0) { goto error; }
    MAP_UtilsDelay(8000000);

    // get scan results
    ucIndex = 0;
    // retVal indicates the valid number of entries 
    // The scan results are occupied in netEntries[] 
    numEntries = sl_WlanGetNetworkList(ucIndex, (unsigned char)WLAN_SCAN_COUNT,
                                    &netEntries[ucIndex]);
    if(numEntries==0) { goto error; }

    // disable scan 
    ucpolicyOpt = SL_SCAN_POLICY(0);
    lRetVal = sl_WlanPolicySet(SL_POLICY_SCAN , ucpolicyOpt, NULL, 0);
    if(lRetVal != 0) { goto error; }

    // stop the device
    lRetVal = sl_Stop(SL_STOP_TIMEOUT);
    if (lRetVal < 0) { goto error; }

    return numEntries;

 error:

    if (errorCode < 0) {
      return errorCode;
    } else {
      return FAILURE;
    }
}


//****************************************************************************
//! \brief Connecting to a WLAN Accesspoint
//!
//!  This function connects to the required AP (SSID_NAME) with Security
//!  parameters specified in te form of macros at the top of this file
//!
//! \param[in] ssid - zero-terminated char* with the SSID name
//! \param[in] key - zero-terminated char* with the network passphrase
//! \param[in] sec_type - network security type (SL_SEC_TYPE_OPEN, SL_SEC_TYPE_WEP, SL_SEC_TYPE_WPA_WPA2)
//!
//! \return  SUCCESS or FAILURE
//****************************************************************************

long network_connect(char *ssid, char *key, int sec_type) {
    SlSecParams_t secParams = {0};
    long lRetVal = 0;
    long errorCode = 0;
    
    secParams.Key = (signed char*)key;
    secParams.KeyLen = strlen(key);
    secParams.Type = sec_type;

    lRetVal = sl_WlanConnect((signed char*)ssid, strlen(ssid), 0, &secParams, 0);
    if (lRetVal < 0) { goto error; };

    // Check if we're properly connected. If after 50 iterations we aren't
    // connected yet, fail with timeout.
    int timeout = 50;
    while ((!IS_CONNECTED(g_ulStatus) || !IS_IP_ACQUIRED(g_ulStatus)) && (timeout > 0))
    { 
      _SlNonOsMainLoopTask();
      mcu_delay(100000);
      timeout -= 1;
    }

    if (!IS_CONNECTED(g_ulStatus) || !IS_IP_ACQUIRED(g_ulStatus)) {
      goto error;
    }

    return SUCCESS;
    
 error:

    if (errorCode < 0) {
      return errorCode;
    } else {
      return FAILURE;
    }
}


int network_get_wifi_list() {
  int result = network_scan_wifi_networks();

  // Count the networks; stop at 20 if there are more.
  int counter = 0;
  while ((counter < 20) && (netEntries[counter].ssid_len > 0)) {
    counter++;
  }

  return counter;
}


// Networking utilities
signed long network_parse_ip(char *ipstr) {
  int ip1, ip2, ip3, ip4;
  // make a local copy of the ip, since strtok modifies its argument
  char ip_cpy[25];
  memset(ip_cpy, 0, 24); // keep at least the final \0, so the string is always terminated.
  strcpy(ip_cpy, ipstr);

  char *ip1_str = strtok(ip_cpy, ".");
  ip1 = parse_unsigned_num(ip1_str, 3);
  if ((ip1 != -1) && (ip1 >= 0) && (ip1 <= 255)) {
    char *ip2_str = strtok(NULL, ".");
    ip2 = parse_unsigned_num(ip2_str, 3);
    if ((ip2 != -1) && (ip2 >= 0) && (ip2 <= 255)) {
      char *ip3_str = strtok(NULL, ".");
      ip3 = parse_unsigned_num(ip3_str, 3);
      if ((ip3 != -1)  && (ip3 >= 0) && (ip3 <= 255)) {
	char *ip4_str = strtok(NULL, ":");  // port number separator
	ip4 = parse_unsigned_num(ip4_str, 3);
	if ((ip4 != -1) && (ip4 >= 0) && (ip4 <= 255)) {
	  return (ip1 << 24) + (ip2 << 16) + (ip3 << 8) + ip4;
	}
      }
    }
  }
  return -1;
}

signed int network_parse_port(char *ipstr) {
  int colon_index = -1;
  int len = strlen(ipstr);
  
  // check if colon is present
  for (int i=0; i<len; i++) {
    if (ipstr[i] == ':') {
      colon_index = i;
      break;
    }
  }
  
  if ((colon_index >= 0) && (len > colon_index+1)) {
    char *portstr = &ipstr[colon_index+1];
    int port = parse_unsigned_num(portstr, 5);
    if ((port >= 0) && (port <= 65535)) {
      return port;
    }
  }

  return -1;
}

