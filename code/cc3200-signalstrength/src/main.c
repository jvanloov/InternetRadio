// Copyright 2018 Joris Van Looveren
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/************************************************************
 * main.c                                                   *
 ************************************************************
 * Main loop for the netradio implementation.               *
 ************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "socket.h"  // from simplelink SDK

#include "hal_mcu.h"
#include "network.h"

extern Sl_WlanNetworkEntry_t netEntries[WLAN_SCAN_COUNT];


int main(void) {
  int numEntries = 0;

  board_init();
  hal_init();
  
#ifndef NO_TERM
  uart_init();
  term_init(115200);
#endif

  term_print("Welcome to CC3200 signal strength dumper\r\n");
  term_print("Starting...\r\n");

  network_init();
  numEntries = network_scan_wifi_networks();

  if (numEntries > 0) {
    int i;
    for (i=0; i<numEntries; i++) {
      term_print("-- network:\r\n");
      term_print("   ssid:    " );
      term_print(netEntries[i].ssid);
      term_print("\r\n");
      term_print("   bssid:   ");
      for (int j=0; j<6; j++) {
	term_print_hex(netEntries[i].bssid[j], 0);
	if (j < 5) {
	  term_print(":");
	}
      }
      term_print("\r\n");
      term_print("   rssi:    ");
      term_print_int(netEntries[i].rssi);
      term_print("\r\n");
      term_print("   security:");
      switch(netEntries[i].sec_type) {
      case SL_SEC_TYPE_OPEN:
	term_print("open");
	break;
      case SL_SEC_TYPE_WEP:
	term_print("WEP");
	break;
      case SL_SEC_TYPE_WPA_WPA2:
	term_print("WPA2");
	break;
      case SL_SEC_TYPE_WPA_ENT:
	term_print("WPA enterprise");
	break;
      case SL_SEC_TYPE_WPS_PBC:
	term_print("WPS (button)");
	break;
      case SL_SEC_TYPE_WPS_PIN:
	term_print("WPS (pin)");
	break;
      default:
	term_print("(unknown)");
      }
      term_print("\r\n");
    }
  } else {
    term_print("An error occurred scanning the wifi networks. Code = ");
    term_print_int(numEntries);
    term_print("\r\n");
  }

  
  hal_cleanup();

  return 1;
}
