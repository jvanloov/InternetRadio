EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:netradio-components
LIBS:netradio-3-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "NetRadio rev. 3"
Date "2017-12-01"
Rev "1"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L LM2596S-5.0 U1
U 1 1 59BD0FE7
P 1750 1100
F 0 "U1" H 2050 1350 60  0000 L CNN
F 1 "LM2596S-5.0" H 1300 1350 60  0000 L CNN
F 2 "TO_SOT_Packages_SMD:TO-263-5Lead" H 1750 1400 60  0001 C CNN
F 3 "" H 1750 1400 60  0000 C CNN
	1    1750 1100
	1    0    0    -1  
$EndComp
$Comp
L INDUCTOR_SMALL L1
U 1 1 59BD35D5
P 2750 1050
F 0 "L1" H 2750 1150 50  0000 C CNN
F 1 "150uH" H 2750 1000 50  0000 C CNN
F 2 "Inductors_SMD:L_12x12mm_h8mm" H 2750 1050 50  0001 C CNN
F 3 "" H 2750 1050 50  0000 C CNN
	1    2750 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 1050 2500 1050
Wire Wire Line
	3000 1050 3000 1250
Wire Wire Line
	3000 1250 2250 1250
$Comp
L CP C1
U 1 1 59BD367D
P 3200 1450
F 0 "C1" H 3225 1550 50  0000 L CNN
F 1 "220uF" H 3225 1350 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Radial_D10_L20_P5-7.5" H 3238 1300 50  0001 C CNN
F 3 "" H 3200 1450 50  0000 C CNN
	1    3200 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 1050 3200 1300
Connection ~ 3000 1050
Connection ~ 2500 1050
Connection ~ 2250 1050
Connection ~ 2250 1250
Connection ~ 3200 1300
$Comp
L GND #PWR01
U 1 1 59BD36BB
P 3200 1600
F 0 "#PWR01" H 3200 1350 50  0001 C CNN
F 1 "GND" H 3200 1450 50  0000 C CNN
F 2 "" H 3200 1600 50  0000 C CNN
F 3 "" H 3200 1600 50  0000 C CNN
	1    3200 1600
	1    0    0    -1  
$EndComp
Connection ~ 3200 1600
$Comp
L D_Schottky D1
U 1 1 59BD37F6
P 2500 1450
F 0 "D1" H 2500 1550 50  0000 C CNN
F 1 "SS24" H 2500 1350 50  0000 C CNN
F 2 "Diodes_SMD:SMB_Handsoldering" H 2500 1450 50  0001 C CNN
F 3 "" H 2500 1450 50  0000 C CNN
	1    2500 1450
	0    1    1    0   
$EndComp
Wire Wire Line
	2500 1050 2500 1300
$Comp
L GND #PWR02
U 1 1 59BD398C
P 2500 1600
F 0 "#PWR02" H 2500 1350 50  0001 C CNN
F 1 "GND" H 2500 1450 50  0000 C CNN
F 2 "" H 2500 1600 50  0000 C CNN
F 3 "" H 2500 1600 50  0000 C CNN
	1    2500 1600
	1    0    0    -1  
$EndComp
Connection ~ 2500 1600
Connection ~ 2500 1300
Wire Wire Line
	3000 1050 3500 1050
Text GLabel 3500 1050 2    60   Output ~ 0
5V
Connection ~ 3200 1050
$Comp
L GND #PWR03
U 1 1 59BD478B
P 1050 1600
F 0 "#PWR03" H 1050 1350 50  0001 C CNN
F 1 "GND" H 1050 1450 50  0000 C CNN
F 2 "" H 1050 1600 50  0000 C CNN
F 3 "" H 1050 1600 50  0000 C CNN
	1    1050 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 1150 1050 1600
Wire Wire Line
	1050 1300 1250 1300
Wire Wire Line
	1250 1150 1050 1150
Connection ~ 1050 1300
Connection ~ 1050 1600
$Comp
L CP C2
U 1 1 59BD48C7
P 850 1150
F 0 "C2" H 875 1250 50  0000 L CNN
F 1 "220uF" H 875 1050 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Radial_D10_L20_P5-7.5" H 888 1000 50  0001 C CNN
F 3 "" H 850 1150 50  0000 C CNN
	1    850  1150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 59BD492F
P 850 1600
F 0 "#PWR04" H 850 1350 50  0001 C CNN
F 1 "GND" H 850 1450 50  0000 C CNN
F 2 "" H 850 1600 50  0000 C CNN
F 3 "" H 850 1600 50  0000 C CNN
	1    850  1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	850  1600 850  1300
Connection ~ 850  1300
Connection ~ 850  1600
Connection ~ 850  1000
$Comp
L MCP1602-3.3V U2
U 1 1 59C02F98
P 5250 1200
F 0 "U2" H 5750 1600 60  0000 C CNN
F 1 "MCP1602-3.3V" H 5100 1600 60  0000 C CNN
F 2 "Housings_SSOP:MSOP-8_3x3mm_Pitch0.65mm" H 5800 950 60  0001 C CNN
F 3 "" H 5800 950 60  0000 C CNN
	1    5250 1200
	1    0    0    -1  
$EndComp
Text GLabel 4000 800  0    60   Input ~ 0
5V
$Comp
L R R1
U 1 1 59C0321A
P 4400 950
F 0 "R1" V 4480 950 50  0000 C CNN
F 1 "10R" V 4400 950 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 4330 950 50  0001 C CNN
F 3 "" H 4400 950 50  0000 C CNN
	1    4400 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 800  4650 800 
Wire Wire Line
	4650 800  4650 1400
Wire Wire Line
	4650 950  4800 950 
Connection ~ 4400 800 
Connection ~ 4800 950 
Wire Wire Line
	4400 1100 4800 1100
Connection ~ 4800 1100
Connection ~ 4400 1100
$Comp
L GND #PWR05
U 1 1 59C03482
P 4100 1100
F 0 "#PWR05" H 4100 850 50  0001 C CNN
F 1 "GND" H 4100 950 50  0000 C CNN
F 2 "" H 4100 1100 50  0000 C CNN
F 3 "" H 4100 1100 50  0000 C CNN
	1    4100 1100
	1    0    0    -1  
$EndComp
$Comp
L C_Small C4
U 1 1 59C0355A
P 4400 1200
F 0 "C4" H 4410 1270 50  0000 L CNN
F 1 "100nF" H 4410 1120 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 4400 1200 50  0001 C CNN
F 3 "" H 4400 1200 50  0000 C CNN
	1    4400 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 1300 4600 1300
Wire Wire Line
	4600 1300 4600 1250
Wire Wire Line
	4600 1250 4800 1250
Connection ~ 4800 1250
Connection ~ 4400 1300
$Comp
L GND #PWR06
U 1 1 59C03706
P 4400 1300
F 0 "#PWR06" H 4400 1050 50  0001 C CNN
F 1 "GND" H 4400 1150 50  0000 C CNN
F 2 "" H 4400 1300 50  0000 C CNN
F 3 "" H 4400 1300 50  0000 C CNN
	1    4400 1300
	1    0    0    -1  
$EndComp
$Comp
L INDUCTOR_SMALL L2
U 1 1 59C03862
P 6100 950
F 0 "L2" H 6100 1050 50  0000 C CNN
F 1 "4.7uH" H 6100 900 50  0000 C CNN
F 2 "Inductors_SMD:L_1210_HandSoldering" H 6100 950 50  0001 C CNN
F 3 "" H 6100 950 50  0000 C CNN
	1    6100 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 950  5850 950 
Wire Wire Line
	5750 1100 6350 1100
Wire Wire Line
	6350 1100 6350 950 
Connection ~ 5850 950 
Connection ~ 5750 950 
Connection ~ 6350 950 
Connection ~ 5750 1100
$Comp
L CP C5
U 1 1 59C039D9
P 6500 1100
F 0 "C5" H 6525 1200 50  0000 L CNN
F 1 "4.7uF" H 6525 1000 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 6538 950 50  0001 C CNN
F 3 "" H 6500 1100 50  0000 C CNN
	1    6500 1100
	1    0    0    -1  
$EndComp
$Comp
L CP C3
U 1 1 59C03A42
P 4100 950
F 0 "C3" H 4125 1050 50  0000 L CNN
F 1 "4.7uF" H 4125 850 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 4138 800 50  0001 C CNN
F 3 "" H 4100 950 50  0000 C CNN
	1    4100 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 950  6700 950 
Text GLabel 6700 950  2    60   Output ~ 0
3.3V
Connection ~ 6500 950 
Wire Wire Line
	5750 1250 6500 1250
Connection ~ 5750 1250
Connection ~ 6500 1250
$Comp
L GND #PWR07
U 1 1 59C03DDE
P 6500 1250
F 0 "#PWR07" H 6500 1000 50  0001 C CNN
F 1 "GND" H 6500 1100 50  0000 C CNN
F 2 "" H 6500 1250 50  0000 C CNN
F 3 "" H 6500 1250 50  0000 C CNN
	1    6500 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 1400 4800 1400
Connection ~ 4650 950 
$Comp
L CONN_01X02 P3
U 1 1 59C04768
P 5350 1750
F 0 "P3" H 5350 1900 50  0000 C CNN
F 1 "5V out" V 5450 1750 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x02" H 5350 1750 50  0001 C CNN
F 3 "" H 5350 1750 50  0000 C CNN
	1    5350 1750
	1    0    0    -1  
$EndComp
Text GLabel 3800 1700 0    60   Input ~ 0
5V
Text GLabel 3800 2400 0    60   Input ~ 0
3.3V
Wire Wire Line
	3800 2400 5350 2400
$Comp
L GND #PWR08
U 1 1 59C048C2
P 3900 2800
F 0 "#PWR08" H 3900 2550 50  0001 C CNN
F 1 "GND" H 3900 2650 50  0000 C CNN
F 2 "" H 3900 2800 50  0000 C CNN
F 3 "" H 3900 2800 50  0000 C CNN
	1    3900 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 2000 3900 2800
Wire Wire Line
	3900 2700 4900 2700
Wire Wire Line
	3900 2000 4700 2000
Connection ~ 3900 2700
$Comp
L CONN_01X02 P1
U 1 1 59C04A0E
P 1650 2100
F 0 "P1" H 1650 2250 50  0000 C CNN
F 1 "Power in" V 1750 2100 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x02" H 1650 2100 50  0001 C CNN
F 3 "" H 1650 2100 50  0000 C CNN
	1    1650 2100
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 P2
U 1 1 59C04A50
P 1650 2650
F 0 "P2" H 1650 2800 50  0000 C CNN
F 1 "ON/OFF" V 1750 2650 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x02" H 1650 2650 50  0001 C CNN
F 3 "" H 1650 2650 50  0000 C CNN
	1    1650 2650
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR09
U 1 1 59C04A87
P 1450 2250
F 0 "#PWR09" H 1450 2000 50  0001 C CNN
F 1 "GND" H 1450 2100 50  0000 C CNN
F 2 "" H 1450 2250 50  0000 C CNN
F 3 "" H 1450 2250 50  0000 C CNN
	1    1450 2250
	1    0    0    -1  
$EndComp
$Comp
L +BATT #PWR010
U 1 1 59C04AB9
P 750 2050
F 0 "#PWR010" H 750 1900 50  0001 C CNN
F 1 "+BATT" H 750 2190 50  0000 C CNN
F 2 "" H 750 2050 50  0000 C CNN
F 3 "" H 750 2050 50  0000 C CNN
	1    750  2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	750  2050 1150 2050
Wire Wire Line
	1350 2050 1450 2050
Wire Wire Line
	1350 2050 1350 2600
Wire Wire Line
	1350 2600 1450 2600
Wire Wire Line
	1450 2150 1450 2250
Wire Wire Line
	1450 2700 1150 2700
Wire Wire Line
	1150 2700 1150 2050
Connection ~ 1450 2050
Connection ~ 1450 2150
Connection ~ 1450 2600
Connection ~ 1450 2700
Connection ~ 9350 -1250
Connection ~ 4100 800 
Connection ~ 6600 950 
$Comp
L CONN_01X03 P4
U 1 1 59C05684
P 5550 2500
F 0 "P4" H 5550 2700 50  0000 C CNN
F 1 "3.3V out" V 5650 2500 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x03" H 5550 2500 50  0001 C CNN
F 3 "" H 5550 2500 50  0000 C CNN
	1    5550 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 1700 5150 1700
Connection ~ 4250 1700
Connection ~ 4250 2000
Connection ~ 1150 2050
Connection ~ 4400 2700
Text GLabel 6000 1600 2    60   Input ~ 0
PowerGood
Wire Wire Line
	5750 1400 5800 1400
Wire Wire Line
	5800 1400 5800 1600
Wire Wire Line
	5800 1600 6000 1600
Connection ~ 5750 1400
Connection ~ 6000 1600
Text GLabel 5350 2850 2    60   Input ~ 0
PowerGood
Connection ~ 3800 2400
$Comp
L +BATT #PWR011
U 1 1 59C2CE5C
P 850 1000
F 0 "#PWR011" H 850 850 50  0001 C CNN
F 1 "+BATT" H 850 1140 50  0000 C CNN
F 2 "" H 850 1000 50  0000 C CNN
F 3 "" H 850 1000 50  0000 C CNN
	1    850  1000
	1    0    0    -1  
$EndComp
Connection ~ 1250 1000
Wire Wire Line
	850  1000 1250 1000
Connection ~ 1250 1150
Connection ~ 1250 1300
Connection ~ 4800 1400
$Comp
L PWR_FLAG #FLG012
U 1 1 59C2D864
P 1150 2050
F 0 "#FLG012" H 1150 2145 50  0001 C CNN
F 1 "PWR_FLAG" H 1150 2230 50  0000 C CNN
F 2 "" H 1150 2050 50  0000 C CNN
F 3 "" H 1150 2050 50  0000 C CNN
	1    1150 2050
	1    0    0    -1  
$EndComp
Connection ~ 750  2050
$Comp
L CP C6
U 1 1 59C2DE3C
P 4250 1850
F 0 "C6" H 4275 1950 50  0000 L CNN
F 1 "220uF" H 4275 1750 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Radial_D10_L20_P5-7.5" H 4288 1700 50  0001 C CNN
F 3 "" H 4250 1850 50  0000 C CNN
	1    4250 1850
	1    0    0    -1  
$EndComp
$Comp
L C_Small C8
U 1 1 59C2DE7D
P 4700 1800
F 0 "C8" H 4710 1870 50  0000 L CNN
F 1 "100nF" H 4710 1720 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 4700 1800 50  0001 C CNN
F 3 "" H 4700 1800 50  0000 C CNN
	1    4700 1800
	1    0    0    -1  
$EndComp
$Comp
L CP C7
U 1 1 59C2E07E
P 4500 1850
F 0 "C7" H 4525 1950 50  0000 L CNN
F 1 "10uF" H 4525 1750 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Radial_D8_L11.5_P3.5" H 4538 1700 50  0001 C CNN
F 3 "" H 4500 1850 50  0000 C CNN
	1    4500 1850
	1    0    0    -1  
$EndComp
$Comp
L C_Small C9
U 1 1 59C2E3B1
P 5000 1800
F 0 "C9" H 5010 1870 50  0000 L CNN
F 1 "10nF" H 5010 1720 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 5000 1800 50  0001 C CNN
F 3 "" H 5000 1800 50  0000 C CNN
	1    5000 1800
	1    0    0    -1  
$EndComp
Connection ~ 4500 1700
Connection ~ 4700 1700
Connection ~ 5000 1700
Wire Wire Line
	4700 2000 4700 1900
Connection ~ 4500 2000
Wire Wire Line
	4700 1900 5150 1900
Wire Wire Line
	5150 1900 5150 1800
Connection ~ 5000 1900
Connection ~ 4700 1900
Connection ~ 5150 1800
Connection ~ 5150 1700
$Comp
L CP C11
U 1 1 59C2EAB4
P 4650 2550
F 0 "C11" H 4675 2650 50  0000 L CNN
F 1 "10uF" H 4675 2450 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Radial_D8_L11.5_P3.5" H 4688 2400 50  0001 C CNN
F 3 "" H 4650 2550 50  0000 C CNN
	1    4650 2550
	1    0    0    -1  
$EndComp
$Comp
L C_Small C12
U 1 1 59C2EAFB
P 4900 2500
F 0 "C12" H 4910 2570 50  0000 L CNN
F 1 "100nF" H 4910 2420 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 4900 2500 50  0001 C CNN
F 3 "" H 4900 2500 50  0000 C CNN
	1    4900 2500
	1    0    0    -1  
$EndComp
$Comp
L C_Small C13
U 1 1 59C2EC7A
P 5150 2500
F 0 "C13" H 5160 2570 50  0000 L CNN
F 1 "10nF" H 5160 2420 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 5150 2500 50  0001 C CNN
F 3 "" H 5150 2500 50  0000 C CNN
	1    5150 2500
	1    0    0    -1  
$EndComp
Connection ~ 4400 2400
Connection ~ 4650 2400
Connection ~ 4900 2400
Connection ~ 5150 2400
Wire Wire Line
	4900 2700 4900 2600
Connection ~ 4650 2700
Wire Wire Line
	4900 2600 5250 2600
Wire Wire Line
	5250 2600 5250 2500
Wire Wire Line
	5250 2500 5350 2500
Connection ~ 5150 2600
Connection ~ 4900 2600
Wire Wire Line
	5350 2850 5350 2600
Connection ~ 5350 2600
Connection ~ 5350 2500
Connection ~ 5350 2400
Connection ~ 5350 2850
Connection ~ 3900 2800
$Comp
L MCP4921-E/P U3
U 1 1 5A15E6F6
P 2900 5150
F 0 "U3" H 2450 5450 50  0000 L CNN
F 1 "MCP4921-E/P" H 2900 5450 50  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_LongPads" H 2900 5150 50  0001 C CIN
F 3 "" H 2900 5150 50  0000 C CNN
	1    2900 5150
	1    0    0    -1  
$EndComp
$Comp
L CONN_02X10 P7
U 1 1 5A15E7BF
P 1300 3950
F 0 "P7" H 1300 4500 50  0000 C CNN
F 1 "LAUNCHPAD P1/P3" V 1300 3950 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x10" H 1300 2750 50  0001 C CNN
F 3 "" H 1300 2750 50  0000 C CNN
	1    1300 3950
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X10 P8
U 1 1 5A15E831
P 2950 3950
F 0 "P8" H 2950 4500 50  0000 C CNN
F 1 "LAUNCHPAD P2" V 3050 3950 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x10" H 2950 3950 50  0001 C CNN
F 3 "" H 2950 3950 50  0000 C CNN
	1    2950 3950
	1    0    0    -1  
$EndComp
$Comp
L C_Small C21
U 1 1 5A15E9E0
P 3650 5750
F 0 "C21" H 3660 5820 50  0000 L CNN
F 1 "100nF" H 3660 5670 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 3650 5750 50  0001 C CNN
F 3 "" H 3650 5750 50  0000 C CNN
	1    3650 5750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 5250 2100 5900
Wire Wire Line
	2100 5250 2300 5250
Connection ~ 2800 5550
Connection ~ 2300 5250
Connection ~ 2100 5250
$Comp
L GND #PWR013
U 1 1 5A15F06A
P 2800 5900
F 0 "#PWR013" H 2800 5650 50  0001 C CNN
F 1 "GND" H 2800 5750 50  0000 C CNN
F 2 "" H 2800 5900 50  0000 C CNN
F 3 "" H 2800 5900 50  0000 C CNN
	1    2800 5900
	1    0    0    -1  
$EndComp
Connection ~ 2800 4750
Wire Wire Line
	2800 4750 2800 4650
Wire Wire Line
	2600 4650 3650 4650
Text GLabel 2600 4650 0    60   Input ~ 0
5V
Connection ~ 2800 4650
$Comp
L C_Small C22
U 1 1 5A15F5C9
P 3000 5750
F 0 "C22" H 3010 5820 50  0000 L CNN
F 1 "100nF" H 3010 5670 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 3000 5750 50  0001 C CNN
F 3 "" H 3000 5750 50  0000 C CNN
	1    3000 5750
	1    0    0    -1  
$EndComp
$Comp
L R R4
U 1 1 5A15F63E
P 3300 5550
F 0 "R4" V 3380 5550 50  0000 C CNN
F 1 "100K" V 3300 5550 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 3230 5550 50  0001 C CNN
F 3 "" H 3300 5550 50  0000 C CNN
	1    3300 5550
	0    1    1    0   
$EndComp
Connection ~ 2800 5900
Wire Wire Line
	2800 5550 2800 5900
Wire Wire Line
	2100 5900 4200 5900
Wire Wire Line
	3000 5650 3000 5550
Wire Wire Line
	3000 5550 3150 5550
Wire Wire Line
	3000 5900 3000 5850
Connection ~ 3000 5900
Connection ~ 3000 5850
Connection ~ 3000 5550
Connection ~ 3000 5650
Connection ~ 3150 5550
Wire Wire Line
	3650 4650 3650 5650
Wire Wire Line
	3650 5900 3650 5850
Wire Wire Line
	3450 5550 3650 5550
Connection ~ 3650 5550
Connection ~ 3450 5550
Wire Wire Line
	2750 4000 2300 4000
Wire Wire Line
	2300 4000 2300 4950
Connection ~ 2750 4000
Connection ~ 2300 4950
Connection ~ 1050 4100
Wire Wire Line
	1050 4100 950  4100
Wire Wire Line
	950  4100 950  5050
Connection ~ 2300 5050
Wire Wire Line
	2750 3700 2200 3700
Connection ~ 2750 3700
Connection ~ 2300 5150
$Comp
L R R5
U 1 1 5A161286
P 3950 5150
F 0 "R5" V 4030 5150 50  0000 C CNN
F 1 "5K" V 3950 5150 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 3880 5150 50  0001 C CNN
F 3 "" H 3950 5150 50  0000 C CNN
	1    3950 5150
	0    1    1    0   
$EndComp
Wire Wire Line
	3500 5150 3800 5150
$Comp
L C_Small C23
U 1 1 5A1614AE
P 4200 5550
F 0 "C23" H 4210 5620 50  0000 L CNN
F 1 "100nF" H 4210 5470 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 4200 5550 50  0001 C CNN
F 3 "" H 4200 5550 50  0000 C CNN
	1    4200 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 5150 4350 5150
Wire Wire Line
	4200 5150 4200 5450
Wire Wire Line
	4200 5900 4200 5650
Connection ~ 3650 5900
Connection ~ 4100 5150
Connection ~ 3800 5150
Connection ~ 3500 5150
$Comp
L CP C24
U 1 1 5A161BE5
P 4500 5150
F 0 "C24" H 4525 5250 50  0000 L CNN
F 1 "1uF" H 4525 5050 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Axial_D5_L11_P18" H 4538 5000 50  0001 C CNN
F 3 "" H 4500 5150 50  0000 C CNN
	1    4500 5150
	0    -1   -1   0   
$EndComp
Connection ~ 4200 5150
Connection ~ 4350 5150
$Comp
L CONN_01X03 P9
U 1 1 5A161EDA
P 5000 5250
F 0 "P9" H 5000 5450 50  0000 C CNN
F 1 "Volume" V 5100 5250 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x03" H 5000 5250 50  0001 C CNN
F 3 "" H 5000 5250 50  0000 C CNN
	1    5000 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 5150 4800 5150
Connection ~ 4800 5150
Connection ~ 4650 5150
$Comp
L GND #PWR014
U 1 1 5A16203B
P 4700 5900
F 0 "#PWR014" H 4700 5650 50  0001 C CNN
F 1 "GND" H 4700 5750 50  0000 C CNN
F 2 "" H 4700 5900 50  0000 C CNN
F 3 "" H 4700 5900 50  0000 C CNN
	1    4700 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 5350 4700 5350
Wire Wire Line
	4700 5350 4700 5900
Connection ~ 4800 5350
Connection ~ 4700 5900
Text Label 950  5050 0    60   ~ 0
CLK
Text Label 2250 3700 0    60   ~ 0
~CS
Text Label 2300 4100 0    60   ~ 0
SDI
Connection ~ 2750 3600
$Comp
L MAX4295ESE U4
U 1 1 5A19BF8A
P 7850 4400
F 0 "U4" H 7400 5100 50  0000 L CNN
F 1 "MAX4295ESE" H 8150 5100 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-16_3.9x9.9mm_Pitch1.27mm" H 7850 4400 50  0001 C CIN
F 3 "" H 7850 4400 50  0000 C CNN
	1    7850 4400
	1    0    0    -1  
$EndComp
$Comp
L INDUCTOR_SMALL L3
U 1 1 5A19C17E
P 7300 3150
F 0 "L3" H 7300 3250 50  0000 C CNN
F 1 "30uH" H 7300 3100 50  0000 C CNN
F 2 "Choke_Toroid_ThroughHole:Choke_Toroid_5x10mm_Vertical" H 7300 3150 50  0001 C CNN
F 3 "" H 7300 3150 50  0000 C CNN
	1    7300 3150
	1    0    0    -1  
$EndComp
$Comp
L INDUCTOR_SMALL L4
U 1 1 5A19C1F6
P 8250 3150
F 0 "L4" H 8250 3250 50  0000 C CNN
F 1 "30uH" H 8250 3100 50  0000 C CNN
F 2 "Choke_Toroid_ThroughHole:Choke_Toroid_5x10mm_Vertical" H 8250 3150 50  0001 C CNN
F 3 "" H 8250 3150 50  0000 C CNN
	1    8250 3150
	1    0    0    -1  
$EndComp
$Comp
L C_Small C31
U 1 1 5A19C255
P 7550 3250
F 0 "C31" H 7560 3320 50  0000 L CNN
F 1 "1uF" H 7560 3170 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Radial_D5_L11_P2" H 7550 3250 50  0001 C CNN
F 3 "" H 7550 3250 50  0000 C CNN
	1    7550 3250
	1    0    0    -1  
$EndComp
$Comp
L C_Small C32
U 1 1 5A19C2E2
P 8000 3250
F 0 "C32" H 8010 3320 50  0000 L CNN
F 1 "1uF" H 8010 3170 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Radial_D5_L11_P2" H 8000 3250 50  0001 C CNN
F 3 "" H 8000 3250 50  0000 C CNN
	1    8000 3250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR015
U 1 1 5A19C345
P 8750 4800
F 0 "#PWR015" H 8750 4550 50  0001 C CNN
F 1 "GND" H 8750 4650 50  0000 C CNN
F 2 "" H 8750 4800 50  0000 C CNN
F 3 "" H 8750 4800 50  0000 C CNN
	1    8750 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 4600 8750 4600
Wire Wire Line
	8750 4600 8750 4800
Wire Wire Line
	8450 4700 8750 4700
Connection ~ 8750 4700
Connection ~ 8450 4600
Connection ~ 8450 4700
$Comp
L GND #PWR016
U 1 1 5A19C742
P 9000 4800
F 0 "#PWR016" H 9000 4550 50  0001 C CNN
F 1 "GND" H 9000 4650 50  0000 C CNN
F 2 "" H 9000 4800 50  0000 C CNN
F 3 "" H 9000 4800 50  0000 C CNN
	1    9000 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 4450 9000 4450
Wire Wire Line
	9000 4450 9000 4800
Connection ~ 9000 4800
Connection ~ 8750 4800
Connection ~ 8450 4450
Text GLabel 6400 4250 0    60   Input ~ 0
5V
$Comp
L C_Small C26
U 1 1 5A19CAB1
P 6500 4350
F 0 "C26" H 6510 4420 50  0000 L CNN
F 1 "1uF" H 6510 4270 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 6500 4350 50  0001 C CNN
F 3 "" H 6500 4350 50  0000 C CNN
	1    6500 4350
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR017
U 1 1 5A19CB7D
P 6500 4450
F 0 "#PWR017" H 6500 4200 50  0001 C CNN
F 1 "GND" H 6500 4300 50  0000 C CNN
F 2 "" H 6500 4450 50  0000 C CNN
F 3 "" H 6500 4450 50  0000 C CNN
	1    6500 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 4250 6750 4250
Connection ~ 6500 4250
Connection ~ 7250 4400
Connection ~ 6500 4450
Text GLabel 5000 5600 2    60   Input ~ 0
Audio
Wire Wire Line
	4800 5250 4600 5250
Wire Wire Line
	4600 5250 4600 5600
Wire Wire Line
	4600 5600 5000 5600
Connection ~ 4800 5250
Connection ~ 5000 5600
$Comp
L C_Small C27
U 1 1 5A19D166
P 5700 4700
F 0 "C27" H 5710 4770 50  0000 L CNN
F 1 "100nF" H 5710 4620 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 5700 4700 50  0001 C CNN
F 3 "" H 5700 4700 50  0000 C CNN
	1    5700 4700
	0    1    1    0   
$EndComp
$Comp
L C_Small C28
U 1 1 5A19D1D9
P 6350 4800
F 0 "C28" H 6360 4870 50  0000 L CNN
F 1 "150pF" H 6360 4720 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 6350 4800 50  0001 C CNN
F 3 "" H 6350 4800 50  0000 C CNN
	1    6350 4800
	1    0    0    -1  
$EndComp
$Comp
L R R6
U 1 1 5A19D412
P 6100 4700
F 0 "R6" V 6180 4700 50  0000 C CNN
F 1 "47K" V 6100 4700 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 6030 4700 50  0001 C CNN
F 3 "" H 6100 4700 50  0000 C CNN
	1    6100 4700
	0    1    1    0   
$EndComp
$Comp
L R R7
U 1 1 5A19D496
P 6650 4700
F 0 "R7" V 6730 4700 50  0000 C CNN
F 1 "47K" V 6650 4700 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 6580 4700 50  0001 C CNN
F 3 "" H 6650 4700 50  0000 C CNN
	1    6650 4700
	0    1    1    0   
$EndComp
$Comp
L R R8
U 1 1 5A19D50E
P 7050 4850
F 0 "R8" V 7130 4850 50  0000 C CNN
F 1 "100K" V 7050 4850 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 6980 4850 50  0001 C CNN
F 3 "" H 7050 4850 50  0000 C CNN
	1    7050 4850
	-1   0    0    1   
$EndComp
$Comp
L C_Small C29
U 1 1 5A19D5E2
P 6800 4800
F 0 "C29" H 6810 4870 50  0000 L CNN
F 1 "5pF" H 6810 4720 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 6800 4800 50  0001 C CNN
F 3 "" H 6800 4800 50  0000 C CNN
	1    6800 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 4700 7250 4700
Wire Wire Line
	6800 4900 6800 5000
Wire Wire Line
	6800 5000 7150 5000
Wire Wire Line
	6250 4700 6500 4700
Connection ~ 6350 4700
Wire Wire Line
	5800 4700 5950 4700
$Comp
L GND #PWR018
U 1 1 5A19DEB8
P 6350 4900
F 0 "#PWR018" H 6350 4650 50  0001 C CNN
F 1 "GND" H 6350 4750 50  0000 C CNN
F 2 "" H 6350 4900 50  0000 C CNN
F 3 "" H 6350 4900 50  0000 C CNN
	1    6350 4900
	1    0    0    -1  
$EndComp
Connection ~ 5950 4700
Connection ~ 6350 4900
Connection ~ 5800 4700
Text GLabel 5600 4700 0    60   Input ~ 0
Audio
Connection ~ 5600 4700
Connection ~ 6800 4700
Connection ~ 6800 4900
Connection ~ 7050 5000
Connection ~ 7050 4700
Wire Wire Line
	7250 4400 6750 4400
Wire Wire Line
	6750 4400 6750 4250
Wire Wire Line
	7150 5000 7150 4900
Wire Wire Line
	7150 4900 7250 4900
Connection ~ 7250 4900
Connection ~ 7250 4700
$Comp
L C_Small C30
U 1 1 5A19F225
P 8700 3900
F 0 "C30" H 8710 3970 50  0000 L CNN
F 1 "100nF" H 8710 3820 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 8700 3900 50  0001 C CNN
F 3 "" H 8700 3900 50  0000 C CNN
	1    8700 3900
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR019
U 1 1 5A19F2E4
P 8900 3950
F 0 "#PWR019" H 8900 3700 50  0001 C CNN
F 1 "GND" H 8900 3800 50  0000 C CNN
F 2 "" H 8900 3950 50  0000 C CNN
F 3 "" H 8900 3950 50  0000 C CNN
	1    8900 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 3900 8600 3900
Wire Wire Line
	8800 3900 8900 3900
Wire Wire Line
	8900 3900 8900 3950
Connection ~ 8450 3900
Connection ~ 8600 3900
Connection ~ 8800 3900
Connection ~ 8900 3950
$Comp
L GND #PWR020
U 1 1 5A19F7A9
P 6950 3950
F 0 "#PWR020" H 6950 3700 50  0001 C CNN
F 1 "GND" H 6950 3800 50  0000 C CNN
F 2 "" H 6950 3950 50  0000 C CNN
F 3 "" H 6950 3950 50  0000 C CNN
	1    6950 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 3950 6950 3900
Wire Wire Line
	6950 3900 7250 3900
Connection ~ 7250 3900
Connection ~ 6950 3950
$Comp
L GND #PWR021
U 1 1 5A1A117F
P 7800 3350
F 0 "#PWR021" H 7800 3100 50  0001 C CNN
F 1 "GND" H 7800 3200 50  0000 C CNN
F 2 "" H 7800 3350 50  0000 C CNN
F 3 "" H 7800 3350 50  0000 C CNN
	1    7800 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7550 3350 8000 3350
Connection ~ 7800 3350
Connection ~ 8000 3350
Connection ~ 7550 3350
Connection ~ 7550 3150
Connection ~ 8000 3150
Wire Wire Line
	7050 3150 6800 3150
Wire Wire Line
	6800 3150 6800 4150
Wire Wire Line
	6800 4150 7250 4150
Wire Wire Line
	8500 3150 9150 3150
Wire Wire Line
	9150 3150 9150 4150
Wire Wire Line
	9150 4150 8450 4150
Connection ~ 8450 4150
Connection ~ 7250 4150
Connection ~ 7050 3150
Connection ~ 8500 3150
$Comp
L CONN_01X02 P10
U 1 1 5A1A166D
P 7800 2950
F 0 "P10" H 7800 3100 50  0000 C CNN
F 1 "Audio out" V 7900 2950 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x02" H 7800 2950 50  0001 C CNN
F 3 "" H 7800 2950 50  0000 C CNN
	1    7800 2950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7850 3150 8000 3150
Wire Wire Line
	7750 3150 7550 3150
Connection ~ 7850 3150
Connection ~ 7750 3150
$Comp
L GND #PWR022
U 1 1 5A1A1ACA
P 8700 4250
F 0 "#PWR022" H 8700 4000 50  0001 C CNN
F 1 "GND" H 8700 4100 50  0000 C CNN
F 2 "" H 8700 4250 50  0000 C CNN
F 3 "" H 8700 4250 50  0000 C CNN
	1    8700 4250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR023
U 1 1 5A1A1B3A
P 7000 4250
F 0 "#PWR023" H 7000 4000 50  0001 C CNN
F 1 "GND" H 7000 4100 50  0000 C CNN
F 2 "" H 7000 4250 50  0000 C CNN
F 3 "" H 7000 4250 50  0000 C CNN
	1    7000 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 4250 8700 4250
Wire Wire Line
	7250 4250 7000 4250
Connection ~ 8700 4250
Connection ~ 8450 4250
Connection ~ 7250 4250
Connection ~ 7000 4250
$Comp
L CP C33
U 1 1 5A1A1F53
P 6050 3800
F 0 "C33" H 6075 3900 50  0000 L CNN
F 1 "330uF" H 6075 3700 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Radial_D8_L21_P3.8" H 6088 3650 50  0001 C CNN
F 3 "" H 6050 3800 50  0000 C CNN
	1    6050 3800
	1    0    0    -1  
$EndComp
$Comp
L C_Small C34
U 1 1 5A1A2145
P 6400 3750
F 0 "C34" H 6410 3820 50  0000 L CNN
F 1 "1uF" H 6410 3670 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Radial_D5_L11_P2" H 6400 3750 50  0001 C CNN
F 3 "" H 6400 3750 50  0000 C CNN
	1    6400 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 3650 6650 3650
Wire Wire Line
	6650 3650 6650 4050
Wire Wire Line
	6650 4050 7250 4050
Connection ~ 6400 3650
Connection ~ 7250 4050
$Comp
L GND #PWR024
U 1 1 5A1A248A
P 6250 3950
F 0 "#PWR024" H 6250 3700 50  0001 C CNN
F 1 "GND" H 6250 3800 50  0000 C CNN
F 2 "" H 6250 3950 50  0000 C CNN
F 3 "" H 6250 3950 50  0000 C CNN
	1    6250 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 3950 6400 3950
Wire Wire Line
	6400 3950 6400 3850
Connection ~ 6250 3950
Text GLabel 5950 3650 0    60   Input ~ 0
5V
Connection ~ 6050 3650
$Comp
L C_Small C35
U 1 1 5A1A2A50
P 9450 3750
F 0 "C35" H 9460 3820 50  0000 L CNN
F 1 "1uF" H 9460 3670 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Radial_D5_L11_P2" H 9450 3750 50  0001 C CNN
F 3 "" H 9450 3750 50  0000 C CNN
	1    9450 3750
	1    0    0    -1  
$EndComp
$Comp
L CP C36
U 1 1 5A1A2B06
P 9850 3800
F 0 "C36" H 9875 3900 50  0000 L CNN
F 1 "330uF" H 9875 3700 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Radial_D8_L21_P3.8" H 9888 3650 50  0001 C CNN
F 3 "" H 9850 3800 50  0000 C CNN
	1    9850 3800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR025
U 1 1 5A1A2B89
P 9650 3950
F 0 "#PWR025" H 9650 3700 50  0001 C CNN
F 1 "GND" H 9650 3800 50  0000 C CNN
F 2 "" H 9650 3950 50  0000 C CNN
F 3 "" H 9650 3950 50  0000 C CNN
	1    9650 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	9250 3650 10050 3650
Wire Wire Line
	9250 3650 9250 4050
Wire Wire Line
	9250 4050 8450 4050
Connection ~ 9450 3650
Wire Wire Line
	9450 3950 9850 3950
Connection ~ 9650 3950
Connection ~ 9850 3650
Connection ~ 9450 3850
Wire Wire Line
	9450 3850 9450 3950
Text GLabel 10050 3650 2    60   Input ~ 0
5V
Connection ~ 10050 3650
Text GLabel 8300 5200 0    60   Input ~ 0
5V
$Comp
L CONN_01X03 P11
U 1 1 5A1A36C2
P 8750 5150
F 0 "P11" H 8750 5350 50  0000 C CNN
F 1 "JP1" V 8850 5150 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03" H 8750 5150 50  0001 C CNN
F 3 "" H 8750 5150 50  0000 C CNN
	1    8750 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	8300 5200 8400 5200
Wire Wire Line
	8400 5200 8400 5050
Wire Wire Line
	8400 5050 8550 5050
Wire Wire Line
	8450 4900 8450 5150
Wire Wire Line
	8450 5150 8550 5150
Connection ~ 8450 4900
Connection ~ 8550 5050
Connection ~ 8550 5150
Text GLabel 2650 4200 0    60   Input ~ 0
amp_en
Wire Wire Line
	2650 4200 2750 4200
Connection ~ 2750 4200
Connection ~ 2650 4200
Text GLabel 8450 5400 0    60   Input ~ 0
amp_en
Wire Wire Line
	8450 5400 8500 5400
Wire Wire Line
	8500 5400 8500 5250
Wire Wire Line
	8500 5250 8550 5250
Connection ~ 8300 5200
Connection ~ 8450 5400
Connection ~ 8550 5250
$Comp
L CONN_02X06 P12
U 1 1 5A1A7718
P 9050 1200
F 0 "P12" H 9050 1550 50  0000 C CNN
F 1 "I/O board" H 9050 850 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x06" H 9050 0   50  0001 C CNN
F 3 "" H 9050 0   50  0000 C CNN
	1    9050 1200
	1    0    0    -1  
$EndComp
$Comp
L ZENER D2
U 1 1 5A1A7856
P 7750 2150
F 0 "D2" H 7750 2250 50  0000 C CNN
F 1 "3.3V" H 7750 2050 50  0000 C CNN
F 2 "Diodes_ThroughHole:Diode_DO-35_SOD27_Horizontal_RM10" H 7750 2150 50  0001 C CNN
F 3 "" H 7750 2150 50  0000 C CNN
	1    7750 2150
	0    1    1    0   
$EndComp
$Comp
L ZENER D3
U 1 1 5A1A78E0
P 8050 2150
F 0 "D3" H 8050 2250 50  0000 C CNN
F 1 "3.3V" H 8050 2050 50  0000 C CNN
F 2 "Diodes_ThroughHole:Diode_DO-35_SOD27_Horizontal_RM10" H 8050 2150 50  0001 C CNN
F 3 "" H 8050 2150 50  0000 C CNN
	1    8050 2150
	0    1    1    0   
$EndComp
$Comp
L ZENER D4
U 1 1 5A1A7972
P 8350 2150
F 0 "D4" H 8350 2250 50  0000 C CNN
F 1 "3.3V" H 8350 2050 50  0000 C CNN
F 2 "Diodes_ThroughHole:Diode_DO-35_SOD27_Horizontal_RM10" H 8350 2150 50  0001 C CNN
F 3 "" H 8350 2150 50  0000 C CNN
	1    8350 2150
	0    1    1    0   
$EndComp
$Comp
L ZENER D5
U 1 1 5A1A7A02
P 8650 2150
F 0 "D5" H 8650 2250 50  0000 C CNN
F 1 "3.3V" H 8650 2050 50  0000 C CNN
F 2 "Diodes_ThroughHole:Diode_DO-35_SOD27_Horizontal_RM10" H 8650 2150 50  0001 C CNN
F 3 "" H 8650 2150 50  0000 C CNN
	1    8650 2150
	0    1    1    0   
$EndComp
$Comp
L R R10
U 1 1 5A1A8176
P 7750 1800
F 0 "R10" V 7830 1800 50  0000 C CNN
F 1 "100R" V 7750 1800 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 7680 1800 50  0001 C CNN
F 3 "" H 7750 1800 50  0000 C CNN
	1    7750 1800
	1    0    0    -1  
$EndComp
$Comp
L R R11
U 1 1 5A1A8216
P 8050 1800
F 0 "R11" V 8130 1800 50  0000 C CNN
F 1 "100R" V 8050 1800 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 7980 1800 50  0001 C CNN
F 3 "" H 8050 1800 50  0000 C CNN
	1    8050 1800
	1    0    0    -1  
$EndComp
$Comp
L R R12
U 1 1 5A1A82B1
P 8350 1800
F 0 "R12" V 8430 1800 50  0000 C CNN
F 1 "100R" V 8350 1800 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 8280 1800 50  0001 C CNN
F 3 "" H 8350 1800 50  0000 C CNN
	1    8350 1800
	1    0    0    -1  
$EndComp
$Comp
L R R13
U 1 1 5A1A833C
P 8650 1800
F 0 "R13" V 8730 1800 50  0000 C CNN
F 1 "100R" V 8650 1800 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 8580 1800 50  0001 C CNN
F 3 "" H 8650 1800 50  0000 C CNN
	1    8650 1800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR026
U 1 1 5A1A83C9
P 7750 2350
F 0 "#PWR026" H 7750 2100 50  0001 C CNN
F 1 "GND" H 7750 2200 50  0000 C CNN
F 2 "" H 7750 2350 50  0000 C CNN
F 3 "" H 7750 2350 50  0000 C CNN
	1    7750 2350
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR027
U 1 1 5A1A8455
P 8050 2350
F 0 "#PWR027" H 8050 2100 50  0001 C CNN
F 1 "GND" H 8050 2200 50  0000 C CNN
F 2 "" H 8050 2350 50  0000 C CNN
F 3 "" H 8050 2350 50  0000 C CNN
	1    8050 2350
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR028
U 1 1 5A1A84E1
P 8350 2350
F 0 "#PWR028" H 8350 2100 50  0001 C CNN
F 1 "GND" H 8350 2200 50  0000 C CNN
F 2 "" H 8350 2350 50  0000 C CNN
F 3 "" H 8350 2350 50  0000 C CNN
	1    8350 2350
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR029
U 1 1 5A1A856D
P 8650 2350
F 0 "#PWR029" H 8650 2100 50  0001 C CNN
F 1 "GND" H 8650 2200 50  0000 C CNN
F 2 "" H 8650 2350 50  0000 C CNN
F 3 "" H 8650 2350 50  0000 C CNN
	1    8650 2350
	1    0    0    -1  
$EndComp
Connection ~ 8650 2350
Connection ~ 8350 2350
Connection ~ 8050 2350
Connection ~ 7750 2350
Connection ~ 8050 1950
Connection ~ 8350 1950
Connection ~ 8650 1950
Connection ~ 7750 1950
Text GLabel 7700 1950 0    60   Input ~ 0
S1
Text GLabel 8000 1950 0    60   Input ~ 0
S2
Text GLabel 8300 1950 0    60   Input ~ 0
S3
Text GLabel 8600 1950 0    60   Input ~ 0
S4
Wire Wire Line
	8000 1950 8050 1950
Wire Wire Line
	8300 1950 8350 1950
Wire Wire Line
	8600 1950 8650 1950
Wire Wire Line
	7700 1950 7750 1950
Wire Wire Line
	7750 1650 7750 1450
Wire Wire Line
	7750 1450 8800 1450
Wire Wire Line
	8050 1650 8050 1350
Wire Wire Line
	8050 1350 8800 1350
Wire Wire Line
	8350 1650 8350 1250
Wire Wire Line
	8350 1250 8800 1250
Wire Wire Line
	8650 1650 8650 1150
Wire Wire Line
	8650 1150 8800 1150
Connection ~ 8800 1150
Connection ~ 8800 1250
Connection ~ 8800 1350
Connection ~ 8800 1450
Connection ~ 8650 1650
Connection ~ 8350 1650
Connection ~ 8050 1650
Connection ~ 7750 1650
$Comp
L GND #PWR030
U 1 1 5A1AA160
P 8300 950
F 0 "#PWR030" H 8300 700 50  0001 C CNN
F 1 "GND" H 8300 800 50  0000 C CNN
F 2 "" H 8300 950 50  0000 C CNN
F 3 "" H 8300 950 50  0000 C CNN
	1    8300 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	8300 950  8800 950 
Connection ~ 8800 950 
Connection ~ 8300 950 
Text GLabel 8700 1050 0    60   Input ~ 0
3.3V
Wire Wire Line
	8700 1050 8800 1050
Connection ~ 8800 1050
Connection ~ 8700 1050
Text GLabel 900  3900 0    60   Input ~ 0
S4
Wire Wire Line
	900  3900 1050 3900
Connection ~ 1050 3900
Connection ~ 900  3900
Text GLabel 900  4200 0    60   Input ~ 0
S3
Wire Wire Line
	900  4200 1050 4200
Connection ~ 1050 4200
Connection ~ 900  4200
Text GLabel 2650 4400 0    60   Input ~ 0
S2
Wire Wire Line
	2650 4400 2750 4400
Connection ~ 2750 4400
Connection ~ 2650 4400
Text GLabel 2650 3600 0    60   Input ~ 0
S1
Wire Wire Line
	2650 3600 2750 3600
Connection ~ 2650 3600
$Comp
L C_Small C37
U 1 1 5A1C6B6B
P 8550 850
F 0 "C37" H 8560 920 50  0000 L CNN
F 1 "100nF" H 8560 770 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 8550 850 50  0001 C CNN
F 3 "" H 8550 850 50  0000 C CNN
	1    8550 850 
	1    0    0    -1  
$EndComp
Wire Wire Line
	8550 750  8700 750 
Wire Wire Line
	8700 750  8700 1050
Connection ~ 8550 750 
Connection ~ 8550 950 
Text GLabel 9400 950  2    60   Input ~ 0
SDI_DISP
Wire Wire Line
	9300 950  9400 950 
Connection ~ 9300 950 
Connection ~ 9400 950 
Text GLabel 9950 1050 2    60   Input ~ 0
CLK_DISP
Wire Wire Line
	9300 1050 9950 1050
Connection ~ 9300 1050
Connection ~ 9950 1050
Text GLabel 10550 1150 2    60   Input ~ 0
D/C_DISP
Wire Wire Line
	9300 1150 10550 1150
Connection ~ 9300 1150
Connection ~ 9400 1150
Text GLabel 9950 1250 2    60   Input ~ 0
RST_DISP
Wire Wire Line
	9300 1250 9950 1250
Connection ~ 9950 1250
Connection ~ 9300 1250
Text GLabel 9400 1350 2    60   Input ~ 0
LED_DISP
Wire Wire Line
	9300 1350 9400 1350
Connection ~ 9300 1350
Connection ~ 9400 1350
Text GLabel 950  3600 0    60   Input ~ 0
SDI_DISP
Wire Wire Line
	950  3600 1050 3600
Connection ~ 1050 3600
Connection ~ 950  3600
Wire Wire Line
	950  5050 2300 5050
Wire Wire Line
	2300 5050 2300 5000
Text GLabel 1600 3800 2    60   Input ~ 0
CLK_DISP
Wire Wire Line
	1550 3800 1600 3800
Connection ~ 1550 3800
Connection ~ 1600 3800
Text GLabel 1600 4200 2    60   Input ~ 0
D/C_DISP
Wire Wire Line
	1550 4200 1600 4200
Connection ~ 1550 4200
Connection ~ 1600 4200
Text GLabel 1650 4050 2    60   Input ~ 0
RST_DISP
Connection ~ 1550 4100
Wire Wire Line
	1650 4050 1600 4050
Wire Wire Line
	1600 4050 1600 4100
Wire Wire Line
	1600 4100 1550 4100
Connection ~ 1650 4050
Wire Wire Line
	2300 5150 2200 5150
Wire Wire Line
	2200 5150 2200 3700
Text GLabel 1650 4400 2    60   Input ~ 0
LED_DISP
Wire Wire Line
	1550 4300 1600 4300
Wire Wire Line
	1600 4300 1600 4400
Wire Wire Line
	1600 4400 1650 4400
Connection ~ 1550 4300
Connection ~ 1650 4400
$Comp
L TEST_1P W1
U 1 1 5A1F1777
P 6000 1600
F 0 "W1" H 6000 1870 50  0000 C CNN
F 1 "PWR_GD" H 6000 1800 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01" H 6200 1600 50  0001 C CNN
F 3 "" H 6200 1600 50  0000 C CNN
	1    6000 1600
	1    0    0    -1  
$EndComp
$Comp
L TEST_1P W2
U 1 1 5A1F1A83
P 5350 2850
F 0 "W2" H 5350 3120 50  0000 C CNN
F 1 "PWR_GD" H 5350 3050 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01" H 5550 2850 50  0001 C CNN
F 3 "" H 5550 2850 50  0000 C CNN
	1    5350 2850
	-1   0    0    1   
$EndComp
$Comp
L TEST_1P W3
U 1 1 5A22FAAE
P 3200 1050
F 0 "W3" H 3200 1320 50  0000 C CNN
F 1 "TEST_5V" H 3200 1250 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01" H 3400 1050 50  0001 C CNN
F 3 "" H 3400 1050 50  0000 C CNN
	1    3200 1050
	1    0    0    -1  
$EndComp
$Comp
L TEST_1P W4
U 1 1 5A22FB46
P 6600 950
F 0 "W4" H 6600 1220 50  0000 C CNN
F 1 "TEST_3.3V" H 6600 1150 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01" H 6800 950 50  0001 C CNN
F 3 "" H 6800 950 50  0000 C CNN
	1    6600 950 
	1    0    0    -1  
$EndComp
$Comp
L TEST_1P W5
U 1 1 5A23028C
P 9400 950
F 0 "W5" H 9400 1220 50  0000 C CNN
F 1 "TEST_SDI" H 9400 1150 50  0000 C CNN
F 2 "" H 9600 950 50  0000 C CNN
F 3 "" H 9600 950 50  0000 C CNN
	1    9400 950 
	1    0    0    -1  
$EndComp
$Comp
L TEST_1P W6
U 1 1 5A23032B
P 9950 1050
F 0 "W6" H 9950 1320 50  0000 C CNN
F 1 "TEST_CLK" H 9950 1250 50  0000 C CNN
F 2 "" H 10150 1050 50  0000 C CNN
F 3 "" H 10150 1050 50  0000 C CNN
	1    9950 1050
	1    0    0    -1  
$EndComp
$Comp
L TEST_1P W7
U 1 1 5A2303D0
P 10550 1150
F 0 "W7" H 10550 1420 50  0000 C CNN
F 1 "TEST_D/C" H 10550 1350 50  0000 C CNN
F 2 "" H 10750 1150 50  0000 C CNN
F 3 "" H 10750 1150 50  0000 C CNN
	1    10550 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	10550 1150 10550 1100
Connection ~ 10550 1150
$EndSCHEMATC
